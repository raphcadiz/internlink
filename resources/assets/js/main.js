window.$ = window.jQuery = require('jquery')
require('bootstrap-sass');

$( document ).ready(function() {
    console.log($.fn.tooltip.Constructor.VERSION);

    $(document).on('click','.animate_wrapper',function(event){
    	$(this).children('.animate_field').focus();
    	$(this).children('.animate_field').focusout(function(){
    		if($(this).val() == ''){
    			$(this).next('.animate_label').removeClass('animate');
    		}
    	});
    	$(this).children('.animate_label').addClass('animate');
    	event.stopPropagation();
    });
});