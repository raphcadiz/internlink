(function(){
    window.Coupon = {
        init : function(){
            this.bindEvents();
        },

        bindEvents : function(){
            $('#add-new-coupon').on( 'click',  $.proxy(this.createCoupon,this) );
        },

        createCoupon : function(e){
            $.ajax({
                type:'GET',
                url: base_url+'/administrator/coupons/add-new'
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },
    }

    Coupon.init();
})();
