(function () {
    window.Matches = {
        init: function init() {
            this.bindEvents();
        },

        bindEvents : function(){
            $(document).on( 'click', '.save-job', $.proxy(this.saveJob,this) );
            $(document).on('click', '.go-to-job', $.proxy(this.goToJob,this) );
        },

        saveJob: function saveJob(e) {
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type: 'POST',
                url: base_url + '/student/save-job/' + id,
                data: '_token=' + _token
            }).done(function (data) {
                if (data == 'max') {
                    $('#maxxed-saved-jobs').modal('show');
                } else if (data == 'saved') {
                    self.children('i').addClass('saved-job');
                } else if (data == 'deleted') {
                    self.children('i').removeClass('saved-job');
                } else {
                    console.log('Something went wrong. Please try again later.');
                }
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        goToJob: function goToJob(e) {
            var self = $(e.currentTarget);
            var id = self.attr('data-id');

            window.location.href = base_url+'/student/apply-job/'+id;
        },

        filter: function() {
            var $token = $('input[name=_token]').val(),
                $keyword = $('input[name=keyword]').val(),
                $location = $('input[name=location]').val(),
                $company = $('input[name=company]').val(),
                $compatibility = $('input[name=compatibility]').val(),
                $industry_id = $('select[name=industry_id]').val(),
                $career_id = $('select[name=career_id]').val(),
                $career_match = $('input[name=career_match]').is(':checked') ? 1 : 0,
                $job_type = [],
                $main = [],
                $time_commitment = [],
                $compensation = [],
                $location_type = [],
                $employer_type = [],
                $employer_size = [],
                $url = $('#filter-matches').attr('action'),
                $loader = $('.loading-matches'),
                $result_table = $('.matches-result-table');

            $('.job-type').each(function() {
                if($(this).is(":checked")) {
                    $job_type.push($(this).val());
                }
            });

            $('.main').each(function() {
                if($(this).is(":checked")) {
                    $main.push($(this).val());
                }
            });

            $('.time-commitment').each(function() {
                if($(this).is(":checked")) {
                    $time_commitment.push($(this).val());
                }
            });

            $('.compensation').each(function() {
                if($(this).is(":checked")) {
                    $compensation.push($(this).val());
                }
            });

            $('.location').each(function() {
                if($(this).is(":checked")) {
                    $location_type.push($(this).val());
                }
            });

            $('.employer-type').each(function() {
                if($(this).is(":checked")) {
                    $employer_type.push($(this).val());
                }
            });

            $('.employer-size').each(function() {
                if($(this).is(":checked")) {
                    $employer_size.push($(this).val());
                }
            });

            var $def = $.ajax({
                type: 'POST',
                url : $url,
                data: {
                    _token : $token,
                    keyword : $keyword,
                    location : $location,
                    company : $company,
                    job_type : $job_type,
                    main : $main,
                    time_commitment : $time_commitment,
                    compensation : $compensation,
                    location_type : $location_type,
                    employer_type : $employer_type,
                    employer_size : $employer_size,
                    compatibility : $compatibility,
                    industry_id : $industry_id,
                    career_id : $career_id,
                    career_match : $career_match
                },
                beforeSend: function() {
                    $loader.show();
                    $result_table.html('');
                }
            });

            $def.done(function(view) {
                $loader.delay(500).fadeOut('slow', function() {
                    $result_table.html(view);
                });
            });
        }


    }

    $('select').select2();

    $('.filter-slider').slider({
        reversed: true
    });

    $(document).on('change', 'select[name=industry_id]', function() {
        var $industry_id = $(this).val()
            $token = $('input[name=_token]').val(),
            $career = $('select[name=career_id]');

        $.ajax({
            type: 'GET',
            url: '/student/matches/get-careers-by-industry/'+$industry_id,
            success: function(options) {
                $career.html(options);
            }
        });
    });

    $(document).on('change', 'input.check-all', function() {
        var $this = $(this),
            $class = $this.attr('data-class-name'),
            $form = $('form#filter-matches'),
            $check_ones = $form.find('input.'+$class+'.check-one');

        if($this.is(':checked')) {
            $check_ones.prop('checked', true);
        } else {
            $check_ones.prop('checked', false);
        }

    });

    $(document).on('change', 'input.check-one', function() {
        var $this = $(this),
            $class = $this.attr('data-class-name'),
            $form = $('form#filter-matches'),
            $check_all = $form.find('input.'+$class+'.check-all'),
            $check_ones = $form.find('input.'+$class+'.check-one:checked');

        if($this.is(':checked') && (($form.find('input.'+$class).length -1) == $check_ones.length)) {
            $check_all.prop('checked', true);
        } else {
            $check_all.prop('checked', false);
        }
    });

    $('form#filter-matches').on('submit', function(e) {
        e.preventDefault();
        Matches.filter();
    });

    Matches.init();
    Matches.filter();
})();