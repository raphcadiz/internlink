(function(){
    window.UserManagement = {
        init : function(){
            this.bindEvents();
        },

        bindEvents : function(){
            $('#add-new-user').on( 'click',  $.proxy(this.addUserModal,this) );
            $('.delete-user').on( 'click',  $.proxy(this.deleteUserModal,this) );
            $('.update-user').on( 'click',  $.proxy(this.updateUserModal,this) );
        },

        addUserModal : function(e){
            $.ajax({
                type:'GET',
                url: base_url+'/administrator/users/add-new'
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        deleteUserModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type:'GET',
                url: base_url+'/administrator/users/delete-user/'+id
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        updateUserModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type:'GET',
                url: base_url+'/administrator/users/update-user/'+id
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },
    }

    UserManagement.init();
})();
