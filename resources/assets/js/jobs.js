(function($) {
    $(function() {
        $('select[name=status]').select2({
            containerCssClass: 'select2-form-control',
            width: 100
        }).on('change', function(e) {
            var job_id = $(this).attr('data-job_id');
            var status_id = $(this).val();
            var current_status = $('input[name=current_status]').val();
            var url = $('.changePostingStatus').attr('href');

            if(status_id != current_status) {
                $('.changePostingStatus').trigger('click');
            }

        });

        $('#bs-modal-medium').on('shown.bs.modal', function() {
            $('#changeJobStatusForm').on('submit', function(e) {
                alert();
                e.preventDefault();
                var $this = $(this),
                    $token = $this.find('input[name="_token"]').val(),
                    $url = $this.attr('action');

                var $def = $.ajax({
                    headers: {
                        'X-CSRF-TOKEN' : $token
                    },
                    url: $url,
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    beforeSend: function() {
                        $this.find(".btn").prop("disabled", true);
                    }
                });

                $this.find("[data-dismiss='modal']").one("click", function () {
                    $def.abort();
                });

                $def.done(function (res) {
                    $('.form-content').hide();
                    $('.form-success').find('strong.message').html(res.message);
                    $('.form-success').removeClass('hide');

                    setTimeout(function() {
                        window.location.reload();
                    }, 1000);
                });

                return false;
            });
        });
    });
})(jQuery);