(function(){
    window.CareerModal = {
        init : function(){
            this.bindEvents();
        },

        bindEvents : function(){
            $('#add-new-career').on( 'click',  $.proxy(this.addCareerModal,this) );
            $('.delete-career').on( 'click',  $.proxy(this.deleteCareerModal,this) );
            $('.update-career').on( 'click',  $.proxy(this.updateCareerModal,this) );
        },

        addCareerModal : function(e){
            $.ajax({
                type:'GET',
                url: base_url+'/administrator/edit/career/add-new'
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        deleteCareerModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type:'GET',
                url: base_url+'/administrator/edit/career/delete-career/'+id
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        updateCareerModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type:'GET',
                url: base_url+'/administrator/edit/career/update-career/'+id
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },
    }

    CareerModal.init();
})();
