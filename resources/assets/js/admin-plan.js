(function(){
    window.PlanManagement = {
        init : function(){
            this.bindEvents();
        },

        bindEvents : function(){
            $('#add-new-plan').on( 'click',  $.proxy(this.addPlanModal,this) );
            $('.delete-plan').on( 'click',  $.proxy(this.deletePlanModal,this) );
            $('.update-plan').on( 'click',  $.proxy(this.updatePlanModal,this) );
        },

        addPlanModal : function(e){
            $.ajax({
                type:'GET',
                url: base_url+'/administrator/plans/add-new'
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        deletePlanModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type:'GET',
                url: base_url+'/administrator/plans/delete-plan/'+id
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        updatePlanModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type:'GET',
                url: base_url+'/administrator/plans/update-plan/'+id
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },
    }

    PlanManagement.init();
})();
