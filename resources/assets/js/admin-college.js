(function(){
    window.CollegeManagement = {
        init : function(){
            this.bindEvents();
        },

        bindEvents : function(){
            $('#add-new-college').on( 'click',  $.proxy(this.addCollegeModal,this) );
            $('.delete-college').on( 'click',  $.proxy(this.deleteCollegeModal,this) );
            $('.update-college').on( 'click',  $.proxy(this.updateCollegeModal,this) );
        },

        addCollegeModal : function(e){
            $.ajax({
                type:'GET',
                url: base_url+'/administrator/edit/college/add-new'
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        deleteCollegeModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type:'GET',
                url: base_url+'/administrator/edit/college/delete-college/'+id
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        updateCollegeModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type:'GET',
                url: base_url+'/administrator/edit/college/update-college/'+id
            }).done(function(data){
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },
    }

    CollegeManagement.init();
})();
