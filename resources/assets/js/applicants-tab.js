(function(){
    window.Applicant = {
        init : function(){
            this.bindEvents();
        },

        bindEvents : function(){
            $('#view-compatibility-free-user').on( 'click', $.proxy(this.getFreeUserWarning,this) );
            $('#view-compatibility').on( 'click', $.proxy(this.showCompatibilityData,this) );
        },

        getFreeUserWarning : function(e) {
             $.ajax({
                type:'GET',
                url: base_url+'/employer/get-free-user-warning',
            }).done(function(data){  
                if( !$('#unpaid-user-warning').length ){
                    $("body").append(data);
                }
                $('#unpaid-user-warning').modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        showCompatibilityData : function(e) {
            $('#compatibility-data-container').slideToggle();
        }
    }

    Applicant.init();
})();
