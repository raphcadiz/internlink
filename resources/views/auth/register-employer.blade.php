@extends('layouts.app')

@section('content')
<div class="container">
    <form class="signin-form" role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}
        <input type="hidden" name="role_id" value="{{ $role->id }}">
        @include('backend.layouts.flash-message')
        <a href="{{ url('/') }}">
             <img class="center-block login-logo" src="{{ asset('images/login-logo.png') }}">
        </a>
        <br /> <br />
        <div class="animate_wrapper form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
            <input id="first_name" type="text" class="form-control animate_field" name="first_name" value="{{ old('first_name') }}" >
            <span class="animate_label">First Name</span>
        </div>
        <div class="animate_wrapper form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <input id="last_name" type="text" class="form-control animate_field" name="last_name" value="{{ old('last_name') }}" >
            <span class="animate_label">Last Name</span>
        </div>

        <div class="animate_wrapper form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" class="form-control animate_field" name="email" value="{{ old('email') }}" >
            <span class="animate_label">Email</span>
        </div>

        <div class="animate_wrapper form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" class="form-control animate_field" name="password" >
            <span class="animate_label">Password</span>
        </div>

        <div class="animate_wrapper form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <input id="password-confirm" type="password" class="form-control animate_field" name="password_confirmation" >
            <span class="animate_label">Confirm</span>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary signin-btn">
                Signup
            </button>
        </div>
        <div class="note">
            <span>Already have an account? <a class="" href="{{ url('/login/employer') }}">Login Now</a></span>
            <span>Forgot Password? <a class="" href="{{ url('/password/reset') }}">Click Here</a></span>
        </div>
    </form>
</div>
@endsection
