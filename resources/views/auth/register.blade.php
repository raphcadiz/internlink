@extends('layouts.app')

@section('content')
<div class="container">
    <form class="signin-form" role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}
        @include('backend.layouts.flash-message')
        <a href="{{ url('/') }}">
            @if(Request::is('register/employer'))
                <img class="center-block login-logo" src="{{ asset('images/login-logo.png') }}">
            @elseif (Request::is('register/student'))
                <img class="center-block login-logo" src="{{ asset('images/login-logo-1.png') }}">
            @elseif (Request::is('register'))
                 <img class="center-block login-logo" src="{{ asset('images/login-logo.png') }}">
            @endif
        </a>
        <br /> <br />
        <div class="animate_wrapper form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
            <input id="first_name" type="text" class="form-control animate_field" name="first_name" value="{{ old('first_name') }}" >
            <span class="animate_label">First Name</span>
        </div>
        <div class="animate_wrapper form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <input id="last_name" type="text" class="form-control animate_field" name="last_name" value="{{ old('last_name') }}" >
            <span class="animate_label">Last Name</span>
        </div>

        <div class="animate_wrapper form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" class="form-control animate_field" name="email" value="{{ old('email') }}" >
            <span class="animate_label">Email</span>
        </div>

        <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
            <select name="role_id" class="form-control" style="font-weight: 600;">
                @foreach($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->role }}</option>
                @endforeach
            </select>
        </div>

        <div class="animate_wrapper form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" class="form-control animate_field" name="password" >
            <span class="animate_label">Password</span>
        </div>

        <div class="animate_wrapper form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <input id="password-confirm" type="password" class="form-control animate_field" name="password_confirmation" >
            <span class="animate_label">Confirm</span>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary signin-btn">
                Signup
            </button>
        </div>
        <div class="note">
            <span>Already have an account? <a class="" href="{{ url('/login') }}">Login Now</a></span>
            <span>Forgot Password? <a class="" href="{{ url('/password/reset') }}">Click Here</a></span>
        </div>
    </form>
    <!-- <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> -->
</div>
@endsection
