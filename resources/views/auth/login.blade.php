@extends('layouts.app')
@section('content')

<div class="container">
    <form class="signin-form" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        @include('backend.layouts.flash-message')
            @if(Request::is('login/employer'))
                <a href="{{ url('/') }}">
                    <img class="center-block  login-logo" src="{{ asset('images/login-logo.png') }}">
                </a>
            @elseif (Request::is('login/student'))       
                <a href="http://www.internmagic.com/">
                    <img class="center-block  login-logo" src="{{ asset('images/login-logo-1.png') }}">
                </a>
            @elseif (Request::is('login'))
                <a href="{{ url('/') }}">
                    <img class="center-block  login-logo" src="{{ asset('images/login-logo.png') }}">
                </a>
            @endif
        </a>
        <br /><br />

        <div class="animate_wrapper form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" class="form-control animate_field" name="email" value="{{ old('email') }}" >
            <span class="animate_label">Email</span>
        </div>

        <div class="animate_wrapper form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" class="form-control animate_field" name="password" >
            <span class="animate_label">Password</span>
        </div>

        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember"> Remember Me
                </label>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary signin-btn">
                Login
            </button>
        </div>
        <div class="note">
            <span>
                Don't have an account?
                @if(Request::is('login/employer'))
                    <a class="" href="{{ url('/register/employer') }}">Register Now</a>
                @elseif (Request::is('login/student'))
                    <a class="" href="{{ url('/register/student') }}">Register Now</a>
                @elseif (Request::is('login'))
                    <a class="" href="{{ url('/register') }}">Register Now</a>
                @endif 
            </span>
            <span>Forgot Password? <a class="" href="{{ url('/password/reset') }}">Click Here</a></span>
        </div>
    </form>
</div>
@endsection
