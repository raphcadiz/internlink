@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="container">
    <form class="signin-form" role="form" method="POST" action="{{ url('/password/email') }}">
        {{ csrf_field() }}
        @include('backend.layouts.flash-message')
         <img class="center-block login-logo" src="{{ asset('images/login-logo.png') }}">
        <br /><br />
        <p style="text-align:center">
            <strong>Forgot Password</strong><br/>
            <span>Please Enter your email.</span>
        </p>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary signin-btn">
                    <i class="fa fa-btn fa-envelope"></i> Submit
            </button>
        </div>
        <div class="note">
            <span>Already have an account? <a class="" href="{{ url('/login') }}">Login Now</a></span>
        </div>
    </form>
</div>
@endsection
