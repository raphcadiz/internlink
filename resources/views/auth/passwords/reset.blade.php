@extends('layouts.app')

@section('content')
<div class="container">
    <form class="signin-form" role="form" method="POST" action="{{ url('/password/reset') }}">
        {{ csrf_field() }}
        
        <input type="hidden" name="token" value="{{ $token }}">
        @include('backend.layouts.flash-message')
        <img class="center-block" src="{{ asset('images/login-logo.png') }}">
        <br /><br />
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" placeholder="Email">
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" class="form-control" name="password" placeholder="Password">
        </div>

        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confrim Password">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary signin-btn">
                <i class="fa fa-btn fa-refresh"></i> Reset Password
            </button>
        </div>
    </form>
</div>
@endsection
