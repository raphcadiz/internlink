@extends('frontend.layouts.master')

@section('styles')

@stop

@section('content')
<div id="content"><!--content-->
  <section class="container-fluid custom-bg1">
      <div class="container text-center" style="color:#fff; position:relative;">
          <h1>BEST. INTERNS. EVER.</h1>
          <h4 class="custom-size">Hire the best fit for your team and improve talent sourcing and retention</h4>
      </div>
  </section>
  <section class="container-fluid padded text-center">
      <div class="container">
          <div class="col-md-12">
              <h4>Get your free posting FREE!</h4>
              <a href="/register" class="btn btn-md btn-danger btn-beta"><strong>SIGN UP FOR THE BETA</strong></a>
          </div>
      </div>
  </section>
</div><!--content-->
@stop

@section('scripts')

@stop