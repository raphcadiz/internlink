<div class="mobile-menu-container hidden-md hidden-lg">
    <div id="mobile-menu">
        <ul>
            <li class="active"><a href="#">Home</a></li>
            <li><a href="/team">Team</a></li>
            <li><a href="http://www.internmagic.com">Students</a></li>
           <li><a href="/brands">Our Brands</a></li>
        </ul>
    </div>
</div>
<div class="c-banner col-md-12 row text-center"><span>Join the InternLogic Beta Today! <a href="/register/employer"><strong>Click here</strong></a> to get your first posting free!</div>
<div id="header" class="main-navbar container-fluid"><!--header-->

    <div class="container container">
        <div id="logo" class="col-md-2 col-xs-10">
            <a href="{{ url('/') }}"><img src="{{ asset('images/login-logo.png') }}" class="img-responsive" alt="Internlogic" style="max-width: 159px;" /></a>
        </div>
        <div id="menu" class="col-md-8 visible-md visible-lg">
            <ul>
                <li class="active"><a href="#">Home</a></li>
                <li><a href="/team">Team</a></li>
                <li><a href="http://www.internmagic.com">Students</a></li>
               <li><a href="/brands">Our Brands</a></li>
            </ul>
        </div>
        <div class="col-xs-2 hidden-md hidden-lg">
            <a class="mobile-menu-trigger"></a>
        </div>
    </div>
</div><!--header-->