<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">
        <title>
            @section('title')
                Internlink
            @show
        </title>
        <!-- Bootstrap -->
        <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

        <!-- Font Awesome -->
        <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        
        <script type="text/javascript" src="{{ asset('js/frontend/jquery-1.11.0.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/frontend/jquery.mmenu.min.all.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/frontend/script.js') }}"></script>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        @yield('styles')
    </head>
    <body>

        @if(Request::is('student'))
            @include('frontend.layouts.nav')
        @else
            @include('frontend.layouts.top-nav')
        @endif

        @yield('content')

        @include('frontend.layouts.footer')
       
        
        <script type="text/javascript" src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
        @yield('scripts')

    </body>
</html>
