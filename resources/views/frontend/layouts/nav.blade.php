<div class="mobile-menu-container hidden-md hidden-lg">
    <div id="mobile-menu">
        <ul>
            <li><a href="{{ url('student') }}">STUDENTS</a></li>
            <li><a href="">EMPLOYERS</a></li>
            <li><a href="{{ url('login') }}" >LOGIN</a></li>
        </ul>
    </div>
</div>
<div id="header" class="student container-fluid"><!--header-->
    <div class="container">
        <div id="logo" class="col-md-4 col-xs-10">
            <a href="{{ url('/') }}"><img src="{{ asset('images/login-logo.png') }}" class="img-responsive" alt="Internlogic" style="max-width: 159px;" /></a>
        </div>
        <div id="menu" class="col-md-8 visible-md visible-lg">
            <ul>
                <li><a href="{{ url('student') }}">STUDENTS</a></li>
                <li><a href="">EMPLOYERS</a></li>
                <li><a href="{{ url('login') }}" class="btn btn-default">LOGIN</a></li>
            </ul>
        </div>
        <div class="col-xs-2 hidden-md hidden-lg">
            <a class="mobile-menu-trigger"></a>
        </div>
    </div>
</div><!--header-->