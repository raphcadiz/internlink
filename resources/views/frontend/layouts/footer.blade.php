 <div id="footer"><!--footer-->
    <section class="container-fluid text-white padded">
        <div class="container text-white">
            <div class="col-md-2 col-sm-2 col-xs-2"></div>
            <div class="col-md-10 col-sm-12 col-xs-12">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <h3>Product</h3>
                    <ul>
                    <li><a href="">Personalities</a></li>
                    <li><a href="/faq">FAQ</a></li>
                </ul>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <h3>Users</h3>
                    <ul>
                        <li><a href="http://www.internmagic.com">Students</a></li>
                        <li><a href="/">Employers</a></li>
                        <li><a href="{{ url('/login/employer') }}">Log In</a></li>
                        <li><a href="{{ url('/register/employer') }}">Sign Up</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <h3>Company</h3>
                    <ul>
                        <li><a href="/team">Our Team</a></li>
                        <li><a href="/contact">Contact Us</a></li>
                        <li><a href="/privacy">Privacy Policy</a></li>
                        <li><a href="/terms-of-service">Terms of Service</a></li>
                    </ul>
                </div>
<!--                 <div class="col-md-5">
                	<h3>Socials</h3>
                    <p><a href=""><img src="images/icon-fb.png" alt="Facebook" /></a>
                    <a href=""><img src="images/icon-twitter.png" alt="Twitter" /></a>
                    <a href=""><img src="images/icon-linked.png" alt="Linked" /></a></p>
                </div> -->
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <p><a href=""><img src="images/facebook.png" alt="Facebook" /></a>
                <a href=""><img src="images/twitter.png" alt="Twitter" /></a>
                <a href=""><img src="images/linkedin.png" alt="Linked" /></a></p>
            </div>
        </div>
    </section>
    <section class="container-fluid">
        <div class="container">
            <p align="center">&copy; Copyright 2016 <span style="color:#fecc46;">InternLogic</span></p>
        </div>
    </section>
</div><!--footer-->