@extends('frontend.layouts.master')

@section('styles')

@stop

@section('content')

<div class="content" id="contact">
  <section class="padded">
       <div class="container">
          <h2 class="text-center"><strong>Contact Us</strong></h2>
          <div class="h-line"></div>
          <div class="col-md-12">
            <form>
              <div class="form-group">
                <label for="name">Name *</label>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="name">
              </div>
              <div class="form-group">
                <label for="email">Email *</label>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="email">
              </div>
              <div class="form-group">
                <label for="employer_or_student">Are you an employer or student?</label>
              </div>
              <div class="form-group">
                <label class="checkbox-inline"><input type="checkbox" value="employer">Employer</label>
                <label class="checkbox-inline"><input type="checkbox" value="student">Student</label>
              </div>
              <div class="form-group">
                <label for="employer_or_student">Message *</label>
              </div>
              <div class="form-group">
                <textarea class="form-control" rows="5"></textarea>
              </div>
              <button type="submit" class="btn btn-default btn-block btn-md">SEND</button>
            </form>

          </div>
       </div>
  </section>
</div>




@stop

@section('scripts')

@stop