<div class="panel-group" id="accordion-main-1">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title student-faq-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-main-1" href="#student-feature-1">
                      Can I Communicate with Employers?
                    </a>
                </h4>
            </div> <!-- end of panel heading -->

            <div id="student-feature-1" class="panel-collapse collapse in">
                <div class="panel-body">

                </div>
            </div>
        </div> <!-- end of panel default -->

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title student-faq-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-main-1" href="#student-feature-2">
                      Can Employers Communicate with Me?
                    </a>
                </h4>
            </div> <!-- end of panel heading -->

            <div id="student-feature-2" class="panel-collapse collapse">
                <div class="panel-body">

                </div>
            </div>
        </div> <!-- end of panel default -->

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title student-faq-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-main-1" href="#student-feature-3">
                      What Does It Take to Create an Account?
                    </a>
                </h4>
            </div> <!-- end of panel heading -->

            <div id="student-feature-3" class="panel-collapse collapse">
                <div class="panel-body">

                </div>
            </div>
        </div> <!-- end of panel default -->

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title student-faq-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-main-1" href="#student-feature-4">
                     What Kinds of Internships Are Provided?
                    </a>
                </h4>
            </div> <!-- end of panel heading -->

            <div id="student-feature-4" class="panel-collapse collapse">
                <div class="panel-body">

                </div>
            </div>
        </div> <!-- end of panel default -->

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title student-faq-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-main-1" href="#student-feature-5">
                     What is Advanced Compatibility? (seemed too feature based)
                    </a>
                </h4>
            </div> <!-- end of panel heading -->

            <div id="student-feature-5" class="panel-collapse collapse">
                <div class="panel-body">

                </div>
            </div>
        </div> <!-- end of panel default -->

</div>

