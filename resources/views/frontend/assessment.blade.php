<?php 
  $assessment = [
        [
          'title' => 'How Does Internlogic Work?',
          'content' => 'Watch our quick video to learn about how InternLogic can help with your career and internship search.'
        ],
        [
          'title' => 'Why Take a Personality Assessment?',
          'content' => 'At InternLogic, we believe there is more to a candidate than just a resume; we want to help you find a position based on who you are as a person. There are no right or wrong answers - merely an assessment to help you determine the best career types and roles that correlate with your personality type.'
        ],
        [
          'title' => 'How Long Does the Assessment Take?',
          'content' => 'Most students take about 10 -15 minutes to finish the assessment.'
        ],
        [
          'title' => 'How did you come up with these questions?',
          'content' => 'Our questions from the assessment are based on organizational psychology that has been used for 70+ years to provide cognitive insight into each individual’s values, needs, and motivations; hence, providing a roadmap to a successful career and life. Our team of psychologists have worked very hard to frame our psychometric assessment to be concise, accurate, and maybe even a little fun.'
        ],             
        [
          'title' => 'I took the assessment, what’s next?',
          'content' => 'After you’ve taken the assessment, check out your results to learn more about your personality type and careers that are well suited for you. Next, go to the Matches tab to see your top matches based on the assessment. Any roles with the Career Type bolded are extra special because that indicates that you not only a high match for that specific position but also the type of career you may excel at. You can save jobs or apply to positions through the Matches page.'
        ],
  ];

  $matches = [
        [
          "title" => "What are the matches based on?",
          "content" => "Our algorithm matches your assessment results with the personality traits the employer selects and provides you with a compatibility match percentage for each position. "
        ],
        [
          "title" => "What is a good match??",
          "content" => "Anything above 60% is a good match and over 80% is GREAT! Our algorithm scores your assessment answers on a scale of 1 - 100 per questions, assessing a total of 30 personality traits. Due to the multitude of variables, there would be a better chance of winning the lottery 5 times in a row than getting a 100% match with a position."
        ],
        [
          "title" => "What if I don’t like my matches?",
          "content" => "InternLogic is a tool to help you broaden your horizons and explore positions you may not have previously considered. We provide you with your cognitive and personality profile to help you discover the most suitable career path."
        ],
        [
          "title" => "If I am a low percentage match with a role, can I still apply to that job?",
          "content" => "Sure! If you feel strongly about a company or position, you should absolutely apply. Use the messaging feature to address the low match with the employer and explain your dedication to them. The employer will still see your message, will have access to your profile, and can choose to follow up."
        ]        

  ];

  $processing = [
        [
          "title" => "How much is the premium subscription and what do I get? ",
          "content" => 'Our pricing is simple and with student’s budget in mind. For just $5 per month, you will have access to Unlimited Saved Jobs, Unlimited Messages, See Who’s Viewed You, and Advanced Job Compatibility. To subscribe to Premium features, just tap the Try Premium Free link on the top right-hand corner, next to your name or click here  (insert link to premium feature). Note: You can cancel your subscription at anytime. '
        ]       
  ];  
?>
          <!-- The Assessment -->

                  <div class="panel-group" id="accordion-main">


                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <h4 class="panel-title student-faq-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-main" href="#student-assessment">
                                  The Assessment
                                </a>
                            </h4>
                        </div> <!-- end of panel heading -->

                        <div id="student-assessment" class="panel-collapse collapse in">
                            <div class="panel-body">

                            <!--  start -->
                               <div id="assessment"> 
                                  @foreach ($assessment as $key => $value)
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                      <h4 class="panel-title">
                                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#assessment" href="#student-assessment-{{$key}}">
                                              <strong>
                                                {{ $value['title'] }}
                                              </strong>
                                          </a>
                                      </h4>
                                  </div> <!-- end of panel heading -->
                                  <div id="student-assessment-{{$key}}" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        {{ $value['content'] }}
                                      </div>
                                  </div>                  
                               </div>
                                  @endforeach
                              </div>


                            </div> <!-- end of panel-body -->
                        </div>

                    </div>


                  <!-- The Matches -->

                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <h4 class="panel-title student-faq-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-main" href="#student-matches">
                                  The Matches
                                </a>
                            </h4>
                        </div> <!-- end of panel heading -->

                        <div id="student-matches" class="panel-collapse collapse">
                            <div class="panel-body">
                            <!--  start -->
                              <div id="matches"> 
                                  @foreach ($matches as $key => $value)
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                      <h4 class="panel-title">
                                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#matches" href="#student-matches-{{$key}}">
                                              <strong>
                                                {{ $value['title'] }}
                                              </strong>
                                          </a>
                                      </h4>
                                  </div> <!-- end of panel heading -->
                                  <div id="student-matches-{{$key}}" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        {{ $value['content'] }}
                                      </div>
                                  </div>                  
                               </div>
                                  @endforeach
                            </div>
                         
                            </div> <!-- end of panel-body -->
                        </div>

                    </div>

                    <!-- Prices & Processing -->

                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <h4 class="panel-title student-faq-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-main" href="#student-processing">
                                  Prices & Processing
                                </a>
                            </h4>
                        </div> <!-- end of panel heading -->

                        <div id="student-processing" class="panel-collapse collapse">
                            <div class="panel-body">
                            <!--  start -->
                              <div id="processing"> 
                                  @foreach ($processing as $key => $value)
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                      <h4 class="panel-title">
                                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#processing" href="#student-processing-{{$key}}">
                                              <strong>
                                                {{ $value['title'] }}
                                              </strong>
                                          </a>
                                      </h4>
                                  </div> <!-- end of panel heading -->
                                  <div id="student-processing-{{$key}}" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        {{ $value['content'] }}
                                      </div>
                                  </div>                  
                               </div>
                                  @endforeach
                            </div>
                         
                            </div> <!-- end of panel-body -->
                        </div>

                    </div>
                  </div> <!-- end of accordion-main -->