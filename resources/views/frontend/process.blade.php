
      <!-- The Process -->

        <div class="panel-group" id="accordion-main">


          <div class="panel panel-default">

              <div class="panel-heading">
                  <h4 class="panel-title student-faq-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-main" href="#student-assessment">
                       Why InternLogic for Business?
                      </a>
                  </h4>
              </div> <!-- end of panel heading -->

              <div id="student-assessment" class="panel-collapse collapse in">
                  <div class="panel-body">
                    Watch our quick video to learn about how InternLogic can help find more suitable interns. 
                  </div> <!-- end of panel-body -->
              </div>

          </div>


        <!-- The Matches -->

          <div class="panel panel-default">

              <div class="panel-heading">
                  <h4 class="panel-title student-faq-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-main" href="#student-process">
                        The Process
                      </a>
                  </h4>
              </div> <!-- end of panel heading -->

              <div id="student-process" class="panel-collapse collapse">
                  <div class="panel-body">
                  <!--  start -->
                    <div id="process"> 
                        @foreach ($process as $key => $value)
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#process" href="#student-process-{{$key}}">
                                    <strong>
                                      {{ $value['title'] }}
                                    </strong>
                                </a>
                            </h4>
                        </div> <!-- end of panel heading -->
                        <div id="student-process-{{$key}}" class="panel-collapse collapse">
                            <div class="panel-body">
                              {{ $value['content'] }}
                            </div>
                        </div>                  
                     </div>
                        @endforeach
                  </div>
               
                  </div> <!-- end of panel-body -->
              </div>

          </div>

          <!-- Prices & Processing -->

          <div class="panel panel-default">

              <div class="panel-heading">
                  <h4 class="panel-title student-faq-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-main" href="#student-processing">
                        Prices & Processing
                      </a>
                  </h4>
              </div> <!-- end of panel heading -->

              <div id="student-processing" class="panel-collapse collapse">
                  <div class="panel-body">
                  <!--  start -->
                    <div id="processing"> 
                        @foreach ($processing as $key => $value)
                      <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#processing" href="#student-processing-{{$key}}">
                                    <strong>
                                      {{ $value['title'] }}
                                    </strong>
                                </a>
                            </h4>
                        </div> <!-- end of panel heading -->
                        <div id="student-processing-{{$key}}" class="panel-collapse collapse">
                            <div class="panel-body">
                              {{ $value['content'] }}
                            </div>
                        </div>                  
                     </div>
                        @endforeach
                  </div>
               
                  </div> <!-- end of panel-body -->
              </div>

          </div>

        </div> <!-- end of accordion-main -->