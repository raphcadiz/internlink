@extends('frontend.layouts.master')

@section('styles')

@stop

@section('content')

<div class="content" id="faq">
  <section class="padded">
       <div class="container">
          <h2 class="text-center green-header"><strong>Frequently Asked Questions (FAQs)</strong></h2> 
          <div class="h-line"></div>
          <div class="col-md-12">

            <div class="row">
              <h4 class="green-header"><strong>STUDENTS</strong></h4>
              <ol type="1">
                <li>
                  <a href="#">How Does Internlogic Work? (Video)</a>
                </li>
              </ol>
            </div>
            <div class="row">
              <h4><strong>The Assestment:</strong></h4>
              <ol type="1">
                <li>
                  <a href="#">Why take a personality assesstment?</a>
                </li>
                <li>
                  <a href="#">How long does the assesstment take?</a>
                </li>
                <li>
                  <a href="#">How did you come up with the assessment questions?</a>
                </li>
                <li>
                  <a href="#">I took the assessment, what's next?</a>
                </li>
              </ol>
            </div>
            <div class="row">
              <h4><strong>The Matches:</strong></h4>
              <ol type="1">
                <li>
                  <a href="#">What are the matches based on?</a>
                </li>
                <li>
                  <a href="#">What is a good match?</a>
                </li>
                <li>
                  <a href="#">What if I don't like my matches?</a>
                </li>
                <li>
                  <a href="#">If I am low on percentage match with a role, can I still apply to that job?</a>
                </li>
              </ol>
            </div>
            <div class="row">
              <h4><strong>Payments:</strong></h4>
              <ol type="1">
                <li>
                  <a href="#">What does the Premium account include?</a>
                </li>
              </ol>
            </div>

            <div class="row">
              <h4 class="green-header"><strong>EMPLOYERS</strong></h4>
              <ol type="1">
                <li>
                  <a href="#">Why Internlogic for Business? (Video)</a>
                </li>
              </ol>
            </div>
            <div class="row">
              <h4><strong>The Process:</strong></h4>
              <ol type="1">
                <li>
                  <a href="#">Why factor in a personality assessment for candidate sourcing?</a>
                </li>
                <li>
                  <a href="#">What are the matches based on?</a>
                </li>
                <li>
                  <a href="#">What is a good match?</a>
                </li>
              </ol>
            </div>
            <div class="row">
              <h4><strong>Payments &amp; Processing:</strong></h4>
              <a href="#">How Much Does Internlogic Costs?</a>
            </div>
            <br/>

            <div class="row">
              <h4 class="green-header"><strong>STUDENTS</strong></h4>
              <ol type="1" id="faq-content">
                <br/>
                <h4><strong>The Assessment:</strong></h4>
                <br/>
                <li>
                  <p id="student-work"><strong>How Does Internlogic Work?</strong></p>
                  <p>Watch our quick video to learn about how Internlogic can help with your career and internship search.</p>
                </li>
                <li>
                  <p><strong>Why Take Personality Assessment</strong></p>
                  <p>At Internlogic, we believe there is more to a candidate than just a resume; we want to help you find a postion based on who you are as a person. There are no right or wrong answers - merely an assessment to help you determine the best career types and roles that correlate with your personality type.</p>
                </li>
                <li>
                  <p><strong>How Long Does the Assessment Take?</strong></p>
                  <p>Most students take about 10 - 15 minutes to finish the assessment.</p>
                </li>
                <li>
                  <p><strong>How did you come up with these questions? </strong></p>
                  <p>Our questions from the assessment are based on organizational psychology that has been used for 70+ years to provide cognitive insight into each individual’s values, needs, and motivations; hence, providing a roadmap to a successful career and life. Our team of psychologists have worked very hard to frame our psychometric assessment to be concise, accurate, and maybe even a little fun.</p>
                </li>
                <li>
                  <p><strong>I took the assessment, what’s next?  </strong></p>
                  <p>After you’ve taken the assessment, check out your results to learn more about your personality type and careers that are well suited for you. Next, go to the Matches tab to see your top matches based on the assessment. Any roles with the Career Type bolded are extra special because that indicates that you not only a high match for that specific position but also the type of career you may excel at. You can save jobs or apply to positions through the Matches page.</p>
                </li>
                <br/>
                <h4><strong>The Matches:</strong></h4>
                <br/>
                <li>
                  <p><strong>What are the matches based on?</strong></p>
                  <p>Our algorithm matches your assessment results with the personality traits the employer selects and provides you with a compatibility match percentage for each position. </p>
                </li>
                <li>
                  <p><strong>What is a good match? </strong></p>
                  <p>Anything above 60% is a good match and over 80% is GREAT! Our algorithm scores your assessment answers on a scale of 1 - 100 per questions, assessing a total of 30 personality traits. Due to the multitude of variables, there would be a better chance of winning the lottery 5 times in a row than getting a 100% match with a position. </p>
                </li>
                <li>
                  <p><strong>What if I don’t like my matches? </strong></p>
                  <p>InternLogic is a tool to help you broaden your horizons and explore positions you may not have previously considered. We provide you with your cognitive and personality profile to help you discover the most suitable career path.</p>
                </li>
                <li>
                  <p><strong>If I am a low percentage match with a role, can I still apply to that job? </strong></p>
                  <p>Sure! If you feel strongly about a company or position, you should absolutely apply. Use the messaging feature to address the low match with the employer and explain your dedication to them. The employer will still see your message, will have access to your profile, and can choose to follow up.</p>
                </li>
              </ol>
              <ol type="1" id="faq-content-2">
                <br/>
                <h4><strong>Payments &amp; Processing:</strong></h4>
                <br/>
                <li>
                  <p><strong>How much is the premium subscription and what do I get? </strong></p>
                  <p>Our pricing is simple and with student’s budget in mind. For just $5 per month, you will have access to Unlimited Saved Jobs, Unlimited Messages, See Who’s Viewed You, and Advanced Job Compatibility.</p>
                  <p>  
                  To subscribe to Premium features, just tap the Try Premium Free link on the top right-hand corner, next to your name or click here  (insert link to premium feature). Note: You can cancel your subscription at anytime. 
                  </p>
                </li>
              </ol>
            </div>


            <div class="row">
              <h4 class="green-header"><strong>EMPLOYER</strong></h4>
              <ol type="1" id="faq-content-3">
                <br/>
                <h4><strong>The Process:</strong></h4>
                <br/>
                <li>
                  <p id="student-work"><strong>Why factor in a personality assessment for candidate sourcing?</strong></p>
                  <p>There is more to a compatible candidate than just their resume or GPA. Each hiring manager and team has a distinct personality and a newcomer must fit into that dynamic. We have learned that employers prioritize personality and the fit within the your team over skills and achievements. We give you cognitive tools to guide you in finding the most suitable candidate.</p>
                </li>
                <li>
                  <p><strong>What are the matches based on?</strong></p>
                  <p>Each student takes our personality assessment. Our algorithm matches your selected traits for a job posting with the student assessment results to provide you with a compatibility match percentage</p>
                </li>
                <li>
                  <p><strong>What is a good match?</strong></p>
                  <p>Anything above 60% is a good match and over 80% is relatively rare. Our algorithm scores your assessment answers on a scale of 1 - 100 per questions, assessing a total of 30 personality traits. Due to the multitude of variables, there would be a better chance of winning the lottery 5 times in a row than getting a 100% match with a student. </p>
                </li>
              </ol>
              <ol type="1" id="faq-content-4">
                <br/>
                <h4><strong>Payments &amp; Processing:</strong></h4>
                <br/>
                <li>
                  <p><strong>How Much Does InternLogic Cost? </strong></p>
                  <p>Each Active Posting is $50 per month and you can cancel your subscription at anytime. We offer discounted rates if you bundle up multiple postings. You can find our pricing breakdown here. - insert link to buying a posting. </p>
                </li>
              </ol>
            </div>


          </div>
        </div>
  </section>
</div>  






@stop

@section('scripts')

@stop