@extends('frontend.layouts.master')

@section('styles')

@stop

@section('content')

<div class="content" id="team">
  <section class="padded">
       <div class="container">
       		<h2 class="text-center"><strong>Meet the Team</strong></h2>	
       		<div class="h-line"></div>
       		<div class="col-md-12">
       		<div class="row">
       			<div class="col-md-4 text-center">
       				<img src="{{ asset('images/team/Asset 14.png') }}" >
       				<h4><strong>Andrew Diaz</strong></h4>
       				<h6><strong>Founder &amp; CEO</strong></h6>
       				<p>The original idea guy who is earning a 
       				Ph.D. in wearing many hats</p>
       			</div>
       			<div class="col-md-4 text-center">
       				<img src="{{ asset('images/team/Asset 19.png') }}" >
       				<h4><strong>RJ Tolson</strong></h4>
       				<h6><strong>Chief Marketing Officer</strong></h6>
       				<p>International business owner, book writer and marketing guru</p>
       			</div>
       			<div class="col-md-4 text-center">
       				<img src="{{ asset('images/team/Asset 16.png') }}" >
       				<h4><strong>Kevin Sommerville</strong></h4>
       				<h6><strong>Board of Advisors</strong></h6>
       				<p>Ph.D., ABPP, and owner of Sommerville Partners</p>
       			</div>
       		</div>
       		<div class="row">
       			<div class="col-md-4 text-center">
       				<img src="{{ asset('images/team/Asset 12.png') }}" >
       				<h4><strong>Jim Thompson</strong></h4>
       				<h6><strong>Board of Advisors</strong></h6>
       				<p>Ph.D., M.B.A., the instrument creator and psychology master</p>
       			</div>
       			<div class="col-md-4 text-center">
       				<img src="{{ asset('images/team/Asset 20.png') }}" >
       				<h4><strong>Peyton Fouts</strong></h4>
       				<h6><strong>Development Manager</strong></h6>
       				<p>The owner of Ouibox Pro, our expert development resource</p>
       			</div>
       			<div class="col-md-4 text-center">
       				<img src="{{ asset('images/team/Asset 18.png') }}" >
       				<h4><strong>Raph</strong></h4>
       				<h6><strong>Developer</strong></h6>
       				<p>The lead developer behind the magic</p>
       			</div>
       		</div>
       		<div class="row">
       			<div class="col-md-4 text-center">
       				<img src="{{ asset('images/team/Asset 17.png') }}" >
       				<h4><strong>Prithvi Nobuth</strong></h4>
       				<h6><strong>Board of Advisors</strong></h6>
       				<p>M.B.A., and consulting partner</p>
       			</div>
       			<div class="col-md-4 text-center">
       				<img src="{{ asset('images/team/Asset 13.png') }}" >
       				<h4><strong>Daniel Duran</strong></h4>
       				<h6><strong>Development Manager</strong></h6>
       				<p>Ph.D., M.B.A., professor and small business owner</p>
       			</div>
       			<div class="col-md-4 text-center">
       				<img src="{{ asset('images/team/Asset 15.png') }}" >
       				<h4><strong>Jay Velasco</strong></h4>
       				<h6><strong>Social Media Manager</strong></h6>
       				<p>Our social media posts don't write themselves</p>
       			</div>
       		</div>
       		</div>
       </div>
  </section>
</div>















@stop

@section('scripts')

@stop