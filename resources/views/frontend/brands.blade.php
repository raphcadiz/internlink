@extends('frontend.layouts.master')

@section('styles')

@stop

@section('content')

<div class="content" id="brands">
  <section class="padded">
       <div class="container">
       	<h2 class="text-center"><strong>Our Brands</strong></h2>   
              <h4 class="text-center">Helping students and employers like never before</h4>  
              <div class="h-line"></div>
              <div class="row">
                     <div class="col-md-4">
                            <img src="{{ asset('images/login-logo-1.png') }}" class="brands-logo">
                     </div>
                     <div class="col-md-8">
                            <h3>Student Solution</h3>
                            <strong>INTERNSHIPS BASED ON PERSONALITY</strong>
                            <p>Using mainly magic and a little bit of science, we are able to match students careers based on their personality. Don't worry about the question. "what are doing after graduation?" We got you covered.</p>
                     </div>
              </div>
              <div class="h-line thin"></div>
              <div class="row">
                     <div class="col-md-4">
                            <img src="{{ asset('images/logo-1.png') }}" class="brands-logo">
                     </div>
                     <div class="col-md-8">
                            <h3>Employer Solution</h3>
                            <strong>HIRE TALENT TALORED FOR YOU</strong>
                            <p>Looking to hire the top talken that is perfect fit for your company? By using InterLogic we leverage our fantastic talent pool from InternMagic adn provide you with the best candidates for the job.</p>
                     </div>
              </div>
              <div class="h-line thin"></div>
              <div class="row">
                     <div class="col-md-4">
                            <img src="{{ asset('images/SparkLift Logo.png') }}" class="brands-logo">
                     </div>
                     <div class="col-md-8">
                            <h3>Additional Services</h3>
                            <strong>SERVICES TO BRING YOU TO THE ENXT LEVEL</strong>
                            <p>Using Sparklift's services will allow students to utilize resources lik <strong>resume</strong> and <strong>cover letter prep</strong>, see <strong>virtual job walkthroughs</strong> and much more. Employers will be able to utilize <strong>talen consulting services</strong> to save time and of course money.</p>
                     </div>
              </div>
       </div>
  </section>
</div>




@stop

@section('scripts')

@stop