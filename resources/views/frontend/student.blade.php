@extends('frontend.layouts.master')

@section('styles')

@stop

@section('content')

<div id="content" class="student"><!--content-->
  <section class="container-fluid custom-bg2 section-1">
      <div class="container text-center" style=" top: -375px;
    position: relative;">
        <h1>Best. Interships. Ever.</h1>
        <h4>Matching YOU with internships based on YOUR personality!</h4>
          <p><a href="{{ url('register') }}" class="btn btn-default btn-md">SIGN UP</a> <a href="" class="btn btn-info btn-md">PLAY VIDEO</a></p>
      </div>
  </section>
  <div class="h-line"></div>
  <section class="padded section-2">
      <div class="container text-center">
      	  <h1>Get matched. Get connected. Get ahead.</h1>
          <h3>Find the perfect career for you</h3>
        <div class="row padded">
          <div class="laptop-img">
          </div>
        </div>
      </div>
  </section>
  <div class="h-line"></div>
  <section class="padded section-3">
      <div class="container text-center">
      	  <h1>How it works</h1>
          <h3>Our process is pretty complicated...it's 3 whole steps</h3>
        <div class="row padded">
          <div class="col-md-4 text-center">
          	<img src="{{ asset('images/1.png') }}" class="img-responsive center-block" alt="Step-1" />
          	<h4><strong>Quick Assestment</strong></h4>
          	<p>Use 10 minutes to take an in-depth,
          	unbiased assestment to secure your
          	future and find some sweet compatible
          	positions.</p>
          </div>
          <div class="col-md-4 text-center">
          	<img src="{{ asset('images/2.png') }}" class="img-responsive center-block" alt="Step-2" />
          	<h4><strong>Get Matched</strong></h4>
          	<p>Get a compatibility percentage for each
          	position based on an analysis of your
          	unique personality results and the
          	traits selected by the employer</p>
          </div>
          <div class="col-md-4 text-center">
          	<img src="{{ asset('images/3.png') }}" class="img-responsive center-block" alt="Step-3" />
          	<h4><strong>Get The Job</strong></h4>
          	<p>Let's put this fancy algorithm to work
          	and get you the internship of your
          	dreams!</p>
          </div>
        </div>          
      </div>
  </section>
  <div class="h-line"></div>
  <section class="padded section-4">
      <div class="container text-center">
      	  <h1>Features, gadgets and more</h1>
          <h3>We give you so much tech, Tony Stark would be jealous</h3>

        <div class="row padded">

          <div class="col-md-5 col-sm-12 col-xs-12 text-center feature-box pull-left">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <img src="{{ asset('images/bolt.png') }}" class="img-responsive center-block" alt="Step-1" />
            </div>
          	<div class="col-md-9 col-sm-9 col-xs-9 text-left">
              <div class="row">
                <h4><strong>Save Time</strong></h4>
              </div>
              <div class="row">
                <p>Instant Apply - Use your InternLogic <br/>
                profile to apply to jobs at record speeds.
                </p>
              </div>
            </div>
          </div>

          <div class="col-md-5 col-sm-12 col-xs-12 text-center feature-box pull-right">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <img src="{{ asset('images/atom.png') }}" class="img-responsive center-block" alt="Step-2" />
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 text-left">
              <div class="row">
                <h4><strong>Advance Compatibility</strong></h4>
              </div>
              <div class="row">
                <p>View exactly how your personality<br/>
                matches up with a certain job, trait by trait.
                </p>
              </div>
            </div>
          </div>

        </div>

        <div class="row padded">

          <div class="col-md-5 col-sm-12 col-xs-12 text-center feature-box pull-left">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <img src="{{ asset('images/chat.png') }}" class="img-responsive center-block" alt="Step-3" />
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 text-left">
              <div class="row">
                <h4><strong>Messaging Hub</strong></h4>
              </div>
              <div class="row">
                <p>Don't let your next job get lost in a flood of <br/>
                emails. Use our messaging feature to quickly communicate.
                </p>
              </div>
            </div>
          </div>

          <div class="col-md-5 col-sm-12 col-xs-12 text-center feature-box pull-right">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <img src="{{ asset('images/binoculars.png') }}" class="img-responsive center-block" alt="Step-4" />
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 text-left">
              <div class="row">
                <h4><strong>See Who's Viewing You</strong></h4>
              </div>
              <div class="row">
                <p>See which employers are viewing your<br/>
                profile. Check out their postings and start a conversation.
                </p>
              </div>
            </div>
          </div>

        </div>

        <div class="row padded">

          <div class="col-md-5 col-sm-12 col-xs-12 text-center feature-box pull-left">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <img src="{{ asset('images/email.png') }}" class="img-responsive center-block" alt="Step-5" />
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 text-left">
              <div class="row">
                <h4><strong>Get Invited</strong></h4>
              </div>
              <div class="row">
                <p>Create an outstanding profile so <br/>
                employers invite you before you even apply for position!
                </p>
              </div>
            </div>
          </div>

          <div class="col-md-5 col-sm-12 col-xs-12 text-center feature-box pull-right">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <img src="{{ asset('images/filter-old.png') }}" class="img-responsive center-block" alt="Step-6" />
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 text-left">
              <div class="row">
                <h4><strong>Make It Yours</strong></h4>
              </div>
              <div class="row">
                <p>Control your internship search.<br/>
                What good is all this great info if you can't search through it effectively?
                </p>
              </div>
            </div>
          </div>

        </div>

      </div>
  </section>
  <div class="h-line"></div>
  <section class="padded section-5">
      <div class="container text-center">
          <h1>Yeah, we use science</h1>
          <h3>We do all the heavy lifting so you don't have to</h3>
        <div class="row padded">
          <div class="col-md-7 col-sm-12 col-xs-12 text-left">
            <p>Our assestment measures 30 different <br/>personality traits. Gender, ethnicity, or pedigree?<br/>
            Our algorithm doesn't see any of that... it only <br/>cares about your personality and how to find<br/>
            the most compatible internships for you. We use <br/>decades of research and over 40 years of <br/>experience
            from our team of psychologists to <br/>create a personality assesstment that is fun, <br/>accurate, and insightful.
            </p>
          </div>
          <div class="col-md-4">
             <img src="{{ asset('images/biology.png') }}" class="img-responsive center-block" alt="biology" />
          </div>
        </div>
      </div>
  </section>
  <div class="h-line"></div>
  <section class="padded section-5">
      <div class="container text-center">
          <h1>Your personality. Your Matches. Your future.</h1>
          <h3>Create your free profile and start getting opportunities today!</h3>
          <a href="" class="btn btn-default btn-md">SIGN UP FOR FREE</a>
      </div>
  </section>
  <div class="h-line"></div>


</div> <!-- end of content -->







@stop

@section('scripts')

@stop