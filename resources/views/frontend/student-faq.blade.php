@extends('frontend.layouts.master')

@section('styles')

@stop

@section('content')

<div class="content" id="student-faq">
  <section class="padded">
  		<div class="container">

      <h1>Students FAQ</h1>

      <div class="col-md-4 col-sm-4">
          <ul class="nav nav-tabs tabs-left">
            <li class="active"><a href="#faq" data-toggle="tab">Frequently Asked Questions</a>
            </li>
            <li><a href="#feature" data-toggle="tab">Feature Based Questions</a>
            </li>
            </li>
          </ul>
      </div>

      <div class="col-md-8 col-sm-8">
        <div class="tab-content">

            <div class="tab-pane active" id="faq">

                @include('frontend.assessment')
      
            </div> <!-- end of #faq -->

            <!-- Second tab -->
            <div class="tab-pane" id="feature">

                @include('frontend.student-feature')

            </div> <!-- end of #feature -->


        </div> <!-- end of tab content -->
      </div>

  		</div> <!-- end of container -->
  </section>
</div>










@stop

@section('scripts')

@stop