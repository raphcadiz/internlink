@extends('frontend.layouts.master')

@section('styles')

@stop

@section('content')

<?php 
  $process = [
        [
          'title' => 'Why factor in a personality assessment for candidate sourcing?',
          'content' => 'There is more to a compatible candidate than just their resume or GPA. Each hiring manager and team has a distinct personality and a newcomer must fit into that dynamic. We have learned that employers prioritize personality and the fit within the your team over skills and achievements. We give you cognitive tools to guide you in finding the most suitable candidate.'
        ],
        [
          'title' => 'What are the matches based on?',
          'content' => 'Each student takes our personality assessment. Our algorithm matches your selected traits for a job posting with the student assessment results to provide you with a compatibility match percentage. '
        ],
        [
          'title' => 'What is a good match? ',
          'content' => 'Anything above 60% is a good match and over 80% is relatively rare. Our algorithm scores your assessment answers on a scale of 1 - 100 per questions, assessing a total of 30 personality traits. Due to the multitude of variables, there would be a better chance of winning the lottery 5 times in a row than getting a 100% match with a student. '
        ]
  ];

  $matches = [
        [
          "title" => "What are the matches based on?",
          "content" => "Our algorithm matches your assessment results with the personality traits the employer selects and provides you with a compatibility match percentage for each position. "
        ],
        [
          "title" => "What is a good match??",
          "content" => "Anything above 60% is a good match and over 80% is GREAT! Our algorithm scores your assessment answers on a scale of 1 - 100 per questions, assessing a total of 30 personality traits. Due to the multitude of variables, there would be a better chance of winning the lottery 5 times in a row than getting a 100% match with a position."
        ],
        [
          "title" => "What if I don’t like my matches?",
          "content" => "InternLogic is a tool to help you broaden your horizons and explore positions you may not have previously considered. We provide you with your cognitive and personality profile to help you discover the most suitable career path."
        ],
        [
          "title" => "If I am a low percentage match with a role, can I still apply to that job?",
          "content" => "Sure! If you feel strongly about a company or position, you should absolutely apply. Use the messaging feature to address the low match with the employer and explain your dedication to them. The employer will still see your message, will have access to your profile, and can choose to follow up."
        ]        

  ];

  $processing = [
        [
          "title" => "How Much Does InternLogic Cost?",
          "content" => 'Each Active Posting is $50 per month and you can cancel your subscription at anytime. We offer discounted rates if you bundle up multiple postings. You can find our pricing breakdown here. - insert link to buying a posting. '
        ]       
  ];  
?>

<div class="content" id="employers-faq">
  <section class="padded">
  		<div class="container">

         <h1>Employers FAQ</h1>

          <div class="col-md-4 col-sm-4">
              <ul class="nav nav-tabs tabs-left">
                <li class="active"><a href="#faq" data-toggle="tab">Frequently Asked Questions</a>
                </li>
                <li><a href="#feature" data-toggle="tab">Feature Based Questions</a>
                </li>
                </li>
              </ul>
          </div>

          <div class="col-md-8 col-sm-8">
            <div class="tab-content">

                <div class="tab-pane active" id="faq">

                    @include('frontend.process')
          
                </div> <!-- end of #faq -->

                <!-- Second tab -->
                <div class="tab-pane" id="feature">

                @include('frontend.employers-feature')

                </div> <!-- end of #feature -->


            </div> <!-- end of tab content -->
          </div>


  		</div> <!-- end of container -->
  </section>
</div>










@stop

@section('scripts')

@stop