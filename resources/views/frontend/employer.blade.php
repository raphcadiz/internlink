@extends('frontend.layouts.master')

@section('styles')

@stop

@section('content')

<div id="content" class="employer student">
  <section class="container-fluid custom-bg1">
      <div class="container text-center" style="color:#fff; position:relative;">
          <h1>BEST. INTERNS. EVER.</h1>
          <h4 class="custom-size">Hire the best fit for your team and improve talent sourcing and retention</h4>
          <!-- <a href="#vid-section" id="play-video" class="btn btn-info btn-md">PLAY VIDEO</a></p> -->
      </div>
  </section>
  <section class="container-fluid">
    <div class="container text-center" style="position:relative;">
      <br />
      <p>Get your first posting FREE</p>
      <p><a href="{{ url('register') }}" class="btn btn-default btn-md btn-custom">SIGN UP FOR THE BETA</a>
    </p>
  </section>
  <section id="vid-section" class="padded section-2">
      <div class="container text-center">
      	  <h1>Get matched. Get connected. Get ahead.</h1>
          <h3>Use the power of data and predictive analytics to hire the best match</h3>
        <div class="row padded">
          <img src="{{ asset('images/business-screen.png') }}" style="max-width: 700px" />
        </div>
      </div>
  </section>
  <div class="h-line"></div>
  <section class="padded section-5">
      <div class="container text-center">
          <h1>Why not use a typical job board?</h1>
          <p>For decades employers have resorted to using GPA and test scores to sort<br>through hundreds of resumes, even though these are not benchmarks metrics for quality candidates. With Interlogic, you can proactively source candidates<br>and utilize predictive analytics to decrease the guesswork in the hiring process.</p>
      </div>
  </section>
  <div class="h-line"></div>
  <section class="padded section-3">
      <div class="container text-center">
      	  <h1>How it works</h1><br/>
          <h3>We like to keep things simple and stick with classic 1-2-3</h3>
          <br/><br/>
        <div class="row padded col-md-offset-2  ">
          <div class="col-md-3 text-center" style="margin-right: 35px;">
          	<img src="{{ asset('images/1.png') }}" class="img-responsive center-block" alt="Step-1" />
          	<p><strong style="font-size: 25px;">Post</strong></p>
          	<p class="summary" style="padding: padding: 0px 20px;">Select the personality traits you want for a specific role with your other requirements</p>
          </div>
          <div class="col-md-3 text-center" style="margin-right: 35px;">
          	<img src="{{ asset('images/2.png') }}" class="img-responsive center-block" alt="Step-2" />
          	<p><strong style="font-size: 25px;">Match</strong></p>
          	<p class="summary" style="padding: padding: 0px 20px;">Our algorithm provide you with a match percentage with each student in our talent pool</p>
          </div>
          <div class="col-md-3 text-center" style="margin-right: 35px;">
          	<img src="{{ asset('images/3.png') }}" class="img-responsive center-block" alt="Step-3" />
          	<p><strong style="font-size: 25px;">Hire</strong></p>
          	<p class="summary" style="padding: padding: 0px 20px;">Use our platform for real-time applicant tracking, resume access, and  messaging solutions to complete the hiring process</p>
          </div>
        </div>          
      </div>
  </section>
  <div class="h-line"></div>
  <section class="padded section-4">
      <div class="container text-center">
      	  <h1>Features, gadgets and more</h1>
          <h3>Use our suite of tools to take your talent sourcing to a new </h3>

        <div class="row padded">

          <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-2 text-center feature-box ">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <img src="{{ asset('images/bolt.png') }}" class="img-responsive center-block" alt="Step-1" />
            </div>
          	<div class="col-md-9 col-sm-9 col-xs-9 text-left">
              <div class="row">
                <h4>Real-Time Dashboard</h4>
              </div>
              <div class="row">
                <p>Get a quick view of applicants to track the<br>
                progress of each posting and have a realt-<br> time applicant tracking system
                </p>
              </div>
            </div>
          </div>

          <div class="col-md-4 col-sm-12 col-xs-12 text-center feature-box">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <img src="{{ asset('images/atom.png') }}" class="img-responsive center-block" alt="Step-2" />
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 text-left">
              <div class="row">
                <h4>Advance Compatibility</h4>
              </div>
              <div class="row">
                <p>View exactly how your personality<br/>
                matches up with a certain job, trait by trait.
                </p>
              </div>
            </div>
          </div>

        </div>

        <div class="row padded">

          <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-2 text-center feature-box ">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <img src="{{ asset('images/chat.png') }}" class="img-responsive center-block" alt="Step-3" />
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 text-left">
              <div class="row">
                <h4>Messaging Solution</h4>
              </div>
              <div class="row">
                <p>Use our message platform to make sure <br/>
               candidates correspondence doesn't get lost in emails
                </p>
              </div>
            </div>
          </div>

          <div class="col-md-4 col-sm-12 col-xs-12 text-center feature-box">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <img src="{{ asset('images/binoculars.png') }}" class="img-responsive center-block" alt="Step-4" />
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 text-left">
              <div class="row">
                <h4>Job Amplification</h4>
              </div>
              <div class="row">
                <p>We send notify students to amplify<br/>
                exposure of open positions and increase<br/>
                the awareness of your company's brand
                </p>
              </div>
            </div>
          </div>

        </div>

        <div class="row padded">

          <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-2 text-center feature-box ">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <img src="{{ asset('images/email.png') }}" class="img-responsive center-block" alt="Step-4" />
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 text-left">
              <div class="row">
                <h4>Unlimited Invites to Apply</h4>
              </div>
              <div class="row">
                <p>Access our entire database to view<br/>
                profiles that match your needs and invite<br/>
                candidates to apply to your position
                </p>
              </div>
            </div>
          </div>

          <div class="col-md-4 col-sm-12 col-xs-12 text-center feature-box">
            <div class="col-md-3 col-sm-3 col-xs-3">
              <img src="{{ asset('images/filter-old.png') }}" class="img-responsive center-block" alt="Step-6" />
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 text-left">
              <div class="row">
                <h4>Candidate Filters</h4>
              </div>
              <div class="row">
                <p>Source candidates using our various filters<br/>
                including compatibility, GPA, Year and many more
                </p>
              </div>
            </div>
          </div>

        </div>

      </div>
  </section>
  <div class="h-line"></div>
<section class="padded section-5">
  <div class="container text-center">
      <h1>Yeah, we use science</h1>
      <h3>We do all the heavy lifting so you don't have to</h3>
    <div class="row padded">
      <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1 text-left inline-info">
        <p>Our assestment measures 30 different personality traits.<br/> Gender, ethnicity, or pedigree?
        Our algorithm doesn't see any of that... it only cares about your personality and how to find
        the most compatible internships for you. We use decades of research and over 40 years of experience
        from our team of psychologists to create a personality assesstment that is fun, accurate, and insightful.
        </p>
      </div>
      <div class="col-md-3 inline-info">
         <img src="{{ asset('images/biology.png') }}" class="img-responsive center-block" alt="biology" />
      </div>
    </div>
  </div>
</section>
  <div class="h-line"></div>
  <section class="padded section-6">
      <div class="container text-center">
          <h1>A powerful tool for an affordable price</h1>
          <h3>The more you buy the more you save!</h3>
          <div class="col-md-12">
          	<div class="col-md-4 col-md-offset-2">
          		<a data-toggle="modal" href="#pricingmodal" class="btn btn-default btn-block btn-md" style="box-shadow: 0px 1px 1px #5f5f5f;">VIEW PRICING</a>
          	</div>
          	<div class="col-md-4">
          		<a href="#" class="btn btn-info btn-block btn-md" style="box-shadow: 0px 1px 1px #5f5f5f;">CLAIM YOUR FREE POSTING</a>
          	</div>
          </div>
      </div>
  </section>
  <div class="h-line"></div>




  <div class="modal fade" id="pricingmodal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">

         	 <button type="button" class="close" data-dismiss="modal">&times;</button>

          <div class="text-center">

          	<h4><strong>A powerful tool for an affordable price.</strong></h4>
          	<h5>Get your first posting <strong>FREE!</strong></h5>

		<div class="row">
              <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <table class="table pricing-table pricing-label">
                  <thead>
                    <tr>
                      <th class="text-center">
                        <div class="title">
                            <h4><strong>Bundle Details</strong></h4>
                            <div></div>
                            <p style="font-weight: 400">Get Access to ALL features <br/>at all level and SAVE more<br/> when you buy<br/>more!<br/><br/></p>                         
                        </div>
                      </th>
                    </tr>
                  </thead>
                  <tbody class="text-center">
                    <tr>
                      <td><p>Advance Compatibility Tool</p></td>
                    </tr>
                    <tr>
                      <td><p>Real-Time Dashboard</p></td>
                    </tr>
                    <tr>
                      <td><p>Candidate Criterea Filters</p></td>
                    </tr>
                    <tr>
                      <td><p>Unlimited Invites to Apply</p></td>
                    </tr>
                    <tr>
                      <td><p>Messaging Solution</p></td>
                    </tr>
                    <tr>
                      <td><p>1 Click Apply</p></td>
                    </tr>
                    <tr>
                      <td><p>Job Amplication</p></td>
                    </tr>
                    <tr>
                      <td>
                        <strong>Choose Your Plan</strong>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pricing-wrapper">
                     <table class="table table-striped pricing-table pricing-values">
                        <thead style="background-color: #59dcc2; color: #fff;">
                          <tr>
                            <th class="text-center">
                              <div class="title">
                                  <h4><strong>Starter</strong></h4>
                                  <h1 style="color: #fff;">1</h1>
                                  <p style="font-weight: 400">Active Posting<br/><br/></p>

                                </div>
                            </th>
                          </tr>
                        </thead>
                        <tbody class="text-center">
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td>
                                <a href="{{ url('plan-subscription/?plan_name=starter') }}" class="btn btn-success btn-block medium-btn" style="background-color: #59dcc2; border-color: #59dcc2;">$49.99/mo</a>
 
                            </td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pricing-wrapper">
                     <table class="table table-striped pricing-table pricing-values">
                        <thead style="background: #43d6b8; color: #fff;">
                          <tr>
                            <th class="text-center">
                              <div class="title">
                                  <h4><strong>Plus</strong></h4>
                                  <h1 style="color: #fff">3</h1>
                                  <p style="font-weight: 400">Upto 3 Active <br /> Posting at a time</p>   
                                </div>
                            </th>
                          </tr>
                        </thead>
                        <tbody class="text-center">
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td>
                                <a href="{{ url('plan-subscription/?plan_name=plus') }}" class="btn btn-success btn-block medium-btn" style="background-color: #43d6b8; border-color: #43d6b8;">$99.99/mo</a>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pricing-wrapper">
                     <table class="table table-striped pricing-table pricing-values">
                        <thead style="background: #36b39a; color: #fff;">
                          <tr>
                            <th class="text-center">
                              <div class="title">
                                  <h4><strong>Pro</strong></h4>
                                  <h1 style="color: #fff;">5</h1>
                                   <p style="font-weight: 400">Upto 5 Active <br /> Posting at a time</p>   
                                </div>
                            </th>
                          </tr>
                        </thead>
                        <tbody class="text-center">
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td>
                                <a href="{{ url('plan-subscription/?plan_name=pro') }}" class="btn btn-success btn-block medium-btn" style="background: #36b39a; border-color: #36b39a;">$149.99/mo</a>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                </div>
	

          </div> <!-- end of text-center -->
        
        </div> <!-- end of modal-body -->
      </div> <!-- end of modal-content -->
      
    </div>
  </div> <!-- end of modal -->

</div>





</div>














@stop

@section('scripts')

@stop