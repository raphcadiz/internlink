<div class="conversation-header">
    @if(isset($currentThread) && count($currentThread))
        <div class="col-md-4 text-left">
            <h5>{{ $currentThread->participantsString(Auth::id(), ['first_name', 'last_name']) }}</h5>
        </div>
        <div class="col-md-4 text-center">
            <h5>{{ $job->title }} <a href="{{ url("employer/posting-info", $job->id) }}"><i class="fa fa-hand-o-right"></i></a></h5>
        </div>
        <div class="col-md-4 text-right">
            <h5 class="text-success">{{ round($job->getStudentCompatibilityPercentage($applicant)) }}%</h5>
        </div>
    @endif

    @if($type == 'new-message')
        <div class="col-md-4 text-left">
            <h5>{{ $applicant->getFullNameAttribute() }}</h5>
        </div>
        <div class="col-md-4 text-center">
            <h5>{{ $job->title }} <a href="{{ url("employer/posting-info", $job->id) }}"><i class="fa fa-hand-o-right"></i></a></h5>
        </div>
        <div class="col-md-4 text-right">
            <h5 class="text-success">{{ round($job->getStudentCompatibilityPercentage($applicant)) }}%</h5>
        </div>
    @endif
</div>
@if(isset($currentThread) && count($currentThread))
    <div class="col-md-12 conversation-messages" id="conversation_messages">
        <input type="hidden" name="current_thread_messages_count" value="{{ $currentThread->messages()->count() }}">
        <?php $date = '';?>
        @foreach($currentThread->messages as $message)
            <br>
            <div class="row" style="padding: 0 15px;">
                <?php
                $position_class = ($message->user_id != Auth::user()->id) ? 'pull-left' : 'pull-right';
                $message_background_color = ($message->user_id != Auth::user()->id) ? 'recipient-message' : 'user-message';
                $date_position = ($message->user_id != Auth::user()->id) ? 'text-left' : 'text-right';
                ?>
                @if ($date != \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $message->created_at)->format('M. d'))
                    <div class="col-md-12 text-center">
                        {!! \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $message->created_at)->format('M. d')  !!}
                    </div>
                    <?php $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $message->created_at)->format('M. d'); ?>
                @endif

                <div class="col-md-8 {{ $position_class }} {{ $message_background_color }}">
                    {{ $message->body }}
                </div>
                <div class="col-md-12 message-time-wr {{ $date_position }}">
                    {!! \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $message->created_at)->format('h:i a')  !!}
                </div>
            </div>
        @endforeach
    </div>
    <div class="reply-container">
        <form action="{{ url('/messages/update', $currentThread->id) }}" method="POST">
        {{ csrf_field() }}
        {!! Form::hidden('recipients[]', $currentThread->participants()->where('user_id', '!=', Auth::user()->id)->first()->user_id ) !!}
        <div class="col-md-10">
            <textarea name="message" id="" class="form-control" required placeholder="Type a message..."></textarea>
        </div>
        <div class="col-md-2">
            <button class="btn btn-success btn-block disabled" disabled><i class="fa fa-paper-plane"></i></button>
        </div>
        </form>
    </div>
@endif
@if($type == 'new-message')
    <div class="col-md-12 conversation-messages"></div>
    <div class="reply-container">
        <form action="{{ url('/messages/store') }}" method="POST">
        {{ csrf_field() }}
        @if(Auth::user()->role->role == 'Student')
            {!! Form::hidden('recipients[]', $job->user_id ) !!}
        @else
             {!! Form::hidden('recipients[]', $applicant->id ) !!}
        @endif
        {!! Form::hidden('job_id', $job->id ) !!}
        {!! Form::hidden('subject', $job->title ) !!}
        <div class="col-md-10">
            <textarea name="message" id="" class="form-control" required placeholder="Type a message..."></textarea>
        </div>
        <div class="col-md-2">
            <button class="btn btn-success btn-block disabled" disabled><i class="fa fa-paper-plane"></i></button>
        </div>
        </form>
    </div>
@endif
@section('scripts')
@stop