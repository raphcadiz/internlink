@if($type == 'new-message')
<div class="col-md-12 new-conversation-item selected">
    <div>
        <span class="participant-name">New message</span>
    </div>
</div>
@endif

@if (count($threads) > 0)
@foreach($threads as $thread)
<div class="col-md-12 conversation-item @if($type != 'new-message' && (!empty($currentThread) && $currentThread->id == $thread->id)) selected @endif " data-thread-id="{{ $thread->id }}">
    <div>
        <span class="participant-name">{{ $thread->participantsString(Auth::id(), ['first_name', 'last_name']) }}</span>
        <span class="pull-right">
            <?php
            $current_date = \Carbon\Carbon::now();
            $message_date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $thread->messages()->orderBy('id', 'DESC')->first()->created_at);

            $date1 = date_create($current_date->format('Y-m-d'));
            $date2 = date_create($message_date->format('Y-m-d'));
            $diff = date_diff($date1,$date2)->d;
            ?>
            @if($diff == 0)
                {!! \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $thread->messages()->orderBy('id', 'DESC')->first()->created_at)->format('h:i A')  !!}
            @elseif($diff <= 7)
                {!! \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $thread->messages()->orderBy('id', 'DESC')->first()->created_at)->format('D')  !!}
            @else
                {!! \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $thread->messages()->orderBy('id', 'DESC')->first()->created_at)->format('m/d/y')  !!}
            @endif
        </span>
    </div>
    <div class="message-preview">
        <p>@if($thread->isUnRead($currentUser->id)) <i class="fa fa-circle text-success"></i> @endif {{ substr($thread->latestMessage->body, 0, 60) }}</p>
    </div>
</div>
@endforeach
@else
@if($type != 'new-message')
<div class="col-md-12 no-conversation-item">
    <div>
        <span class="participant-name">No conversation started.</span>
    </div>
</div>
@endif
@endif