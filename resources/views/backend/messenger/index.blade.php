@extends('backend.layouts.master')

@section('title')
    InternLogic - Messages
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-xs-12 col-md-offset-2 message-box">
            <div class="row">
                <div class="col-md-4 conversation-list-container">
                    <div class="conversation-header">
                        <div class="col-md-12 search-conversation-list">
                            <form action="{{ url('/messages/search') }}" method="POST">
                            {{ csrf_field() }}
                            <div id="custom-search-input">
                                <div class="input-group col-md-12">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </span>
                                    <input type="text" class="form-control" name="keyword" placeholder="Search" value="{{ isset($keyword) ? $keyword : '' }}" />
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="conversation-list">
                        @include('backend.messenger.partials.conversation-list', compact('threads', 'currentUser', 'currentThread', 'type', 'applicant', 'job'))
                    </div>
                </div>
                <div class="col-md-8 conversation-content">
                    @include('backend.messenger.partials.conversation-content', compact('threads', 'currentUser', 'currentThread', 'type', 'applicant', 'job'))
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            triggerMessagesJs();
        });
    </script>
@stop