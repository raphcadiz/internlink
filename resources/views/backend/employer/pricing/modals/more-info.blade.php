@extends('layouts.modals.base', ['show_cancel' => true, 'show_submit' => false])
@section('title')
    InternLogic - Match Percentage Algorithm
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 text-center">
           <img src="{{ asset('images/more-info.png') }}" style="max-width:200px">
        </div>
    </div>
    <div class="clear"></div><br>
    <div class="row">
        <div class="col-md-12 text-justify">
            <p>
                In order to generate a great match for you several things have to go right.
            </p>
            <p>
                Our proprietary matching algorithm takes into consideration each candidates answers on a scale of 1-100 per questions We collect your candidate selection inputs on a 1-100 scale.
            </p>
            <p>
                So, in a rutshell if you were to try to get a 100% match with someone you would have better odds winning the lottery 5 times in a row. Statistically. you highest matches will be between 60% and 80%.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="form-group">
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
@stop