@extends('backend.layouts.master')
@section('title')
    InternLogic - Applicants
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-filter"></i> Filter</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        {{ Form::open(['action' => 'ApplicantsController@postFilterApplicants', 'id' => 'filter-applicants']) }}
                        <div class="form-group">
                            <label>Postings</label>
                            <select name="job_id" id="" class="form-control">
                                @foreach($jobs as $job)
                                    <option value="{{ $job->id }}">{{ $job->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="border-separator"></div>
                        <div class="form-group">
                            <label for="">Applicant Status</label>
                            <select name="status" id="" class="form-control">
                                <option value="all">All</option>
                                <option value="phone screened">Phone Screened</option>
                                <option value="interviewed">Interviewed</option>
                            </select>
                        </div>
                        <div class="border-separator"></div>
                        <div class="form-group">
                            {{ Form::checkbox('invited', 1, false, array('class' => 'invited', 'autocomplete' => 'off')) }} Invited
                        </div>
                        {{--<div class="form-group">
                            {{ Form::checkbox('applied', 1, false, array('class' => 'applied', 'autocomplete' => 'off')) }} Applied
                        </div>--}}
                        <div class="border-separator"></div>
                        <div class="form-group">
                            <label for="">Compatibility (%)</label>
                        </div>
                        <div class="form-group">
                            {{ Form::text('compatibility', '', [
                                'class' => 'form-control filter-slider',
                                'data-slider-min' => '25',
                                'data-slider-max' => '100',
                                'data-slider-step' => '1',
                                'data-slider-ticks' => '[25,50,75,100]',
                                'data-slider-ticks-labels' => '["25%","50%","75%","100%"]',
                                'data-slider-value' => '25',
                                'style' => 'width: 100%'
                            ]) }}
                        </div>
                        <div class="border-separator"></div>
                        <div class="form-group">
                            <label for="">College</label>
                            <select name="college_id" class="form-control">
                                <option value="0">All</option>
                                @foreach($colleges as $id => $value)
                                    <option value="{{ $id }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="border-separator"></div>
                        <div class="form-group">
                            <label for="">Major</label>
                            <select name="major_id" class="form-control">
                                <option value="0">All</option>
                                @foreach($majors as $id => $value)
                                    <option value="{{ $id }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="border-separator"></div>
                        <div class="form-group">
                            <label for="">GPA (Above)</label>
                        </div>
                        <div class="form-group">
                            {{ Form::text('gpa', '', [
                                'class' => 'form-control filter-slider',
                                'data-slider-min' => '1.0',
                                'data-slider-max' => '4.0',
                                'data-slider-step' => '.1',
                                'data-slider-ticks' => '[1,2,3,4]',
                                'data-slider-ticks-labels' => '["1.0","2.0","3.0","4.0"]',
                                'data-slider-value' => '1',
                                'style' => 'width: 100%'
                            ]) }}
                        </div>
                        <div class="border-separator"></div>
                        <div class="form-group">
                            <label for="">Year</label>
                        </div>
                        <div class="form-group">
                            {{ Form::checkbox('year[]', 'graduated', false, array('class' => 'applied', 'autocomplete' => 'off')) }} Graduated
                        </div>
                        <div class="form-group">
                            {{ Form::checkbox('year[]', 'senior', false, array('class' => 'applied', 'autocomplete' => 'off')) }} Senior
                        </div>
                        <div class="form-group">
                            {{ Form::checkbox('year[]', 'junior', false, array('class' => 'applied', 'autocomplete' => 'off')) }} Junior
                        </div>
                        <div class="form-group">
                            {{ Form::checkbox('year[]', 'sophomore', false, array('class' => 'applied', 'autocomplete' => 'off')) }} Sophomore
                        </div>
                        <div class="form-group">
                            {{ Form::checkbox('year[]', 'freshman',  false, array('class' => 'applied', 'autocomplete' => 'off')) }} Freshman
                        </div>
                        <div class="form-group">
                            {{ Form::checkbox('year[]', 'other', false, array('class' => 'applied', 'autocomplete' => 'off')) }} Other
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Submit', ['class' => 'btn btn-success btn-block medium-btn']) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-xs-12">
                <div class="">
                    <ul class="nav nav-tabs" id="tabs">
                        <li role="presentation" class="active">
                            <a href="#applicants" aria-controls="applicants" role="tab" data-toggle="tab">APPLICANTS</a>
                        </li>
                        <li role="presentation">
                            <a href="#candidates" aria-controls="candidates" role="tab" data-toggle="tab">CANDIDATES</a>
                        </li>
                    </ul>
                </div>
                <div class="x_panel no-padding" style="border-top:0">
                    <div class="x_content no-padding" style="margin:0;">
                        <div class="tab-content trait-selection-tab">
                            <div class="tab-pane active no-padding" role="tabpanel" id="applicants">
                                <div class="row">
                                    <div class="col-md-12 text-center loading-applicants" style="padding:20px">
                                        <i class="fa fa-spinner fa-spin fa-1x"></i> Loading results.
                                    </div>
                                    <div class="col-md-12 applicants-result-table"></div>
                                </div>
                            </div>
                            <div class="tab-pane no-padding" role="tabpanel" id="candidates">
                                <div class="row">
                                    <div class="col-md-12 text-center loading-candidates" style="padding:20px">
                                        <i class="fa fa-spinner fa-spin fa-1x"></i> Loading results.
                                    </div>
                                    <div class="col-md-12 candidates-result-table"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script src="{{ asset('js/applicants.js') }}"></script>
@stop