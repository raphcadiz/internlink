<table id="job-matches" class="table no-border">
    @if(empty($candidates))
        <tr>
            <td class="text-center" colspan="4">No Candidates.</td>
        </tr>
    @else
        @foreach($candidates as $candidate)
            <tr>
                <td valign="center" align="center" data-job-id="{{ $candidate['job_id'] }}" data-user-id="{{ $candidate['user_id'] }}" class="go-to-candidate" style="cursor:pointer">
                    <?php
                        $user = App\Models\User::find($candidate['user_id']);
                    ?>
                    @if( $user->basic_information->prof_pic != '' )
                        <img class="img-responsive avatar-view img-round" src="{{ asset('uploads/profile-pic/'.$user->basic_information->prof_pic) }}" width="50" height="50">
                    @else
                        <img class="img-responsive avatar-view img-round" src="http://coloredfaces.com/img/avatars/rJM6HQsLT6bGC8i.png" width="50">
                    @endif
                </td>
                <td data-job-id="{{ $candidate['job_id'] }}" data-user-id="{{ $candidate['user_id'] }}" class="go-to-candidate" style="cursor:pointer">
                    <span style="font-weight: bold;">{{ $candidate['name'] }}</span><br />
                    <span>{{ mb_strimwidth($candidate['college'], 0, 20, "...") }} | {{ $candidate['gpa'] }}</span>
                </td>
                <td data-job-id="{{ $candidate['job_id'] }}" data-user-id="{{ $candidate['user_id'] }}" class="go-to-candidate" style="cursor:pointer">
                    <span>{{ $candidate['job_title'] }}</span><br>
                    <span>{{ ucwords($candidate['status']) }}</span>
                </td>
                <td valign="center">
                    @if($candidate['invited'])
                        <i class="fa fa-paper-plane text-success"></i>
                    @else
                        <a href="/employer/invite-candidate/{{ $candidate['job_id'] }}/{{ $candidate['user_id'] }}" data-toggle="modal" data-target="#bs-modal-medium"><i class="fa fa-paper-plane"></i></a>
                    @endif
                </td>
                <td valign="center">
                    <strong style="font-size:18px" class="text-success">{{ round($candidate['compatibility']).'%' }}</strong>
                </td>
            </tr>
        @endforeach
    @endif
</table>
