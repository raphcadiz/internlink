<table id="job-matches" class="table no-border">
    @if(empty($applicants))
        <tr>
            <td class="text-center" colspan="4">No applicants.</td>
        </tr>
    @else
        @foreach($applicants as $applicant)
            <tr data-job-id="{{ $applicant['job_id'] }}" data-user-id="{{ $applicant['user_id'] }}" class="go-to-applicant" style="cursor:pointer">
                <td valign="center" align="center">
                    <?php
                        $user = App\Models\User::find($applicant['user_id']);
                    ?>
                    @if( $user->basic_information->prof_pic != '' )
                        <img class="img-responsive avatar-view img-round" src="{{ asset('uploads/profile-pic/'.$user->basic_information->prof_pic) }}" width="50" height="50">
                    @else
                        <img class="img-responsive avatar-view img-round" src="http://coloredfaces.com/img/avatars/rJM6HQsLT6bGC8i.png" width="50">
                    @endif
                </td>
                <td>
                    <span style="font-weight: bold;">{{ $applicant['name'] }}</span><br />
                    <span>{{ mb_strimwidth($applicant['college'], 0, 20, "...") }} | {{ $applicant['gpa'] }}</span><br>
                    <span>{{ $applicant['major'] }}</span>
                </td>
                <td>
                    <span>{{ ucwords($applicant['status']) }}</span>
                </td>
                <td valign="center">
                    @if($applicant['invited'])
                        <i class="fa fa-paper-plane text-success"></i>
                    @endif
                </td>
                <td valign="center">
                    <strong style="font-size:18px" class="text-success">{{ $applicant['compatibility'].'%' }}</strong>
                </td>
            </tr>
        @endforeach
    @endif
</table>