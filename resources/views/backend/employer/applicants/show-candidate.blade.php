@extends('backend.layouts.master')
@section('title')
    InternLogic - Candidate Information
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-xs-12 col-md-offset-1">
            <div class="x_panel">
                <div class="x_content">
                    <div class="x_title">
                        <div class="col-md-4">
                            <h2>{{ $job->title }} @if($user->isInvitedJob($job->id)) <i class="fa fa-paper-plane text-success"></i> @endif</h2>
                        </div>
                        <div class="col-md-4">        
                        </div>
                        <div class="col-md-4 text-right">
                            <h2 class="pull-right">
                                <label class="text-success">
                                    {{ round($job->getStudentCompatibilityPercentage($user)) }}%
                                </label>
                            </h2>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="profile_img text-center">
                                    <div class="crop-avatar">
                                        @if( $user->basic_information->prof_pic != '' )
                                            <img class="img-responsive avatar-view center-round" src="{{ asset('uploads/profile-pic/'.$user->basic_information->prof_pic) }}" width="150" height="150">
                                        @else
                                            <img class="img-responsive avatar-view center-round" src="http://coloredfaces.com/img/avatars/rJM6HQsLT6bGC8i.png" width="150">
                                        @endif
                                        <h2>{{ $user->getFullNameAttribute() }}</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <table class="table table-borderless table-striped">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <strong>Personality Type:</strong>
                                                <label class="text-success">{{ $user->personalityType()->type }}</label>
                                            </td>
                                            <td>
                                                <strong>Major:</strong>
                                                {{ $user->basic_information->major->major }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Email:</strong>
                                                {{ $user->email }}
                                            </td>
                                            <td>
                                                <strong>College:</strong>
                                                {{ mb_strimwidth($user->basic_information->college->college, 0, 20, "...") }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Phone:</strong>
                                                {{ $user->basic_information->phone_number }}
                                            </td>
                                            <td>
                                                <strong>GPA:</strong>
                                                {{ $user->basic_information->gpa }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Location:</strong>
                                                {{ $user->basic_information->city }}, {{ $user->basic_information->state }}
                                            </td>
                                            <td>
                                                <strong>Year:</strong>
                                                {{ $user->basic_information->year }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="x_title"></div>
                    <div class="row">
                        <div class="col-l-12 col-xs-12 col-sm-12 col-lg-12 text-center">
                            <a href="javascript:;" id="{{ !Auth::user()->isPremiumEmployer() ? 'view-compatibility-free-user' : 'view-compatibility' }}" style="color: #ffaa0e;font-size: 18px;" data-content="{{ $job->id }}">View Advanced Compatibility <span class="fa fa-chevron-down"></span></a>
                            @if( Auth::user()->isPremiumEmployer() )
                                <div id="compatibility-data-container" class="text-left center-block" style="display:none; margin: 20px 0">
                                    <div class="text-center label-traits">
                                        <span><i class="fa fa-fw fa-square" style="color: #1ebc9e;"></i> = Applicant Traits</span>
                                        <span><i class="fa fa-fw fa-square" style="color: #ffaa0e;"></i> = Posting Traits</span>
                                    </div>
                                    @foreach( $job->getStudentCompatibility($user) as $job_compatibility )
                                        <?php
                                            $assessment = App\Models\Assessment::find($job_compatibility->assessment_id);
                                        ?>
                                        @if( $assessment )
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <input class="form-control assessment-sliders" name="assessment[{{ $assessment->id }}]" data-trait-id="{{ $assessment->id }}" style="width: 100%;" id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="[{{ round($job_compatibility->sss) }},{{ $job_compatibility->ess }}]"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-xs-6 text-left"><strong>{{ $assessment->attribute_l }}</strong></div>
                                            <div class="col-md-6 col-xs-6 text-right"><strong>{{ $assessment->attribute_r }}</strong></div>
                                        </div>
                                        <br />
                                        @endif
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="x_title"></div>
                    <div class="row text-justify">
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <h4>About</h4>
                            <p>{{ $user->basic_information->about }}</p>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <h4>Experience</h4>
                            <p>{{ $user->basic_information->experience }}</p>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <h4>Skills</h4>
                            <?php $skills = ( !empty($user->basic_information->skills) ) ? explode(',', $user->basic_information->skills ) : []; ?>
                            @if(!empty($skills))
                                @foreach($skills as $skill)
                                <span class="btn btn-default"> {{ $skill }}</span>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <h4>Resume</h4>
                            <a href="{{ asset('uploads/docs/'.$user->basic_information->resume) }}" target="_blank"><i class="fa fa-fw fa-file-text fa-2"></i>Resume</a>
                        </div>
                    </div>
                    <div class="x_title"></div>
                    <div class="row">
                        <br />
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <a href='{{ url("messages/new/$job->id/$user->id") }}'' class="btn btn-success medium-btn">
                                <i class="fa fa-fw fa-comments"></i> Messages
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('scripts')
<script type="text/javascript" src="{{ asset('js/applicants-tab.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.assessment-sliders').slider();
        $('.assessment-sliders').slider( 'disable');
        $('select').select2();
    });
</script>
@stop