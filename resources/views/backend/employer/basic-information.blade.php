@extends('backend.layouts.master')

@section('title')
    InternLogic - Basic Information
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-xs-12 col-md-offset-3">
            <h3 class="">Basic Information</h3>
            @include('backend.layouts.flash-message')
            <div class="x_panel">
                <div class="x_title">
                    <h2>Basic Information</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form method="POST" role="form" action="{{ url('/employer/edit-basic-information', $employer_information->user->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Company Name <span class="text-danger">*</span></label>
                                    <input type="text" name="company_name" class="form-control" required value="{{ $employer_information->company_name }}" placeholder="Company Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Industry Type <span class="text-danger">*</span></label>
                                    <select name="industry_id" class="form-control" required>
                                        @foreach($industries as $industry)
                                            <option value="{{ $industry->id }}" {{ $employer_information->industry_id == $industry->id ? 'selected' : ''}}>{{ $industry->industry }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Business Size <span class="text-danger">*</span></label>
                                    <select name="business_size_id" class="form-control" required>
                                        @foreach($businessSizes as $businessSize)
                                            <option value="{{ $businessSize->id }}" {{ $employer_information->business_size_id == $businessSize->id ? 'selected' : ''}}>{{ $businessSize->size_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Employer Type <span class="text-danger">*</span></label>
                                    <select name="employer_type_id" class="form-control" required>
                                        @foreach($employerTypes as $employerType)
                                            <option value="{{ $employerType->id }}" {{ $employer_information->employer_type_id == $employerType->id ? 'selected' : ''}}>{{ $employerType->type_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Phone Number <span class="text-danger">*</span></label>
                                    <input type="text" name="phone_number" class="form-control" required value="{{ $employer_information->phone_number }}" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address <span class="text-danger">*</span></label>
                                    <input type="text" name="address" class="form-control" required value="{{ $employer_information->address }}" placeholder="Address">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>City <span class="text-danger">*</span></label>
                                    <input type="text" name="city" class="form-control" required value="{{ $employer_information->city }}" placeholder="City">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>State <span class="text-danger">*</span></label>
                                    {{ Form::select('states', $states, ['class' => 'form-control', 'autocomplete' => 'off', 'required' => 'required']) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Zip Code <span class="text-danger">*</span></label>
                                    <input type="text" name="zip_code" class="form-control" required value="{{ $employer_information->zip_code }}" placeholder="Zip Code">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Company Logo</label>
                                    <input type="file" name="company_logo">
                                </div>
                                <div class="col-md-6">
                                    <a href="#">some-file-here.jpg</a>
                                </div>
                            </div> 
                        </div>
                        <input type="submit" class="btn btn-primary medium-btn pull-right" name="submit" value="Update" style="margin:0">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('select').select2();
            $('select[name=states]').select2({
                width: '100%'
            });
        });
    </script>
@stop