@extends('backend.layouts.master')

@section('title')
    InternLogic - My Postings
@stop
@section('styles')
    {{ HTML::style('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}
@stop
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 page-title">
                <div class="title_left">
                    <h3 class="">My Postings</h3>
                </div>
                <div class="title_right"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap no-footer">
                            <div class="col-sm-12">
                                <a class="changePostingStatus hide" data-toggle="modal" data-target="#bs-modal-medium" href="{{ url('/employer/posting/change-status') }}"></a>
                                <table id="datatable" class="table table-bordered dataTable no-footer table-hover" role="grid" aria-describedby="datatable_info">
                                    <thead>
                                        <tr role="row">
                                            <th>
                                                Name
                                            </th>
                                            <th>
                                                Total Applicants
                                            </th>
                                            <th>
                                                Views
                                            </th>
                                            <th>
                                                Status
                                            </th>
                                            <th>
                                                Created
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach( $user->jobs as $job )
                                        <tr role="role">
                                            <td class="sorting_1">
                                                <a href="{{ url('/employer/posting-info', $job->id) }}">{{ $job->title }}</a>
                                            </td>
                                            <td>
                                                {{ $job->applicants()->count() }}
                                            </td>
                                            <td>
                                                {{ $job->studentViews()->count() }}
                                            </td>
                                            <td>
                                                <select name="status" class="status-select job-{{ $job->id }}" autocomplete="off" data-job_id="{{ $job->id }}">
                                                    <option value="1" <?= ($job->status == 1) ? 'selected' : '' ?> >Published</option>
                                                    <option value="0" <?= ($job->status == 0) ? 'selected' : '' ?> >Draft</option>
                                                </select>
                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($job->created_at)->format('m/d/y') }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    @parent
    {{ HTML::script('plugins/switch/switch.jquery.js') }}
    {{ HTML::script('plugins/filterable-form/sort-manager.js') }}
    {{ HTML::script('vendors/datatables.net/js/jquery.dataTables.min.js') }}
    {{ HTML::script('vendors/datatables.net-bs/js/jquery.dataTables.min.js') }}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable();
            $('select[name=datatable_length]').select2({
                containerCssClass: 'select2-form-control'
            });
            $('div#datatable_filter').find('input[type=search]').addClass('form-control');
        });
    </script>
@stop