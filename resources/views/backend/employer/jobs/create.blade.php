@extends('backend.layouts.master')

@section('title')
    InternLogic - Add Job
@stop

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-xs-12 col-md-offset-2">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Add New Job</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        <form method="POST" class="form-horizontal form-label-left" role="form" action="{{ url('/employer/postings/add-new') }}">
                            {{ csrf_field() }}
                            @include('backend.layouts.flash-message')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Job Title <span class="text-danger">*</span></label>
                                        <input type="text" name="title" class="form-control" value="" placeholder="Job Title" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Career Type <span class="text-danger" required>*</span></label>
                                        <select name="career_id"class="form-control">
                                            @foreach($careers as $career)
                                                @if(!empty($career->title))
                                                    <option value="{{ $career->id }}">{{ $career->title }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Company Description <span class="text-danger">*</span></label>
                                        <textarea name="company_description" class="form-control" rows="4" placeholder="Description" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Responsibilities <span class="text-danger">*</span></label>
                                        <textarea name="responsibilities" class="form-control" rows="4" placeholder="Responsibilities" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Requirements <span class="text-danger">*</span></label>
                                        <textarea name="requirements" class="form-control" rows="4" required placeholder="Requirements"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">GPA <span class="text-danger">*</span> (Or above)</label>
                                        <select name="gpa" class="form-control" required>
                                            <option value="4">4.0</option>
                                            <option value="3.5">3.5</option>
                                            <option value="3">3.0</option>
                                            <option value="2.5">2.5</option>
                                            <option value="2">2.0</option>
                                            <option value="1.5">1.5</option>
                                            <option value="1">1.0</option>
                                            <option value="0.5">0.5</option>
                                            <option value="0">No Requirement</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Year <span class="text-danger">*</span> (Or above)</label>
                                        <select name="year" id="" class="form-control" required>
                                            <option value="freshman">Freshman</option>
                                            <option value="sophomore">Sophomore</option>
                                            <option value="junior">Junior</option>
                                            <option value="senior">Senior</option>
                                            <option value="graduated">Graduated</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Skills</label>
                                        <input id="skills" name="skills" type="text" class="tags form-control" value="" />
                                        <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Employment Type <span class="text-danger">*</span></label>
                                        <select name="employment_type" id="" class="form-control">
                                            <option value="Full-Time">Full-Time</option>
                                            <option value="Part-Time">Part-Time</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Start - End</label>
                                        <div class="input-group date">
                                            <input type="text" id="job_dates" name="job_dates" class="form-control" value="" placeholder="">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Internship Or Job <span class="text-danger">*</span></label>
                                        <select name="job_type" id="" class="form-control">
                                            <option value="Internship">Internship</option>
                                            <option value="Job">Job</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Paid or Unpaid(For Internships)</label>
                                        <select name="paid" id="" class="form-control">
                                            <option value="Paid">Paid</option>
                                            <option value="Unpaid">Unpaid</option>
                                            <option value="College Credit">College Credit</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Display Salary</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="show_salary" id="" value="1" checked="checked">Yes
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="show_salary" id="" value="0">No
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Is this a virtual internship? (Remote)</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="virtual_internship" id="" value="1" checked="checked">Yes
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="virtual_internship" id="" value="0">No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="salary-group">
                                   <div class="col-md-6">
                                       <div class="form-group">
                                           <label>Salary</label>
                                           <input type="text" name="salary" class="form-control" value="" placeholder="Salary" autocomplete="off">
                                       </div>
                                   </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Rate</label>
                                            <select name="rate" id="" class="form-control">
                                                <option value="Per Hour">Per Hour</option>
                                                <option value="Per Month">Per Month</option>
                                                <option value="Per Year">Per Year</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <input type="submit" class="btn btn-primary medium-btn" name="submit" value="Continue">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
<!-- /.container-fluid -->
@stop

@section('scripts')

<!-- jQuery Tags Input -->
<script src="{{ asset('vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('input[name=show_salary]').on('click', function(e){
            $('#salary-group').toggle();
        });

        $('input[name=internlogic_link]').on('click', function(e){
            $('#job_url').toggle();
        });

        $('select').select2();

        $('#skills').tagsInput({
            width: 'auto'
        });

        $('#job_dates').daterangepicker();
        
    });
</script>
@stop