@extends('backend.layouts.master')
@section('title')
    InternLogic - Candidate Trait Selection
@stop
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-xs-12 col-md-offset-2">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Update Candidate Trait Selection</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        <form class="form-horizontal form-label-left" role="form" method="POST" action="{{ url('/employer/posting/update-trait-selection') }}">
                            {{ csrf_field() }}
                            {{ Form::hidden('job_id', $job->id) }}
                            @include('backend.layouts.flash-message')
                            <div class="row content">
                                <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                    <table class="table table-borderless trait-selection">
                                        <thead>
                                        <tr>
                                            <th>Strongly Desire</th>
                                            <th class="text-center">Neutral</th>
                                            <th class="text-right">Strongly Desire</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($job->traitsSelection as $index => $selection)
                                            <tr id="{{ $selection->trait_id }}">
                                                <td colspan="3">
                                                    <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                                                        <input class="form-control trait-sliders" name="traits[{{ $selection->trait_id }}]" data-trait-id="{{ $selection->trait_id }}" style="width: 100%;" id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="20" data-slider-max="80" data-slider-step="1" data-slider-value="{{ $selection->employer_standard_score }}"/>
                                                    </div>
                                                    <a href="#{{ str_replace(' ', '-', $selection->candidateTrait->trait_1) }}" class="attrib-toggle">
                                                        <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                                            <span class="pull-left"><strong>{{ $selection->candidateTrait->trait_1 }}</strong></span>
                                                        </div>
                                                        <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
                                                            <span class="pull-right"><strong>{{ $selection->candidateTrait->trait_2 }}</strong></span>
                                                        </div>
                                                    </a>    
                                                    <br><br>
                                                    <div class="row attrib-toggle-data" id="{{ str_replace(' ', '-', $selection->candidateTrait->trait_1) }}" style="{{ $index == 0 ? '' : 'display:none'}}">
                                                        <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6 text-center"><br>
                                                            <img src="{{ asset('images/traits/'.$selection->candidateTrait->assessment->attribute_l_icon)}}" class="attribute-icon" />
                                                            <p class="text-center">{{ $selection->candidateTrait->assessment->statement_1 }}</p>
                                                        </div>
                                                        <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6 text-center"><br>
                                                            <img src="{{ asset('images/traits/'.$selection->candidateTrait->assessment->attribute_r_icon)}}" class="attribute-icon" />
                                                            <p class="text-center">{{ $selection->candidateTrait->assessment->statement_2 }}</p>
                                                        </div>
                                                    </div>
                                                    <br />
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12" style="margin-top: 20px;">
                                <div class="form-group text-center">
                                    <input type="submit" class="btn btn-primary medium-btn" value="Save">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#tabs a').on('click', function(e){
                e.preventDefault();
                $(this).tab('show');
            });

            $('select').select2();

            $('.trait-sliders').slider();

            // $('table.trait-selection tbody > tr').on('mouseover', function () {
            //     var $row = $(this),
            //             $trait_details = $row.find('.trait-details');

            //     $row.closest('tbody').find('tr').removeClass('tr-selected');
            //     $row.closest('tbody').find('.trait-details').addClass('hide');
            //     $trait_details.removeClass('hide');
            //     $row.addClass('tr-selected');
            // });
            $('.attrib-toggle').on('click', function(e){
                var self = $(e.currentTarget);
                e.preventDefault();
                var target_id = self.attr('href');

                $(target_id).slideToggle('slow');

            });
        });
    </script>
@stop
@section('styles')
    <style type="text/css">
        tr.tr-selected {
            background: #F4F0CB;
        }
    </style>
@stop