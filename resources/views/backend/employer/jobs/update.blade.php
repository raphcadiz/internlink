@extends('backend.layouts.master')

@section('title')
    InternLogic - Update Job
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-xs-12 col-md-offset-2">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Update Job</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        <form method="POST" class="form-horizontal form-label-left" role="form" action="{{ url('/employer/posting/update') }}">
                            {{ csrf_field() }}
                            {{ Form::hidden('job_id', $job->id) }}
                            @include('backend.layouts.flash-message')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Job Title <span class="text-danger">*</span></label>
                                        <input type="text" name="title" class="form-control" placeholder="Job Title" autocomplete="off" value="{{ $job->title }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Career Type <span class="text-danger" required>*</span></label>
                                        <select name="career_id"class="form-control">
                                            @foreach($careers as $career)
                                                @if(!empty($career->title))
                                                    <option value="{{ $career->id }}" {{ ($job->career_id == $career->id) ? 'selected' : '' }}>{{ $career->title }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Company Description <span class="text-danger">*</span></label>
                                        <textarea name="company_description" class="form-control" rows="4" placeholder="Description" required>{!! $job->company_description !!}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Responsibilities <span class="text-danger">*</span></label>
                                        <textarea name="responsibilities" class="form-control" rows="4" placeholder="Responsibilities" required>{!! $job->responsibilities !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Requirements <span class="text-danger">*</span></label>
                                        <textarea name="requirements" class="form-control" rows="4" required placeholder="Requirements">{!! $job->requirements !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">GPA <span class="text-danger">*</span> (Or above)</label>
                                        <select name="gpa" class="form-control" required>
                                            <option {{ ($job->gpa == 4) ? 'selected' : '' }} value="4">4.0</option>
                                            <option {{ ($job->gpa == 3.5) ? 'selected' : '' }} value="3.5">3.5</option>
                                            <option {{ ($job->gpa == 3) ? 'selected' : '' }} value="3">3.0</option>
                                            <option {{ ($job->gpa == 2.5) ? 'selected' : '' }} value="2.5">2.5</option>
                                            <option {{ ($job->gpa == 2) ? 'selected' : '' }} value="2">2.0</option>
                                            <option {{ ($job->gpa == 1.5) ? 'selected' : '' }} value="1.5">1.5</option>
                                            <option {{ ($job->gpa == 1) ? 'selected' : '' }} value="1">1.0</option>
                                            <option {{ ($job->gpa == 0.5) ? 'selected' : '' }} value="0.5">0.5</option>
                                            <option {{ ($job->gpa == 0) ? 'selected' : '' }} value="0">No Requirement</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Year <span class="text-danger">*</span> (Or above)</label>
                                        <select name="year" id="" class="form-control" required>
                                            <option {{ ($job->year == 'freshman') ? 'selected' : '' }} value="freshman">Freshman</option>
                                            <option {{ ($job->year == 'sophomore') ? 'selected' : '' }} value="sophomore">Sophomore</option>
                                            <option {{ ($job->year == 'junior') ? 'selected' : '' }} value="junior">Junior</option>
                                            <option {{ ($job->year == 'senior') ? 'selected' : '' }} value="senior">Senior</option>
                                            <option {{ ($job->year == 'graduated') ? 'selected' : '' }} value="graduated">Graduated</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Skills</label>
                                        <input id="skills" name="skills" type="text" class="tags form-control" value="{{ $job->skills }}" />
                                        <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Employment Type <span class="text-danger">*</span></label>
                                        <select name="employment_type" id="" class="form-control">
                                            <option {{ ($job->employment_type == 'Full-Time') ? 'selected' : '' }} value="Full-Time">Full-Time</option>
                                            <option {{ ($job->employment_type == 'Part-Time') ? 'selected' : '' }} value="Part-Time">Part-Time</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Start - End</label>
                                        <div class="input-group date">
                                            <input type="text" id="job_dates" name="job_dates" class="form-control" value="{!! \Carbon\Carbon::createFromFormat('Y-m-d', $job->job_start)->format('m/d/Y').' - '.\Carbon\Carbon::createFromFormat('Y-m-d', $job->job_end)->format('m/d/Y') !!}" placeholder="">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Internship Or Job <span class="text-danger">*</span></label>
                                        <select name="job_type" id="" class="form-control">
                                            <option {{ ($job->job_type == 'Internship') ? 'selected' : '' }} value="Internship">Internship</option>
                                            <option {{ ($job->job_type == 'Job') ? 'selected' : '' }} value="Job">Job</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Paid or Unpaid(For Internships)</label>
                                        <select name="paid" id="" class="form-control">
                                            <option {{ ($job->paid == 'Paid') ? 'selected' : '' }} value="Paid">Paid</option>
                                            <option {{ ($job->paid == 'Unpaid') ? 'selected' : '' }} value="Unpaid">Unpaid</option>
                                            <option {{ ($job->paid == 'College Credit') ?'selected' : '' }} value="College Credit">College Credit</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Display Salary</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="show_salary" id="" value="1" checked="checked">Yes
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="show_salary" id="" value="0">No
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Is this a virtual internship? (Remote)</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="virtual_internship" id="" value="1" checked="checked">Yes
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="virtual_internship" id="" value="0">No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="salary-group">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Salary</label>
                                            <input type="text" name="salary" class="form-control" value="{{ $job->salary }}" placeholder="Salary" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Rate</label>
                                            <select name="rate" id="" class="form-control">
                                                <option {{ ($job->rate == 'Per Hour') ? 'selected' : '' }} value="Per Hour">Per Hour</option>
                                                <option {{ ($job->rate == 'Per Month') ? 'selected' : '' }} value="Per Month">Per Month</option>
                                                <option {{ ($job->rate == 'Per Year') ? 'selected' : '' }} value="Per Year">Per Year</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <input type="submit" class="btn btn-primary medium-btn" value="Save">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
    @stop

    @section('scripts')

            <!-- jQuery Tags Input -->
    <script src="{{ asset('vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('input[name=show_salary]').on('click', function(e){
                $('#salary-group').toggle();
            });

            $('input[name=internlogic_link]').on('click', function(e){
                $('#job_url').toggle();
            });

            $('select').select2();

            $('#skills').tagsInput({
                width: 'auto'
            });

            $('#job_dates').daterangepicker();

        });
    </script>
@stop