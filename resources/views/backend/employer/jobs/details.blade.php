@extends('backend.layouts.master')

@section('title')
    InternLogic - Posting Information
@stop
@section('styles')
    {{ HTML::style('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}
@stop
@section('content')
    <div class="container-fluid">
        <input type="hidden" name="job_id" value="{{ $job->id }}">
        <input type="hidden" name="current_status" value="{{ $job->status }}">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="pull-left">
                            <div class="row">
                                <div class="pull-left">
                                    <h2>{{ $job->title }}&nbsp;&nbsp;</h2>
                                </div>
                            </div>
                            <div class="row">
                                <span style="display: inline-block;">
                                    {{ Auth::user()->basic_information->company_name }} - {{ Auth::user()->basic_information->city.', '.Auth::user()->basic_information->state }}
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                        </div>
                        <div class="pull-right">
                            <select name="status" class="status-select" autocomplete="off" data-job_id="{{ $job->id }}">
                                <option value="1" <?= ($job->status == 1) ? 'selected' : '' ?> >Published</option>
                                <option value="0" <?= ($job->status == 0) ? 'selected' : '' ?> >Draft</option>
                            </select>
                            <?php $status_id = ($job->status == 1) ? 0 : 1;?>
                            <a class="changePostingStatus hide" data-toggle="modal" data-target="#bs-modal-medium" href=""></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Job Details</h4>
                                <table class="table table-borderless table-striped">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <strong>Career Type:</strong> {{ $job->career->title }}
                                        </td>
                                        <td>
                                            <strong>Location Type:</strong>  {{ ($job->virtual == 1) ? 'Virtual' : '--' }}
                                        </td>
                                        <td>
                                            <strong>Applicants:</strong>  {{ $job->applicants->count() }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Position:</strong> {{ $job->job_type }} / {{ $job->paid }}
                                        </td>
                                        <td>
                                            <strong>Employment Type:</strong> {{ $job->employment_type }}
                                        </td>
                                        <td>
                                            <strong>Views:</strong> {{ $job->studentViews()->count() }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Duration:</strong> {{ \Carbon\Carbon::parse($job->job_start)->format('m/d/y') }} - {{ \Carbon\Carbon::parse($job->job_end)->format('m/d/y') }}
                                        </td>
                                        <td>
                                            <strong>Rate:</strong> ${{ $job->salary }}
                                            @if($job->rate == 'Per Hour')
                                                / hour
                                            @endif
                                            @if($job->rate == 'Per Month')
                                                / month
                                            @endif
                                            @if($job->rate == 'Per Year')
                                                / year
                                            @endif

                                        </td>
                                        <td>
                                            <strong>Created:</strong> {{ \Carbon\Carbon::parse($job->created_at)->format('m/d/y') }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br>
                        <div class="x_title"></div>
                        <div class="row text-justify">
                            <div class="col-md-12">
                                <h4>Description</h4>
                                <p>{{ $job->company_description }}</p>
                            </div>
                            <div class="col-md-12">
                                <h4>Responsibilities</h4>
                                <p>{{ $job->responsibilities }}</p>
                            </div>
                            <div class="col-md-12">
                                <h4>Requirements</h4>
                                <p>{{ $job->requirements }}</p>
                            </div>
                            <div class="col-md-12">
                                <h4>Skills</h4>
                                <p>
                                    @foreach ($skills as $skill)
                                        @if (!empty($skill))
                                            <span class="btn btn-default btn-xs">{{ $skill }}</span>
                                        @endif
                                    @endforeach

                                    @if (empty($skills))
                                        No skills added.
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="x_title"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <a class="btn btn-block medium-btn btn-success" href="{{ url('/employer/posting/update', $job->id) }}" data-toggle="tooltip" data-original-title="Update Job Details">Update Details</a>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ url('/employer/posting/update-trait-selection', $job->id) }}" class="btn btn-block medium-btn btn-primary" data-toggle="tooltip" data-original-title="Update Trait Selection">Update Trait Selection</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop