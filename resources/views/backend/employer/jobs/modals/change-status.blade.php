@extends('layouts.modals.base')
@section('title')
    Change Job Status
@stop
@section('content')
    <div class="form-content">
        {{ Form::open(['action' => 'JobController@postChangeStatus', 'id' => 'changeJobStatusForm']) }}
        {{ csrf_field() }}
        {{ Form::hidden('job_id', $job->id) }}
        {{ Form::hidden('status', $status_id) }}
        <div class="row">
            <div class="col-md-12 text-center">
                Are you sure you want to change the status of this job to <strong>{{ ($status_id == 1) ? 'PUBLISHED' : 'DRAFT' }}</strong>?<br>
                <strong>{{ strtoupper($job->title) }}</strong>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <button class="btn btn-success btn-block medium-btn" type="submit">Yes</button>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <button class="btn btn-danger btn-block medium-btn" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        {{ Form::close() }}
    </div>
    <div class="form-success hide">
        <div class="row">
            <div class="col-md-12 text-center">
                <strong class="message"></strong><br>
                <i class="fa fa-check-circle fa-4 text-success"></i><br>
            </div>
        </div>
    </div>
@stop