@extends('backend.layouts.master')

@section('title')
    InternLogic - My Postings
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 page-title">
            <div class="title_left">
                <h3 class="">Job Postings</h3>
            </div>
            <div class="title_right"></div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <a href="{{ url('employer/postings/add-new') }}" class="btn btn-info pull-right">Add New</a>
        </div>
    </div>

    <br />

    <div class="row">
        <div class="col-lg-12">
        @include('backend.layouts.flash-message')
        @if(count($user->jobs) >= 1 )
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Career</th>
                        <th>Employment Type</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Job Type</th>
                        <th>Paid</th>
                        <th>Salary</th>
                        <th>Rate</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $user->jobs as $job )
                        <tr>
                            <td>{{ $job->title }}</td>
                            <td>{{ $job->career->title }}</td>
                            <td>{{ $job->employment_type }}</td>
                            <td>{{ $job->job_start }}</td>
                            <td>{{ $job->job_end }}</td>
                            <td>{{ $job->job_type }}</td>
                            <td>{{ $job->paid }}</td>
                            <td>{{ $job->salary }}</td>
                            <td>{{ $job->rate }}</td>
                            <td>{{ $job->status == 1 ? 'Active' : 'Inactive' }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-info fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        No Jobs Posted Yet
            </div>
        @endif
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@stop

@section('scripts')
@stop