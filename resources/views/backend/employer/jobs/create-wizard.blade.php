@extends('backend.layouts.master')

@section('title')
    InternLogic - Add Job
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9 col-xs-12 col-md-offset-1">
            @if(Session::has('message'))
                <div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add New Job</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <div class="form_wizard wizard_horizontal" id="create-job-wizard">
                        <ul class="wizard_steps anchor">
                            <li>
                                <a href="#step-1" class="selected" isdone="1" rel="1">
                                    <span class="step_no">1</span>
                                    <span class="step_descr">
                                        Step 1 <br> <small>Post Description</small>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-2" class="disabled" isdone="0" rel="2">
                                    <span class="step_no">2</span>
                                    <span class="step_descr">
                                        Step 2 <br> <small>Candidate Selection</small>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-3" class="disabled" isdone="0" rel="3">
                                    <span class="step_no">3</span>
                                    <span class="step_descr">
                                        Step 3 <br> <small>Pricing</small>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-4" class="disabled" isdone="0" rel="4">
                                    <span class="step_no">4</span>
                                    <span class="step_descr">
                                        Step 4 <br> <small>Payment</small>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-5" class="disabled" isdone="0" rel="5">
                                    <span class="step_no">5</span>
                                    <span class="step_descr">
                                        Step 5 <br> <small>Payment Confirmation</small>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-6" class="disabled" isdone="0" rel="6">
                                    <span class="step_no">6</span>
                                    <span class="step_descr">
                                        Step 6 <br> <small>Completed</small>
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <div class="stepContainer">
                            <div class="content" id="step-1" style="display: block;">

                            </div>
                            <div class="content" id="step-2" style="display: none;"></div>
                            <div class="content" id="step-3" style="display: none;"></div>
                            <div class="content" id="step-4" style="display: none;"></div>
                            <div class="content" id="step-5" style="display: none;"></div>
                            <div class="content" id="step-6" style="display: none;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
    <script src="{{ asset('vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#create-job-wizard').smartWizard({
                onFinish: onFinishCallback
            });

            $('.wizard_steps').show();

            function onFinishCallback() {

            }
        });
    </script>
@stop
@section('styles')
    <link rel="stylesheet" href="{{ asset('vendors/jQuery-Smart-Wizard/css/smart_wizard.css') }}">
@stop