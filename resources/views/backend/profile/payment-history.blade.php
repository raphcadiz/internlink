@extends('backend.layouts.master')

@section('title')
    InternLogic - Payment History
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-xs-12 col-md-offset-2">
            <h3 class="">Payment History</h3>
            <div class="x_panel">
                <div class="x_content">
                    @if( count($payments) < 1 )
                        <span class="center-block text-center" style="font-size:18px">
                            No Payments Made Yet.
                        </span>
                    @else
                        @foreach( $payments as $payment )
                            @if($payment['type'] == 'subscription_auto_payment')
                                <a href="javascript::void()" class="no-site-receipt" data-id="{{ $payment['payment_id'] }}">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 text-left">
                                            @if( $payment['type'] == 'subscription_payment' )
                                                <strong>{{ ucfirst($payment['payment_n']) }} Subscription</strong>
                                            @else
                                                <strong>{{ ucfirst($payment['payment_n']) }} Payment</strong>
                                            @endif
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 text-center">
                                            <strong class="text-success">{{ $payment['amount'] }}</strong>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 text-right">
                                            <strong>{{ date_format($payment['created_at'], 'M d, Y') }}</strong>
                                        </div>
                                    </div>
                                </a>
                                <div class="divider-dashed"></div>
                            @else
                                <a href="{{  $payment['type'] == 'subscription_payment' ? url('show-receipt', $payment['payment_id']) : url('employer/payment-confirmation', $payment['payment_id']) }}">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4  text-left">
                                            @if( $payment['type'] == 'subscription_payment' )
                                                <strong>{{ ucfirst($payment['payment_n']) }} Subscription</strong>
                                            @else
                                                <strong>{{ ucfirst($payment['payment_n']) }} Payment</strong>
                                            @endif
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4  text-center">
                                            <strong class="text-success">{{ $payment['amount'] }}</strong>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4  text-right">
                                            <strong>{{ date_format($payment['created_at'], 'M d, Y') }}</strong>
                                        </div>
                                    </div>
                                </a>
                                <div class="divider-dashed"></div>     
                            @endif            
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

<div class="modal fade" tabindex="-1" role="dialog" id="no-receipt-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Auto Charge Payments</h4>
                </div>
                <div class="modal-body text-center">
                    <span style="font-size:18px" class="center-block"> This payment is available for download only. </span>
                    <br />
                    <a href="" class="btn btn-primary medium-btn" id="download-trigger">Download Here</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
    
    $('.no-site-receipt').on('click', function(e){
        $('#no-receipt-modal').modal('show');
        var invoice = $(this).data('id');
        $('#download-trigger').attr('href', '{{ url("invoice-download")}}/'+invoice);
    });

</script>
@stop