@extends('backend.layouts.master')

@section('title')
    InternLogic - Employer Profile
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-xs-12 col-md-offset-1">
             <h3 class="">Profile</h3>
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                             <h2>
                                  {{ $user->basic_information->company_name }}
                             </h2>
                        </div>
                    </div>
                    <div class="x_title"></div>
                    <div class="row">
                        <div class="col-md-10 col-xs-12 col-md-offset-1">
                           <br />
                           <div class="row">
                               <div class="col-md-6 col-sm-6 col-xs12">
                                    <p>
                                        <strong>Contact Name:</strong> {{ $user->getFullNameAttribute() }}
                                    </p>
                                    <p>
                                        <strong>Email:</strong> {{ $user->email }}
                                    </p>
                                    <p>
                                        <strong>City/State/Zip:</strong> {{ $user->basic_information->city }}, {{ $user->basic_information->state }}, {{ $user->basic_information->zip_code }}
                                    </p>
                               </div>
                               <div class="col-md-6 col-sm-6 col-xs12">
                                    <p>
                                        <strong>Industry Type:</strong> {{ $user->basic_information->industry->industry }}
                                    </p>
                                    <p>
                                        <strong>Business Size:</strong> {{ $user->basic_information->businessSize->size_name }}
                                    </p>
                                    <p>
                                        <strong>Employer Type:</strong> {{ $user->basic_information->employerType->type_name }}
                                    </p>
                               </div>
                           </div>
                        </div>
                    </div>
                    <br />
                    <div class="x_title"></div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 text-center">
                            <a href="{{ url('employer/edit-basic-information', $user->id) }}" class="btn btn-primary medium-btn">Edit Profile</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
@stop