@extends('backend.layouts.master')

@section('title')
    InternLogic - User Profile
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-xs-12 col-md-offset-1">
            <h3 class="">Profile</h3>
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                             <div class="profile_img text-center">
                                <div class="crop-avatar">
                                    <img src="{{ asset('uploads/profile-pic/'.$user->basic_information->prof_pic) }}" width="150" height="150" alt="" class="img-responsive avatar-view center-round" style="margin: 0 auto 12px">
                                    <!-- <img src="http://coloredfaces.com/img/avatars/rJM6HQsLT6bGC8i.png" width="150" alt="" class="img-responsive avatar-view center-round" style="margin: 0 auto 12px"> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <table class="table table-borderless table-striped">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <strong>Personality Type:</strong>
                                                
                                            </td>
                                            <td>
                                                <strong>Major:</strong>
                                                {{ $user->basic_information->major->major }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Email:</strong>
                                                {{ $user->email }}
                                            </td>
                                            <td>
                                                <strong>College:</strong>
                                                {{ mb_strimwidth($user->basic_information->college->college, 0, 20, "...") }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Phone:</strong>
                                                {{ $user->basic_information->phone_number }}
                                            </td>
                                            <td>
                                                <strong>GPA:</strong>
                                                {{ $user->basic_information->gpa }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Location:</strong>
                                                {{ $user->basic_information->city }}, {{ $user->basic_information->state }}
                                            </td>
                                            <td>
                                                <strong>Year:</strong>
                                                {{ $user->basic_information->year }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                    </div>
                    <div class="x_title"></div>
                    <div class="row text-justify">
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <h4>About</h4>
                            <p>{{ $user->basic_information->about }}</p>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <h4>Experience</h4>
                            <p>{{ $user->basic_information->experience }}</p>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <h4>Skills</h4>
                            <?php $skills = explode(',', $user->basic_information->skills ); ?>
                            <!-- <ul> -->
                                @foreach($skills as $skill)
                                    <button class="btn btn-default btn-xs">{{ $skill }}</button>
                                @endforeach
                            <!-- </ul> -->
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <h4>Resument</h4>
                            <a href="{{ asset('uploads/docs/'.$user->basic_information->resume) }}" target="_blank"><i class="fa fa-fw fa-file-text fa-2"></i>Resume</a>
                        </div>
                    </div>
                    <br />
                    <div class="x_title"></div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 text-center">
                            <a href="{{ url('student/edit-basic-information', $user->id) }}" class="btn btn-primary medium-btn">Edit Profile</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
@stop