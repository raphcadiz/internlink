@extends('backend.layouts.master')

@section('title')
    InternLogic - Email Settings
@stop

@section('styles')
<!-- Switchery -->
<link href="{{ asset('vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-xs-12 col-md-offset-2">
            <h3 class="">Email Settings</h3>
            <p>Which email would like to receive?</p>
            @include('backend.layouts.flash-message')
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-9 col-lg-10 text-left">
                            <strong>
                                Notification Type
                            </strong>
                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-3 col-lg-2 text-right">
                            <strong>
                                On/Off
                            </strong>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="x_title"></div>
                <div class="x_content">
                    <form action="" method="POST">
                        {{ csrf_field() }}
                        @foreach($notifications as $notification)
                            <div class="row">
                                <div class="col-md-10 col-sm-10 col-xs-9 col-lg-10 text-left">
                                    <strong>
                                        {{ $notification->notification }}
                                    </strong>
                                    <p>
                                        {{ $notification->description }}
                                    </p>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-3 col-lg-2 text-right">
                                   <label>
                                      <input type="checkbox" value="{{ $notification->id }}" name="notifications[]" class="js-switch" {{ $user->userAllowNotification($notification->id) ? 'checked' : '' }} /> 
                                    </label>
                                </div>
                            </div>
                            <div class="divider-dashed"></div>
                        @endforeach
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                <input type="submit" class="btn btn-success medium-btn" value="Save Email Settings">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

@stop

@section('scripts')
<!-- Switchery -->
<script src="{{ asset('vendors/switchery/dist/switchery.min.js') }}"></script>
@stop