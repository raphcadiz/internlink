@extends('backend.layouts.master')

@section('title')
    InternLogic - Update Password
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-xs-12 col-md-offset-3">
            <h3 class="">Update Password</h3>
            <div class="x_panel">
                <div class="x_content">
                    <form method="POST" role="form" action="{{ url('/update-password', $user->id) }}">
                        {{ csrf_field() }}

                        @include('backend.layouts.flash-message')

                        <div class="form-group">
                            <label for="current_password">Current Password</label>
                            <input type="password" name="current_password" class="form-control" required="required" placeholder="Enter current password">
                        </div>
                        <div class="form-group">
                            <label for="password">New Password</label>
                            <input type="password" name="password" class="form-control" required="required" placeholder="Enter new password">
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Confirm New Password</label>
                            <input type="password" name="password_confirmation" class="form-control" required="required" placeholder="Confirm new password">
                        </div>
                    
                        <input type="submit" class="btn btn-primary medium-btn pull-right" name="submit" value="Update" style="margin:0">
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@stop

@section('scripts')

@stop