@extends('backend.layouts.master')

@section('title')
    InternLogic - Update Email
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-xs-12 col-md-offset-3">
            <h3 class="">Update Email</h3>
            <div class="x_panel">
                <div class="x_content">
                    <form method="POST" role="form" action="{{ url('/update-email', $user->id) }}">
                        {{ csrf_field() }}

                        @include('backend.layouts.flash-message')

                        <div class="form-group">
                            <label for="current_email">Current Email</label>
                            <input type="email" name="current_email" class="form-control" required="required" placeholder="Enter current email" value="{{ $user->email }}" readonly="readonly">
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" required="required" placeholder="Enter password to confirm changes">
                        </div>
                        <div class="form-group{{ $errors->has('new_email') ? ' has-error' : '' }}">
                            <label for="email">New Email</label>
                            <input type="email" name="email" class="form-control" required="required" placeholder="Enter new email" value="{{ old('new_email') }}">
                        </div>
                    
                        <input type="submit" class="btn btn-primary medium-btn pull-right" name="submit" value="Update" style="margin:0">
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@stop

@section('scripts')

@stop