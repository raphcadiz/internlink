@extends('backend.layouts.master')

@section('title')
    InternLogic - Deactivate Account
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-xs-12 col-md-offset-3">
            <h3 class="">Deactivate Account</h3>
            <div class="x_panel">
                <div class="x_content">
                    <h3>Deactivate Confirmation</h3>
                    <p>
                        It's not too late! But, if you must we are sorry to see you go. Please let us know how we could have made your experience better. Come back anytime, we wish you the best on your career search, and thank you for choosing Internlogic!
                    </p>
                    <br />
                    <div class="row">
                        <div class="col-md-6 col-lg-6 co-xs-12">
                            @if($user->hasRole('Student'))
                                <a href="{{ url('student/matches') }}" class="btn btn-success btn-block medium-btn">BACK TO POSTINGS</a>
                            @else
                                <a href="{{ url('employer/applicants') }}" class="btn btn-success btn-block medium-btn">BACK TO POSTINGS</a>
                            @endif
                        </div>
                        <div class="col-md-6 col-lg-6 co-xs-12">
                            <a href="{{ url('deactivate-account', $user->id)}}" class="btn btn-danger btn-block medium-btn">DEACTIVATE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@stop

@section('scripts')

@stop