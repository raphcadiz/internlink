@extends('backend.layouts.master')

@section('title')
    InternLogic - Premium Plan
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 page-title">
            <div class="title_left">
                <h3 class="">Pricing</h3>
            </div>
            <div class="title_right"></div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Premium Features</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="">Get access to all our premium features to land a job!</h1>
                    <span class="center-block" style="font-size:18px">We're not here to break the bank. Our pricing is simple and affordable!</span>
                </div>
            </div>
            <br />
            <div class="row">

              <div class="col-md-12">

                <!-- price element -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="well premium-box text-center">
                        <span class="center-block" style="font-size:55px"><i class="fa fa-fw fa-heart"></i></span>
                        <h4>Unlimited Saved Jobs</h4>
                        <p>Save as many jobs your heart desires and never lose track of great opportunities</p>
                    </div>
                </div>
                <!-- price element -->

                <!-- price element -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="well premium-box text-center">
                        <span class="center-block" style="font-size:55px"><i class="fa fa-fw fa-wechat"></i></span>
                        <h4>Unlimited Messages</h4>
                        <p>Communicate to employer that you are interested in and get their attention to get a job</p>
                    </div>
                </div>
                <!-- price element -->

                <!-- price element -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="well premium-box text-center">
                        <span class="center-block" style="font-size:55px"><i class="fa fa-fw fa-bar-chart"></i></span>
                        <h4>View Advanced Job Compatibility</h4>
                        <p>See Exactly how you match up with every job! You can even use the information to get a job</p>
                    </div>
                </div>
                <!-- price element -->

                <!-- price element -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="well premium-box text-center">
                        <span class="center-block" style="font-size:55px"><i class="fa fa-fw fa-binoculars"></i></span>
                        <h4>See Who's Viewed You</h4>
                        <p>Check out the list of Employer that looked at your profile. You could even send them message to show you're interested</p>
                    </div>
                </div>
                <!-- price element -->
              </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center">
                    @if(Auth::user()->subscribed('premium'))
                        @if(Auth::user()->subscription('premium')->onGracePeriod())
                          <span style="font-size:18px">You Cancelled your Subscription!</span> <br /><br />
                          <a href="{{ url('resume-subscription') }}" class="btn btn-success big-btn">Resume Subscription</a>
                        @else
                          <span style="font-size:18px">You are already subscribed to our premium features!</span> <br /><br />
                          <a href="{{ url('cancel-subscription') }}" class="btn btn-danger big-btn">Cancel Subscription</a>
                        @endif
                    @else
                        <a href="{{ url('plan-subscription/?plan_name=premium') }}" class="btn btn-success big-btn">$4.99/month</a>
                    @endif
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>
<!-- /.container-fluid -->
@stop

@section('scripts')
@stop