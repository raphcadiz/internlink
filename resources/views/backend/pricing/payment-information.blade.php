@extends('backend.layouts.master')
@section('title')
    InternLogic - Premium Plan Payment
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-xs-12 col-md-offset-2">
            <h3 class="">Plan Payment</h3>
            <div class="x_panel">
                <form id="payment-form" method="POST" action="{{ url('/plan-subscription') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="plan_id" value="{{ $plan->id }}">
                    <div class="x_title">
                        <h2>Order Summary</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if($user->trial_consumed())
                            <div class="form-group">
                                <strong>{{ ucfirst($plan->name) }} Subscription</strong>
                                <span class="pull-right applicants-count">${{ $plan->price }}</span>
                                <br />
                                <span>You have consumed your free one month trial already</span>                                
                            </div>
                        @else
                            <div class="form-group">
                                <strong>{{ ucfirst($plan->name) }} Subscription (Free Trial)</strong>
                                <span class="pull-right applicants-count">$0</span>
                                <br />
                                <span>After your free month, you'll pay ${{ $plan->price }}/month <br /> You can cancel anytime.</span>
                                
                            </div>
                        @endif
                    </div>

                    <div class="x_content">                 
                        <br>
                        @include('backend.layouts.flash-message')

                        <div class="divider-dashed"></div>

                        <label for="">Credit Card Information</label><br>
                        <div class="form-group">
                            <span>Credit Card Number</span>
                            <input type="text" class="form-control" placeholder="Card Number" data-stripe="number">
                        </div>
                        <div class="form-group">
                            <span>Cardholder Name</span>
                            <input type="text" class="form-control" placeholder="Name" name="cardholder_name" value="{{ Auth::user()->name }}">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <span>Expiration Date</span>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            {{ Form::selectMonth('', '', ['class' => 'form-control', 'data-stripe' => 'exp_month']) }}
                                        </div>
                                        <div class="col-md-6">
                                            {{ Form::selectYear('', date('Y'), date('Y') + 10, '', ['class' => 'form-control', 'data-stripe' => 'exp_year']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <span>Verification Code</span>
                                    <input type="text" class="form-control" data-stripe="cvc">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="divider-dashed"></div>
                        <label for="">Billing Address</label>
                        <div class="form-group">
                            <span>Address 1</span>
                            <input type="text" name="address_line1" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <span>Address 2</span>
                            <input type="text" name="address_line2" class="form-control">
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <span>City</span>
                                    <input type="text" name="address_city" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <span>State</span><br>
                                    {{ Form::select('address_state', $states, ['class' => 'form-control', 'autocomplete' => 'off', 'required' => 'required']) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <span>Zip Code</span>
                                    <input type="text" name="address_zip" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <span>Country</span>
                                    {{ Form::select('address_country', $countries, ['class' => 'form-control', 'autocomplete' => 'off', 'required' => 'required']) }}
                                </div>
                            </div>
                            <div class="col-md-8"></div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <span>Promo Code</span>
                                    <input type="text" name="promo_code" class="form-control">
                                    <input type="hidden" name="use_promo_code" value="0">
                                </div>
                                <div class="col-md-8">
                                    <br>
                                    <button class="btn btn-primary apply-promo-code" type="button">APPLY CODE</button>
                                    <span class="promo-code-message text-success" style="display: none;"><i class="fa fa-check"></i> Valid promo code.</span>
                                    <span class="promo-code-error text-danger" style="display: none;"><i class="fa fa-close"></i> Invalid promo code.</span>
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12" style="margin-top: 20px;">
                        <div class="form-group text-center">
                            <button class="btn btn-primary medium-btn" type="submit" id="saveTraitSelection">PLACE ORDER</button>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="alert" style="display: none;" id="payment-error"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            $('select').select2();

            var $form = $('#payment-form');

            var stripeKey = $('meta[name="publishable_key"]').attr('content');

            Stripe.setPublishableKey(stripeKey);

            $form.submit(function(event) {
                // Disable the submit button to prevent repeated clicks:
                $form.find('button[type=submit]').prop('disabled', true).html('<i class="fa fa-spinner fa-spin"></i> PROCESSING');

                // Request a token from Stripe:
                Stripe.card.createToken($form, stripeResponseHandler);

                // Prevent the form from being submitted:
                return false;
            });

            $('input[name=applicants_count]').on('keyup', function() {
                if($(this).val() != '') {
                    $('span.applicants-count').html($(this).val());
                    var $total = $(this).val() * 5;
                    $('span.total-budget').html('$'+$total);
                } else {
                    $('span.applicants-count').html(0);
                    $('span.total-budget').html('$0');
                }
            }).on('change', function() {
                if($(this).val() != '') {
                    $('span.applicants-count').html($(this).val());
                    var $total = $(this).val() * 5;
                    $('span.total-budget').html('$'+$total);
                } else {
                    $('span.applicants-count').html(0);
                    $('span.total-budget').html('$0');
                }
            });

        });

        function stripeResponseHandler(status, response) {
            // Grab the form:
            var $form = $('#payment-form');

            if (response.error) { // Problem!

                // Show the errors on the form:
                $form.find('#payment-error').addClass('alert-danger').show().text(response.error.message)
                $form.find('button[type=submit]').prop('disabled', false).html('PLACE ORDER'); // Re-enable submission

            } else { // Token was created!

                // Get the token ID:
                var token = response.id;

                // Insert the token ID into the form so it gets submitted to the server:
                $form.append($('<input type="hidden" name="stripeToken">').val(token));

                // Submit the form:
                $form.get(0).submit();
            }
        };

        $(document).on('click', '.apply-promo-code', function() {
            var $this = $(this),
            $promo_code_input = $('input[name=promo_code]'),
            $promo_code_value = $promo_code_input.val(),
            $promo_code_msg = $('span.promo-code-message'),
            $promo_code_error = $('span.promo-code-error'),
            $token = $('input[name="_token"]').val(),
            $buttons = $('.btn');

            var $def = $.ajax({
                headers: {
                    'X-CSRF-TOKEN' : $token
                },
                url: 'validate-promo-code',
                type: 'POST',
                data: {
                    promo_code : $promo_code_value
                },
                dataType: 'json',
                beforeSend: function() {
                    $this.prop('disabled', true).html('<i class="fa fa-spinner fa-spin"></i> PROCESSING');
                    $buttons.prop('disabled', true);
                    $promo_code_msg.hide();
                    $promo_code_error.hide();
                }
            });

            $def.done(function (res) {
                if(res.valid_promo){
                    $promo_code_input.prop('readonly', true);
                    $buttons.prop('disabled', false);
                    $this.prop('disabled', true).html('APPLY CODE');
                    $promo_code_msg.show();
                    $('input[name=use_promo_code]').val(1);
                } else {
                    $buttons.prop('disabled', false);
                    $this.html('APPLY CODE');
                    $promo_code_error.show();
                    $('input[name=use_promo_code]').val(0);
                }
                
            });

            $def.fail(function (err) {
                $buttons.prop('disabled', false);
                $this.html('APPLY CODE');
                $promo_code_error.show();
                $('input[name=use_promo_code]').val(0);
            });
        });

    </script>
@stop