@extends('backend.layouts.master')
@section('title')
    InternLogic - Payment Confirmation
@stop
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-xs-12 col-md-offset-2">
                <h3 class="">Payment Confirmation</h3>
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Order Confirmation</h2>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <p><strong>Thank you!</strong></p>
                                <p>Please review the confirmation message below and print this page for your records. You should receive a confirmation E-mail of your order in a few minutes. Thank you!</p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <br><br>
                    <div class="x_title">
                        <strong>Order Information</strong>
                        <a href="{{ url('print-subscription-payment', $plan_payment->id) }}" target="_blank" class="pull-right"><i class="fa fa-print"></i></a><br>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="form-group">
                            <strong>Status</strong>
                            <span class="pull-right">Order Processed</span>
                        </div>
                        <div class="form-group">
                            <strong>Order Number</strong>
                            <span class="pull-right">{{ $plan_payment->stripe_id }}</span>
                        </div>
                        <div class="form-group">
                            <strong>Order Date</strong>
                            <span class="pull-right">{{ date_format($plan_payment->created_at, 'm/d/Y ') }}</span>
                        </div>
                        <div class="form-group">
                            <strong>Name</strong>
                            <span class="pull-right">{{ $plan_payment->user->first_name.' '.$plan_payment->user->last_name }}</span>
                        </div>
                        <div class="form-group">
                            <strong>Card Number</strong>
                            <span class="pull-right">XXXX-XXXX-XXXX-{{ $plan_payment->user->card_last_four }}</span>
                        </div>
        
                        <div class="form-group">
                            <strong>Email</strong>
                            <span class="pull-right">{{ $plan_payment->user->email }}</span>
                        </div>
                        <div class="form-group">
                            <strong>Phone </strong>
                            <span class="pull-right"></span>
                        </div>
                        <div class="form-group">
                            <strong>Billing Address</strong>
                            <span class="pull-right">{{ $plan_payment->address_1 }}</span>
                        </div>
                        <div class="form-group">
                            <strong>City</strong>
                            <span class="pull-right">{{ $plan_payment->city }}</span>
                        </div>
                        <div class="form-group">
                            <strong>State</strong>
                            <span class="pull-right">{{ $plan_payment->state }}</span>
                        </div>
                        <div class="form-group">
                            <strong>Zip Code</strong>
                            <span class="pull-right">{{ $plan_payment->zip_code }}</span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br><br>
                    <div class="x_title">
                        <strong>Order Summary</strong>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="form-group">
                            <strong>Payment</strong>
                            <span class="pull-right applicants-count">${{ $plan_payment->amount }}</span>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 20px;">
                        <div class="form-group text-center">
                            @if( Auth::user()->hasRole('Student'))
                                <a href="{{ url('student/matches') }}" class="btn btn-primary medium-btn">TO MATCHES</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop