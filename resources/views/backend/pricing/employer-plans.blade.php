@extends('backend.layouts.master')

@section('title')
    InternLogic - Premium Plans
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

          <div class="x_content">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="">A powerful tool for an affordable price.</h1>
                    <span class="center-block" style="font-size:18px">Get your first posting <strong>FREE!</strong></span>
                </div>
            </div>

            <br /><br />

            <div class="row">
              <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                <table class="table pricing-table pricing-label">
                  <thead>
                    <tr>
                      <th class="text-center">
                        <div class="title">
                            <h2>Bundle Details</h2>
                            <span style="font-weight: 400">Get Access to ALL features at <br /> all level and SAVE more when <br /> you buy more!</span>                          </div>
                      </th>
                    </tr>
                  </thead>
                  <tbody class="text-center">
                    <tr>
                      <td>Advance Compatibility Tool</td>
                    </tr>
                    <tr>
                      <td>Real-Time Dashboard</td>
                    </tr>
                    <tr>
                      <td>Candidate Criterea Filters</td>
                    </tr>
                    <tr>
                      <td>Unlimited Invites to Apply</td>
                    </tr>
                    <tr>
                      <td>Messaging Solution</td>
                    </tr>
                    <tr>
                      <td>1 Click Apply</td>
                    </tr>
                    <tr>
                      <td>Job Amplication</td>
                    </tr>
                    <tr>
                      <td>
                        <strong>Choose Your Plan</strong>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pricing-wrapper">
                     <table class="table table-striped pricing-table pricing-values">
                        <thead>
                          <tr>
                            <th class="text-center">
                              <div class="title">
                                  <h2>Starter</h2>
                                  <h1>1</h1>
                                  <span style="font-weight: 400">Active Posting</span>   
                                </div>
                            </th>
                          </tr>
                        </thead>
                        <tbody class="text-center">
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td>
                              @if(Auth::user()->subscribed('starter'))
                                    @if(Auth::user()->subscription('starter')->onGracePeriod())
                                      <a href="{{ url('resume-subscription') }}" class="btn btn-primary  btn-block medium-btn">Resume Subscription</a>
                                      <span style="font-size:14px">You cancelled subscription!</span>
                                    @else
                                      <a href="{{ url('cancel-subscription') }}" class="btn btn-danger  btn-block medium-btn">Cancel Subscription</a>
                                      <span style="font-size:14px">You are subscribed!</span>
                                    @endif
                                @else
                                    <a href="{{ url('plan-subscription/?plan_name=starter') }}" class="btn btn-success btn-block medium-btn">$49.99/mo</a>
                                @endif
                            </td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pricing-wrapper">
                     <table class="table table-striped pricing-table pricing-values">
                        <thead>
                          <tr>
                            <th class="text-center">
                              <div class="title">
                                  <h2>Plus</h2>
                                  <h1>3</h1>
                                  <span style="font-weight: 400">Upto 3 Active <br /> Posting at a time</span>   
                                </div>
                            </th>
                          </tr>
                        </thead>
                        <tbody class="text-center">
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td>
                                @if(Auth::user()->subscribed('plus'))
                                    @if(Auth::user()->subscription('plus')->onGracePeriod())
                                      <a href="{{ url('resume-subscription') }}" class="btn btn-primary  btn-block medium-btn">Resume Subscription</a>
                                      <span style="font-size:14px">You cancelled subscription!</span>
                                    @else
                                      <a href="{{ url('cancel-subscription') }}" class="btn btn-danger  btn-block medium-btn">Cancel Subscription</a>
                                      <span style="font-size:14px">You are subscribed!</span>
                                    @endif
                                @else
                                    <a href="{{ url('plan-subscription/?plan_name=plus') }}" class="btn btn-success btn-block medium-btn">$99.99/mo</a>
                                @endif
                            </td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pricing-wrapper">
                     <table class="table table-striped pricing-table pricing-values">
                        <thead style="background: #36b39a;">
                          <tr>
                            <th class="text-center">
                              <div class="title">
                                  <h2>Pro</h2>
                                  <h1>5</h1>
                                   <span style="font-weight: 400">Upto 5 Active <br /> Posting at a time</span>   
                                </div>
                            </th>
                          </tr>
                        </thead>
                        <tbody class="text-center">
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td><i class="fa fa-fw fa-check text-success fa-2"></i></td>
                          </tr>
                          <tr>
                            <td>
                                @if(Auth::user()->subscribed('pro'))
                                    @if(Auth::user()->subscription('pro')->onGracePeriod())
                                      <a href="{{ url('resume-subscription') }}" class="btn btn-primary  btn-block medium-btn">Resume Subscription</a>
                                      <span style="font-size:14px">You cancelled subscription!</span>
                                    @else
                                      <a href="{{ url('cancel-subscription') }}" class="btn btn-danger  btn-block medium-btn">Cancel Subscription</a>
                                      <span style="font-size:14px">You are subscribed!</span>
                                    @endif
                                @else
                                    <a href="{{ url('plan-subscription/?plan_name=pro') }}" class="btn btn-success btn-block medium-btn">$149.99/mo</a>
                                @endif
                            </td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            </div>
          </div><!--end content -->
        </div>
      </div>
    </div>

</div>
<!-- /.container-fluid -->
@stop

@section('scripts')
@stop