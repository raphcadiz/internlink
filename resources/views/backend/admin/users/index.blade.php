@extends('backend.layouts.master')

@section('styles')
<!-- Datatables -->
<link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                            Users <small>User Management</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <a href="javascript:void(0)" id="add-new-user" class="btn btn-info pull-right medium-btn" style="margin: 0 0 17px;">Add New</a>
        </div>
    </div>

    <div class="clearfix"></div>


    <div class="row">
        <div class="col-lg-4">
          <div class="x_panel">
            <h4><i class="fa fa-user"></i> Total Users: <span>{{  collect($users)->count() }}</span></h4>
            <h5 class="pull-left"  style="margin-right: 15px;">Administrator: 
                <span>{{ 
                        collect($users)->where('role_id', 1)->count()
                      }}
                </span>
            </h5>
            <h5 class="pull-left" style="margin-right: 15px;">Employer: 
                <span>{{ 
                        collect($users)->where('role_id', 2)->count()
                      }}
                </span>
            </h5>
            <h5 class="pull-left" style="margin-right: 15px;">Student: 
                <span>{{ 
                        collect($users)->where('role_id', 3)->count()
                      }}
                </span>
            </h5>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="x_panel">
             <h4><i class="fa fa-check-circle"></i> Total Active Users: <span>{{  collect($users)->where('deleted_at', null)->count() }}</span></h4>
            <h5 class="pull-left"  style="margin-right: 15px;">Administrator: 
                <span>{{ 
                        collect($users)->where('role_id', 1)->where('deleted_at', null)->count()
                      }}
                </span>
            </h5>
            <h5 class="pull-left" style="margin-right: 15px;">Employer: 
                <span>{{ 
                        collect($users)->where('role_id', 2)->where('deleted_at', null)->count()
                      }}
                </span>
            </h5>
            <h5 class="pull-left" style="margin-right: 15px;">Student: 
                <span>{{ 
                        collect($users)->where('role_id', 3)->where('deleted_at', null)->count()
                      }}
                </span>
            </h5>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="x_panel">
            <h4><i class="fa fa-times-circle"></i> Total Inactive Users: <span>{{  collect($users)->where('deleted_at', '!=', null)->count() }}</span></h4>
            <h5 class="pull-left"  style="margin-right: 15px;">Administrator:
                <span>{{ 
                        collect($users)->where('role_id', 1)->where('deleted_at','!=',null)->count()
                      }}
                </span>
            </h5>
            <h5 class="pull-left" style="margin-right: 15px;">Employer: 
                <span>{{ 
                        collect($users)->where('role_id', 2)->where('deleted_at','!=',null)->count()
                      }}
                </span>
            </h5>
            <h5 class="pull-left" style="margin-right: 15px;">Student: 
                <span>{{ 
                        collect($users)->where('role_id', 3)->where('deleted_at','!=',null)->count()
                      }}
                </span>
            </h5>
          </div>
        </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <div class="clearfix"></div>
            @include('backend.layouts.flash-message')
            @if(count($users) >= 1 )
                <table id="users-table" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Status</th>
                            <th>Verified</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $users as $user )
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role->role }}</td>
                                <td>{{ Carbon\Carbon::parse($user->created_at)->format('M-d-Y') }}</td>
                                <td>{{ Carbon\Carbon::parse($user->updated_at)->format('M-d-Y') }}</td>
                                <td>{{ ($user->trashed()) ? 'Deleted' : 'Active' }}</td>
                                <td>{{ ($user->verified) ? 'Verified' : 'Unverified' }}</td>
                                <td>
                                    <a href="javascript:void(0);" class="text-success update-user" data-content="{{ $user->id }}"><i class="fa fa-fw fa-pencil"></i></a>
                                    |
                                    @if(!$user->trashed())
                                        <a href="javascript:void(0);" class="text-danger delete-user" data-content="{{ $user->id }}"><i class="fa fa-fw fa-trash"></i></a>
                                    @else
                                        <a href="{{ url('administrator/user/restore', $user->id) }}"><i class="fa fa-fw fa-recycle"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-info fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            No Users Yet
                </div>
            @endif
          </div>
        </div>
      </div>
    </div>


</div>
<!-- /.container-fluid -->
@stop

@section('scripts')
<!-- Datatables -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-scroller/js/datatables.scroller.min.js') }}"></script>
<script src="{{ asset('vendors/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('vendors/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('vendors/pdfmake/build/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin-user-management.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var handleDataTableButtons = function() {
          if ($("#users-table").length) {
            $("#users-table").DataTable({
              "pageLength": 50,
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        TableManageButtons.init();
    });
</script>
@stop