<div class="modal fade">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Update Career</h4>
      </div>
      <form method="POST" action="{{ url('administrator/edit/career/update-career', $careers->id) }}">
        {{ csrf_field() }}
        <div class="modal-body row">
            <div class="col-md-8 col-xs-12 col-md-offset-2">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label for="career">Career:</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" name="career" class="form-control" value="{{ $careers->career }}" autofocus>
                        </div>
                    </div>                         
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-primary" value="Save">
        </div>
      </form>
    </div>
  </div>
</div>