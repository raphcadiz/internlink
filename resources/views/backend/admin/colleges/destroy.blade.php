<div class="modal fade">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Delete College</h4>
      </div>
        <div class="modal-body">
            <span>Do you want to skip trash and permanently delete this plan?</span>
        </div>
        <div class="modal-footer">      
            <a href="{{ url('administrator/edit/college/delete-permanently', $colleges->id) }}" class="btn btn-default">Skip Trash</a> 
            <a href="{{ url('administrator/edit/college/delete-trash', $colleges->id) }}" class="btn btn-primary">Delete</a>  
        </div>
    </div>
  </div>
</div>