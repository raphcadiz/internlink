@extends('backend.layouts.master')

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                            Industry <small>Industries Management</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <a href="{{ url('/dashboard') }}">Dashboard</a>
                    </li>
                    <li>
                        Industries
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

    <br />

    <div class="row">
        <div class="col-lg-12">
        @include('backend.layouts.flash-message')
        @if(count($industries) >= 1 )
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Industry</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $industries as $industry )
                        <tr>
                            <td>{{ $industry->id }}</td>
                            <td>{{ $industry->industry }}</td>
                            <td>{{ Carbon\Carbon::parse($industry->created_at)->format('M-d-Y') }}</td>
                            <td>{{ Carbon\Carbon::parse($industry->updated_at)->format('M-d-Y') }}</td>
                            <td>{{ ($industry->trashed()) ? 'Deleted' : 'Active' }}</td>
                            <td>
                                <a href="#" class="text-success"><i class="fa fa-fw fa-pencil"></i></a>
                                |
                                <a href="{{ url('administrator/delete-industry', $industry->id) }}" onclick="return confirm('Are you sure?');" class="text-danger"><i class="fa fa-fw fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-info fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        No Industries Yet
            </div>
        @endif
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@stop

@section('scripts')
@stop