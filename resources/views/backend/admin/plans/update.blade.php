<div class="modal fade">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Update Plan</h4>
      </div>
      <form method="POST" action="{{ url('administrator/plans/update-plan', $plans->id) }}">
        {{ csrf_field() }}
        <div class="modal-body row">
            <div class="col-md-8 col-xs-12 col-md-offset-2">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label for="plan_name">Name:</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" name="name" class="form-control" placeholder="" value="{{ $plans->name }}">
                        </div>
                    </div>                         
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label for="description">Description:</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" name="description" class="form-control" value="{{ $plans->alias }}">
                        </div>
                    </div>                         
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label for="price">Price:</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="number" step="0.01" name="price" class="form-control" value="{{ $plans->price }}">
                        </div>
                    </div>                         
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label for="type">Type:</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <select class="form-control" name="type">
                                <option value="student" {{ $plans->name == 'premium' ? 'selected' : '' }} >Student</option>
                                <option value="business" {{ $plans->name != 'premium' ? 'selected' : '' }}>Business</option>
                            </select>
                        </div>
                    </div>                         
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-primary" value="Save">
        </div>
      </form>
    </div>
  </div>
</div>