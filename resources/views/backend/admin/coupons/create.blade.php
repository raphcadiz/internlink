<div class="modal fade">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Add New User</h4>
      </div>
      <form method="POST" action="{{ url('administrator/coupons/add-new') }}">
        {{ csrf_field() }}
        <div class="modal-body row">
            <div class="col-md-8 col-xs-12 col-md-offset-2">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label for="percent_off">Percent off:</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" name="percent_off" class="form-control" placeholder="Give users this percentage off invoices">
                        </div>
                    </div>                         
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label for="amount_off">Amount off:</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="text" name="amount_off" class="form-control">
                        </div>
                    </div>                         
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label for="duration">Duration:</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <select class="form-control" name="duration">
                                <option value="once">Once</option>
                                <option value="multi-month">Multi-month</option>
                                <option value="forever">Forever</option>
                            </select>
                        </div>
                    </div>                         
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-primary" value="Save">
        </div>
      </form>
    </div>
  </div>
</div>