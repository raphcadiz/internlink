@extends('backend.layouts.master')

@section('title')
    InternLogic - matches
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 page-title">
            <div class="title_left">
                <h3 class="">Matches</h3>
            </div>
            <div class="title_right"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-search"></i> Refine Search</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    {{ Form::open(['action' => 'MatchesController@postFilterMatches', 'id' => 'filter-matches']) }}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="keyword">Keyword</label>
                        <input type="text" name="keyword" class="form-control" placeholder="Marketing, Design..">
                    </div>
                    <div class="form-group">
                        <label for="location">Location</label>
                        <input type="text" name="location" class="form-control" placeholder="(City, State, or ZipCode)">
                    </div>
                    <div class="form-group">
                        <label for="company">Company</label>
                        <input type="text" name="company" class="form-control" placeholder="e.g. Google">
                    </div>
                    <div class="form-group">
                        <label for="job_type">Show Me</label>
                        <div class="checkbox" style="margin-top:0">
                            <label>
                                {{ Form::checkbox('', 'all', false, array('class' => 'job-type check-all', 'autocomplete' => 'off', 'data-class-name' => 'job-type')) }} All
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'internship', false, array('class' => 'job-type check-one', 'autocomplete' => 'off', 'data-class-name' => 'job-type')) }} Internship
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'job', false, array('class' => 'job-type check-one', 'autocomplete' => 'off', 'data-class-name' => 'job-type')) }} Job
                            </label>
                        </div>
                    </div>
                    <div class="border-separator"></div>
                    <div class="form-group">
                        <label>Main</label>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'applied-jobs', false, array('class' => 'main', 'autocomplete' => 'off')) }} <i class="fa fa-check text-success"></i> Applied
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'invited-jobs', false, array('class' => 'main', 'autocomplete' => 'off')) }} <i class="fa fa-paper-plane text-success"></i> Invited to Apply
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'viewed-me', false, array('class' => 'main', 'autocomplete' => 'off')) }} <i class="fa fa-eye text-primary"></i> Who's viewed You
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'saved-jobs', false, array('class' => 'main', 'autocomplete' => 'off')) }} <i class="fa fa-heart text-warning"></i> Saved
                            </label>
                        </div>
                    </div>

                    <div class="border-separator"></div>
                    <div class="form-group">
                        <label for="">Compatibility</label>
                    </div>
                    <div class="form-group">
                        {{ Form::text('compatibility', '', [
                            'class' => 'form-control filter-slider',
                            'data-slider-min' => '25',
                            'data-slider-max' => '100',
                            'data-slider-step' => '1',
                            'data-slider-ticks' => '[25,50,75,100]',
                            'data-slider-ticks-labels' => '["25%","50%","75%","100%"]',
                            'data-slider-value' => '25',
                            'style' => 'width: 100%'
                        ]) }}
                    </div>
                    <div class="border-separator"></div>
                    <div class="form-group">
                        <label for="">Industry</label>
                        <select name="industry_id" id="" class="form-control">
                            <option value="0">All</option>
                            @foreach($industries as $industry)
                                <option value="{{ $industry->id }}">{{ $industry->industry }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="border-separator"></div>
                    <div class="form-group">
                        <label for="">Careers</label>
                        <select name="career_id" id="" class="form-control">
                            <option value="0">All</option>
                            @foreach($careers as $career)
                                <option value="{{ $career->id }}">{{ $career->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{ Form::checkbox('career_match', '1', false, array('class' => '', 'autocomplete' => 'off')) }} Career Match
                    </div>
                    <div class="border-separator"></div>
                    <div class="form-group">
                        <label for="">Time Commitment</label>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'all', false, array('class' => 'time-commitment check-all', 'autocomplete' => 'off', 'data-class-name' => 'time-commitment')) }} All
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'Part-Time', false, array('class' => 'time-commitment check-one', 'autocomplete' => 'off', 'data-class-name' => 'time-commitment')) }} Part-Time
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'Full-Time', false, array('class' => 'time-commitment check-one', 'autocomplete' => 'off', 'data-class-name' => 'time-commitment')) }} Full-Time
                            </label>
                        </div>
                    </div>
                    <div class="border-separator"></div>
                    <div class="form-group">
                        <label for="">Compensation</label>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'all', false, array('class' => 'compensation check-all', 'autocomplete' => 'off', 'data-class-name' => 'compensation')) }} All
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'paid', false, array('class' => 'compensation check-one', 'autocomplete' => 'off', 'data-class-name' => 'compensation')) }} Paid
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'unpaid', false, array('class' => 'compensation check-one', 'autocomplete' => 'off', 'data-class-name' => 'compensation')) }} Unpaid
                            </label>
                        </div>
                    </div>
                    <div class="border-separator"></div>
                    <div class="form-group">
                        <label for="">Location Type</label>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'all', false, array('class' => 'location check-all', 'autocomplete' => 'off', 'data-class-name' => 'location')) }} All
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'virtual', false, array('class' => 'location check-one', 'autocomplete' => 'off', 'data-class-name' => 'location')) }} Virtual
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'one', false, array('class' => 'location check-one', 'autocomplete' => 'off', 'data-class-name' => 'location')) }} Local
                            </label>
                        </div>
                    </div>
                    <div class="border-separator"></div>
                    <div class="form-group">
                        <label for="">Employer Type</label>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'all', false, array('class' => 'employer-type check-all', 'autocomplete' => 'off', 'data-class-name' => 'employer-type')) }} All
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'profit', false, array('class' => 'employer-type check-one', 'autocomplete' => 'off', 'data-class-name' => 'employer-type')) }} Profit
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'non-profit', false, array('class' => 'employer-type check-one', 'autocomplete' => 'off', 'data-class-name' => 'employer-type')) }} Non-Profit
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'government', false, array('class' => 'employer-type check-one', 'autocomplete' => 'off', 'data-class-name' => 'employer-type')) }} Government
                            </label>
                        </div>
                    </div>
                    <div class="border-separator"></div>
                    <div class="form-group">
                        <label for="">Employer Size</label>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'small', false, array('class' => 'employer-size', 'autocomplete' => 'off')) }} Small (0-99)
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'medium', false, array('class' => 'employer-size', 'autocomplete' => 'off')) }} Medium (100-499)
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('', 'large', false, array('class' => 'employer-size', 'autocomplete' => 'off')) }} Large (500+)
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::submit('Submit', ['class' => 'btn btn-success btn-block medium-btn']) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
            <div class="x_panel no-padding">
                <div class="x_content no-padding" style="margin:0;">
                    <div class="row">
                        <div class="col-md-12 text-center loading-matches" style="padding:20px">
                            <i class="fa fa-spinner fa-spin fa-1x"></i> Loading results.
                        </div>
                        <div class="col-md-12 matches-result-table"></div>
                    </div>
                </div>
            </div>
            <div class="text-right">

            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

<div class="modal fade" tabindex="-1" role="dialog" id="maxxed-saved-jobs">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Maximum Save Jobs</h4>
                </div>
                <div class="modal-body text-center">
                    <p style="font-size:18px" class="center-block"> You have reached maximum jobs to be saved.<br /> Upgrade to premium for unlimited saved jobs. </p>
                    <a href="{{ url('student/subscription-pricing') }}" class="btn btn-primary medium-btn" id="download-trigger">Upgrade</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript" src="{{ asset('js/matches.js') }}"></script>
@stop
