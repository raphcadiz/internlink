<table id="job-matches" class="table no-border">
    @foreach($matches as $match)
        <tr>
            <td align="center" valign="center">
                @if( $user->isAppliedJob($match['job_id']) )
                    <i class="fa fa-fw fa-check text-success"></i>
                @endif
            </td>
            <td valign="center" data-id="{{ $match['job_id'] }}" class="go-to-job">
                <strong>{{ $match['title'] }}</strong>
                @if( $user->isInvitedJob($match['job_id']) )
                    <i class="fa fa-fw fa-paper-plane text-success"></i>
                @endif
                @if( $user->isPremiumStudent() && $user->isViewedBy($match['employer_id']))
                    <i class="fa fa-fw fa-eye text-success"></i>
                @endif
                <br />
                {{ $match['company'] }}
                {{ $match['city'] }}
                {{ $match['state'] }}
            </td>
            <?php
            $personality_type = $user->personalityType();
            $careers =  json_decode($personality_type->careers);
            ?>
            <td valign="center" style="font-size:14px">
                @if( in_array($match['career'], $careers) )
                    <strong>{{ $match['career'] }}</strong>
                @else
                    {{ $match['career'] }}
                @endif
            </td>
            <td valign="center">
                <strong style="font-size:18px" class="text-success"> {{ ($match['compatibility'] != 0.0) ? $match['compatibility'].'%' : 'N/A' }}</strong>
            </td>
            <td valign="bottom">
                <a href="javascript:void(0)" class="save-job" data-content="{{ $match['job_id'] }}">
                    <i class="fa fa-fw fa-heart {{ $user->isSavedJob($match['job_id']) ? 'saved-job' : '' }}"></i>
                </a>
            </td>
        </tr>
    @endforeach
    @if(empty($matches))
        <tr>
            <td class="text-center" colspan="5">No matches.</td>
        </tr>
    @endif
</table>