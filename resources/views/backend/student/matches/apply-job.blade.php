@extends('backend.layouts.master')
@section('title')
    InternLogic - Apply Job
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 page-title">
            <div class="title_left">
                <h3 class="">Apply Job</h3>
            </div>
            <div class="title_right"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-xs-12 col-md-offset-1">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 text-left">
                            <h3>{{ $job->title }}</h3>
                            {{ $job->user->basic_information->company_name }}
                            {{ !empty($job->user->basic_information->city) ? '- '.$job->user->basic_information->city : '' }}{{ !empty($job->user->basic_information->state) ? ', '.$job->user->basic_information->state : '' }}
                        </div>
                        <div class="col-md-6 col-xs-12 text-right">
                            <strong style="font-size: 18px;margin-top: 10px;display: block;" class="text-success">{{ round($job->getStudentCompatibilityPercentage(Auth::user())) }}%</strong>
                        </div>
                    </div>
                    <div class="x_title"></div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Job Details</h4>
                            <table class="table table-borderless table-striped">
                                <tbody>
                                    <tr>
                                        <td style="border-top:0">
                                            <strong>Career:</strong>
                                            {{ $job->career->title }}
                                        </td>
                                        <td style="border-top:0">
                                            <strong>Employement Type:</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top:0">
                                            <strong>Industry:</strong>
                                            {{ $job->user->basic_information->industry->industry }}
                                        </td>
                                        <td style="border-top:0">
                                            <strong>Duration:</strong>
                                            {{ date('m/d/Y', strtotime($job->job_start)) }} - {{ date('m/d/Y', strtotime($job->job_end)) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top:0">
                                            <strong>Position:</strong>
                                            {{ $job->employment_type }} / {{ $job->paid }}
                                        </td>
                                        <td style="border-top:0">
                                            <strong>Rate:</strong>
                                            ${{ $job->salary }} {{ $job->rate }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top:0">
                                            <strong>Posting Date:</strong>
                                            {{ Carbon\Carbon::parse($job->created_at)->format('n/d/Y') }} ({{ Carbon\Carbon::now()->subDays(Carbon\Carbon::parse($job->created_at)->diffInDays())->diffForHumans() }})
                                        </td>
                                        <td style="border-top:0">
                                            <strong>Location Type:</strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="x_title"></div>
                    <div class="x_title">
                        <div class="row">
                            <div class="text-center">
                                <a href="javascript::void(0)" id="{{ !Auth::user()->isPremiumStudent() ? 'view-compatibility-free-user' : 'view-compatibility' }}" style="color: #ffaa0e;font-size: 18px;" data-content="{{ $job->id }}">View Advanced Compatibility <span class="fa fa-chevron-down"></span></a>
                                @if( Auth::user()->isPremiumStudent() )
                                    <div id="compatibility-data-container" class="text-left center-block" style="display:none; margin: 20px 0">
                                        <div class="text-center label-traits">
                                            <span><i class="fa fa-fw fa-square" style="color: #1ebc9e;"></i> = Your Traits</span>
                                            <span><i class="fa fa-fw fa-square" style="color: #ffaa0e;"></i> = Posting Traits</span>
                                        </div>
                                        @foreach( $job->getStudentCompatibility(Auth::user()) as $job_compatibility )
                                            <?php
                                                $assessment = App\Models\Assessment::find($job_compatibility->assessment_id);
                                            ?>
                                            @if( $assessment )
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <input class="form-control assessment-sliders" name="assessment[{{ $assessment->id }}]" data-trait-id="{{ $assessment->id }}" style="width: 100%;" id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="[{{ round($job_compatibility->sss) }},{{ $job_compatibility->ess }}]"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-lg-6 col-xs-6 text-left"><strong>{{ $assessment->attribute_l }}</strong></div>
                                                <div class="col-md-6 col-lg-6 col-xs-6 text-right"><strong>{{ $assessment->attribute_r }}</strong></div>
                                            </div>
                                            <br />
                                            @endif
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row text-justify">
                        <div class="col-md-12">
                            <h4>Description</h4>
                            <p>{{ $job->company_description }}</p>
                        </div>
                        <div class="col-md-12">
                            <h4>Responsibilities</h4>
                            <p>{{ $job->responsibilities }}</p>
                        </div>
                        <div class="col-md-12">
                            <h4>Requirements</h4>
                            <p>{{ $job->requirements }}</p>
                        </div>
                        <div class="col-md-12">
                            <h4>Skills</h4>
                            <?php $skills = explode(',', $job->skills); ?>
                            @foreach($skills as $skill)
                               <button class="btn btn-default btn-xs">{{ $skill }}</button>
                            @endforeach
                        </div>
                    </div>
                    <div class="x_title"></div>
                    <div class="row">
                        <br />
                        <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                            <a href="javascript:;" class="save-job" data-content="{{ $job->id }}" data-toggle="tooltip" data-original-title="save">
                                <i class="fa fa-fw fa-heart fa-2 {{ Auth::user()->isSavedJob($job->id) ? 'saved-job' : '' }}"></i><br />
                                Save Job
                            </a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                            <a href="{{ url("messages/new/$job->id", Auth::user()->id) }}">
                                <i class="fa fa-fw fa-weixin fa-2"></i><br />
                                Message
                            </a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                            @if( Auth::user()->isAppliedJob($job->id) )
                                <a href="javascript:void(0)" class="" data-content="{{ $job->id }}">
                                    <i class="fa fa-fw fa-check fa-2 saved-job"></i><br />
                                    Applied
                                </a>
                            @else
                                @if( Auth::user()->basic_information->confirm_on_apply == 1)
                                    <a href="javascript:void(0)" class="apply-with-confirm" data-content="{{ $job->id }}">
                                        <i class="fa fa-fw fa-check fa-2 "></i><br />
                                        Apply
                                    </a>
                                @else
                                    <a href="javascript:void(0)" class="apply-job" data-content="{{ $job->id }}">
                                    <i class="fa fa-fw fa-check fa-2 "></i><br />
                                    Apply
                                </a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="apply-with-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Application Confirmation</h4>
                </div>
                <div class="modal-body text-center">
                    <p style="font-size:18px" class="center-block"> Are you sure you want to apply to this job? </p>
                    <div class="text-center">
                        <a href="javascript:void(0)" data-content="{{ $job->id }}" class="btn btn-success medium-btn apply-job">Yes</a>
                        <a href="#" class="btn btn-danger medium-btn" data-dismiss="modal">No</a>
                    </div>    
                </div>
                <div class="modal-footer">
                    <div class="text-right">
                        <div class="checkbox" style="margin-top:0">
                            <label>
                                <input type="checkbox" value="1" id="dont-confirm-again">Check this box if you don't want to be ask again.
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="application-submitted">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Application Submitted</h4>
                </div>
                <div class="modal-body text-center">
                    <div class="text-center">
                        <p style="font-size:18px" class="center-block"> Application Successfully Submitted </p>
                        <span class="center-block"><i class="success-check fa fa-check-circle"></i></span>
                        <a href="#" class="" data-dismiss="modal">Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="maxxed-saved-jobs">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Maximum Save Jobs</h4>
                </div>
                <div class="modal-body text-center">
                    <p style="font-size:18px" class="center-block"> You have reached maximum jobs to be saved.<br /> Upgrade to premium for unlimited saved jobs. </p>
                    <a href="{{ url('student/subscription-pricing') }}" class="btn btn-primary medium-btn" id="download-trigger">Upgrade</a>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('scripts')
<script type="text/javascript" src="{{ asset('js/apply-job.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.assessment-sliders').slider();
        $('.assessment-sliders').slider( 'disable');
    });
</script>
@stop