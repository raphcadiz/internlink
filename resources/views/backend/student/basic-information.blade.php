@extends('backend.layouts.master')

@section('title')
    InternLogic - Basic Information
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-xs-12 col-md-offset-3">
            <h3 class="">Basic Information</h3>
            @include('backend.layouts.flash-message')
            <div class="x_panel">
                <div class="x_content">
                    <form method="POST" role="form" action="{{ url('/student/edit-basic-information', $student_information->user->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <h4 style="margin-top:24px">Basic Information</h4>
                        <div class="row">
                            <div class="col-md-4 col-xs-12 form-group">
                                <label>First Name</label>
                                <input type="text" name="first_name" required="required" class="form-control" value="{{ $student_information->user->first_name }}" placeholder="First Name">
                            </div>
                            <div class="col-md-4 col-xs-12 form-group">
                                <label>Middle Name</label>
                                <input type="text" name="middle_name" required="required" class="form-control" value="{{ $student_information->user->middle_name }}" placeholder="Middle Name">
                            </div>
                            <div class="col-md-4 col-xs-12 form-group">
                                <label>Last Name</label>
                                <input type="text" name="last_name" required="required" class="form-control" value="{{ $student_information->user->last_name }}" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" name="address" class="form-control" value="{{ $student_information->address }}" placeholder="Address" required="required">
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12 form-group">
                                <label>City</label>
                                <input type="text" name="city" class="form-control" value="{{ $student_information->city }}" placeholder="City" required="required">
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 form-group">
                                        <label>State</label>
                                        <input type="text" name="state" class="form-control" value="{{ $student_information->state }}" placeholder="CA">
                                    </div>
                                    <div class="col-md-9 col-xs-12 form-group">
                                        <label>Zip Code</label>
                                        <input type="text" name="zip_code" class="form-control" value="{{ $student_information->zip_code }}" placeholder="Zip Code" required="required">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="phone_number" class="form-control" value="{{ $student_information->phone_number }}" placeholder="Number">
                        </div>
                        <div class="form-group">
                            <label>Date of Birth</label>
                            <input type="text" name="dob" id="dob" class="date-picker form-control" required="required" value="{{ $student_information->dob }}" placeholder="Birthday">
                        </div>
                        <div class="form-group">
                            <label>Profile Picture</label>
                            <input type="file" name="prof_pic">
                        </div>
                        <div class="form-group">
                            <label for="college_id">College</label>
                            <select name="college_id" id="" class="select_college form-control" required="required">
                                <option></option>
                                @foreach($colleges as $college)
                                    <option value="{{ $college->id }}" {{ ( $student_information->college_id == $college->id ) ? 'selected' : '' }}>{{ $college->college }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="major_id">Major</label>
                            <select name="major_id" id="" class="select_major form-control" required="required">
                                <option></option>
                                @foreach($majors as $major)
                                    <option value="{{ $major->id }}" {{ ( $student_information->major_id == $major->id ) ? 'selected' : '' }}>{{ $major->major }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12  form-group">
                                <label for="year">Year</label>
                                <select name="year" id="" class="form-control" required>
                                    <option value="">Select</option>
                                    <option {{ ($student_information->year == 'graduated') ? 'selected' : '' }} value="graduated">Graduated</option>
                                    <option {{ ($student_information->year == 'senior') ? 'selected' : '' }} value="senior">Senior</option>
                                    <option {{ ($student_information->year == 'junior') ? 'selected' : '' }} value="junior">Junior</option>
                                    <option {{ ($student_information->year == 'sophomore') ? 'selected' : '' }} value="sophomore">Sophomore</option>
                                    <option {{ ($student_information->year == 'freshman') ? 'selected' : '' }} value="freshman">Freshman</option>
                                    <option {{ ($student_information->year == 'other') ? 'selected' : '' }} value="other">Other</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-xs-12  form-group">
                                <label for="gpa">GPA</label>
                                <input type="text" name="gpa" class="form-control" value="{{ $student_information->gpa }}" placeholder="GPA">
                            </div>
                        </div>

                        <h4 style="margin-top:24px">Professional Information</h4>

                        <div class="form-group">
                            <label>About</label>
                            <textarea class="form-control" rows="4" name="about">{{ $student_information->about }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Experience</label>
                            <textarea class="form-control" rows="4" name="experience">{{ $student_information->experience }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Education</label>
                            <textarea class="form-control" rows="4" name="education">{{ $student_information->education }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Skills</label>
                            <input id="skills" name="skills" type="text" class="tags form-control" value="{{ $student_information->skills }}" />
                            <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                        </div>
                        <div style="clear:both;display:block"></div>
                        <div class="form-group">
                            <label>Resume</label>
                            <input type="file" name="resume">
                        </div>

                        <input type="submit" class="btn btn-primary medium-btn pull-right" name="submit" value="Update" style="margin:0">
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@stop

@section('scripts')
<script src="{{ asset('js/moment/moment.min.js') }}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>

<!-- Select2 -->
<script src="{{ asset('vendors/select2/dist/js/select2.full.min.js') }}"></script>
<!-- jQuery Tags Input -->
<script src="{{ asset('vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>

<!-- bootstrap-daterangepicker -->
<script>
    $(document).ready(function() {
        $('#dob').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $(".select_college").select2({
            placeholder: "Select a Collge",
            allowClear: true
        });

        $(".select_major").select2({
            placeholder: "Select a Major",
            allowClear: true
        });

        $("select[name=year]").select2();

        $('#skills').tagsInput({
            width: 'auto'
        });
    });
</script>
<!-- /bootstrap-daterangepicker -->
@stop