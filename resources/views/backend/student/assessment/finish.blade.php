@extends('backend.layouts.master')

@section('title')
    InternLogic - Assessment Success
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <br />
    <br />
    <div class="row">
        <div class="col-md-6 col-xs-12 col-md-offset-3">
            <div class="x_panel">
                <div class="x_content text-center">
                    <h3 class="">Congratulations!</h3>

                    <p style="font-size:18px">Now let's check some matches and get you the career of your dreams!</p>

                    <span class="center-block"><i class="success-check fa fa-check-circle"></i></span>

                    <a href="{{ url('student/matches') }}" class="btn btn-success medium-btn">GO TO MATCHES</a>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@stop

@section('scripts')

@stop