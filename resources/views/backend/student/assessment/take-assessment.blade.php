@extends('backend.layouts.master')
@section('title')
    InternLogic - Take Assessment
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9 col-xs-12 col-md-offset-1">
            <h3 class="">Take Assessment</h3>
            @if(Session::has('message'))
                <div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            {{ Session::get('message') }}
                </div>
            @endif
            <div class="x_panel">
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center" style="font-size:18px;margin-bottom:33px">Use the slider to select which statement you aggree most with.</p>
                    </div>
                </div>
                <br /><br />
                <form method="POST" action="{{ url('student/assessment') }}" id="assessment-form">
                    {{ csrf_field() }}
                    <div id="wizard" class="form_wizard wizard_horizontal">
                        <ul class="wizard_steps" style="display:none">
                            @foreach($assessments as $row => $assessment)
                                <li>
                                  <a href="#step-{{ $row+1 }}">
                                    <span class="step_no"></span>
                                    <span class="step_descr">
                                    </span>
                                  </a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="clearfix"></div>
                        @foreach($assessments as $row => $assessment)
                            <div id="step-{{ $row+1 }}">
                                <div class="row">
                                    <div class="col-md-6" style="border-right: 1px solid">
                                        <h4 class="text-center">Statement 1</h4>
                                        <p class="text-center">{{ $assessment->statement_1 }}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <h4 class="text-center">Statement 2</h4>
                                        <p class="text-center">{{ $assessment->statement_2 }}</p>
                                    </div>
                                </div>
                                <br /><br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <input class="form-control assessment-sliders" name="assessment[{{ $assessment->id }}]" data-trait-id="{{ $assessment->id }}" style="width: 100%;" id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="{{ Auth::user()->assessement_raw_score($assessment->id) != 0 ? Auth::user()->assessement_raw_score($assessment->id) : 50 }}"/>
                                    </div>
                                </div>
                                <table class="table no-border">
                                    <tr>
                                        <td align="left" style="border-top:0">Strongly Agree</td>
                                        <td align="left" style="border-top:0">Agree</td>
                                        <td align="center" style="border-top:0">Neutral</td>
                                        <td align="right" style="border-top:0">Agree</td>
                                        <td align="right" style="border-top:0">Strongly Agree</td>
                                    </tr>
                                </table>
                            </div>
                        @endforeach
                    </div>
                    <!-- End SmartWizard Content -->
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script src="{{ asset('vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#wizard').smartWizard({
            onFinish:onFinishCallback
        });

        $('.wizard_steps').show();
        
        function onFinishCallback(objs, context){
            $('#assessment-form').submit();
        }

        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');

        $('.assessment-sliders').slider();
    });
</script>
<style type="text/css">
    .stepContainer {
        height: auto !important;
        overflow-x: visible !important;
    }
    .buttonFinish.buttonDisabled {
        display: none;
    }
    .wizard_horizontal ul.wizard_steps li a .step_no {
        width: 10px;
        height: 0
    }
    .wizard_horizontal ul.wizard_steps {
        margin: 0;
        position: absolute;
        top: -5px;
        left: 0;
        padding: 0;
    }
    .wizard_horizontal ul.wizard_steps li:first-child a:before {
        left: 0
    }
    .wizard_horizontal ul.wizard_steps li a:before {
        top: 0;
    }
    .wizard_horizontal ul.wizard_steps li:last-child a:before {
        right: 0;
        width: 100%;
    }
    .actionBar {
        border-top: 0;
    }
</style>
@stop