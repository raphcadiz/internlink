@extends('backend.layouts.master')
@section('title')
    InternLogic - Assessment Result
@stop
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-xs-12 col-md-offset-2">
            <h3 class="">Assessment Result</h3>
            <div class="">
                    <ul class="nav nav-tabs" id="tabs">
                        <li role="presentation" class="active">
                            <a href="#personality-type" aria-controls="recommendation" role="tab" data-toggle="tab">PERSONALITY TYPE</a>
                        </li>
                        <li role="presentation">
                            <a href="#your-result" aria-controls="hiring" role="tab" data-toggle="tab">YOUR RESULT</a>
                        </li>
                    </ul>
                </div>
            <div class="x_panel" style="border-top:0">
                <div class="x_content">
                    <br />
                    <div class="tab-content trait-selection-tab">
                        <div class="tab-pane active" role="tabpanel" id="personality-type">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                    <div class="profile_img text-center">
                                        <div class="crop-avatar">
                                            <img src="{{ asset('images/avatars').'/'.$personality_type->avatar }}" width="150" alt="" class="img-responsive avatar-view center-round" style="margin: 0 auto 12px">
                                        </div>
                                        <h3>{{ $personality_type->type }}</h3>
                                        <h4>{{ $personality_type->name }}</h4>
                                        <span>Share your result with your friends!</span>
                                        <br />
                                        <a href="#" style="color:#3b5998"><i class="fa fa-facebook-square fa-2"></i></a>
                                        <a href="#" style="color:#1da1f2"><i class="fa fa-twitter-square fa-2"></i></a>
                                        <a href="#" style="color:#fb7629"><i class="fa fa-rss-square fa-2"></i></a>
                                        <a href="#" style="color:#de5449"><i class="fa fa-google-plus-square fa-2"></i></a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                    <strong>Careers</strong>
                                    <div class="row">
                                    <?php 
                                    $careers = json_decode($personality_type->careers);
                                    $careers_chunk = array_chunk($careers, round(count($careers) / 2));
                                    foreach($careers_chunk as $careers){
                                        ?>
                                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                                <ul style="padding-left: 16px;">
                                                    <?php
                                                    foreach ($careers as $career) {
                                                        echo "<li>$career</li>";
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                        <?php
                                    }
                                    ?>
                                    </div>
                                </div>
                            </div>
                            <br /><br />
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                    <strong>Overview</strong>
                                    <ul style="padding-left: 16px;">
                                    <?php 
                                        $overviews = json_decode($personality_type->overview);
                                        foreach ($overviews as $overview) {
                                            echo "<li>$overview</li>";
                                        }
                                    ?>
                                    </ul>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                    <strong>Description</strong>
                                    <p style="text-align:justify">{{ $personality_type->description }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="your-result">
                            <div id="compatibility-data-container" class="text-left center-block">
                                @foreach( $user->assessment_results() as $key => $assessment_result )
                                    <a href="#{{ str_replace(' ', '-', $assessment_result->attribute_l) }}" class="attrib-toggle">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <input class="form-control assessment-sliders" name="" data-trait-id="" style="width: 100%;" id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="{{ $assessment_result->student_final_score }}"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 text-left"><strong>{{ $assessment_result->attribute_l }}</strong></div>
                                            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 text-right"><strong>{{ $assessment_result->attribute_r }}</strong></div>
                                        </div>
                                    </a>
                                    <br />
                                    <div class="row attrib-toggle-data" id="{{ str_replace(' ', '-', $assessment_result->attribute_l) }}" style="{{ $key == 0 ? '' : 'display:none'}}">
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 text-center">
                                            <img src="{{ asset('images/traits/'.$assessment_result->attribute_l_icon)}}" class="attribute-icon" />
                                            <p>{{ $assessment_result->attribute_l_description }}</p>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 text-center">
                                            <img src="{{ asset('images/traits/'.$assessment_result->attribute_r_icon)}}" class="attribute-icon" />
                                            <p>{{ $assessment_result->attribute_r_description }}</p>
                                        </div>
                                    </div>
                                    <br />
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.assessment-sliders').slider();
        $('.assessment-sliders').slider( 'disable');

        $('.attrib-toggle').on('click', function(e){
            var self = $(e.currentTarget);
            e.preventDefault();
            var target_id = self.attr('href');

            $(target_id).slideToggle();

        });
    });
</script>
@stop