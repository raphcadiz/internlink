<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <div class="clearfix"></div>
    <ul class="nav side-menu">
        <li>
            <a href="{{ url('administrator/users') }}"><i class="fa fa-fw fa-users"></i> Users</a>
        </li>
        <li class="hidden">
            <a href="{{ url('administrator/careers') }}"><i class="fa fa-fw fa-graduation-cap"></i> Careers</a>
        </li>
        <li>
            <a href="{{ url('administrator/plans') }}"><i class="fa fa-fw fa-star"></i> Plans</a>
        </li>
        <li>
            <a href="{{ url('administrator/coupons') }}"><i class="fa fa-fw fa-money"></i> Coupon</a>
        </li>
        <li>
            <a><i class="fa fa-briefcase"></i> Business <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li>
                    <a href="#"><i class="fa fa-file-text"></i> Employer Posting</a>
              </li>
              <li>
                    <a href="#"><i class="fa fa-comments"></i> Messaging Activity</a>
                </li>
            </ul>
        </li>
        <li>
            <a><i class="fa fa-book"></i> Student <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li>
                    <a href="#"><i class="fa fa-edit"></i> Edit Data<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li>
                            <a href="{{ url('administrator/edit/college') }}"><i class="fa fa-institution"></i> College</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-trophy"></i> Career Type</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap"></i> Position</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users"></i> Employer Type</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-building"></i> Industry</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-calendar"></i> Year In College</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-map-marker"></i> Location Type</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-gears"></i> Settings</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-download"></i> Export</a>
                </li>
            </ul>
        </li>
        <li>
            <a><i class="fa fa-home"></i> More <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li>
                    <a href="{{ url('/logout') }}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
  </div>
</div>
<!-- /sidebar menu -->