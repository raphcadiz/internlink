<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="publishable_key" content="{{ env('STRIPE_KEY') }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">
        <title>
            @section('title')
                Internlink
            @show
        </title>

        <!-- Bootstrap -->
        <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- NProgress -->
        <link href="{{ asset('vendors/nprogress/nprogress.css') }}" rel="stylesheet">
        <!-- iCheck -->
        <link href="{{ asset('vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
        <!-- bootstrap-progressbar -->
        {{--<link href="{{ asset('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet">--}}
        <!-- JQVMap -->
        <link href="{{ asset('vendors/jqvmap/dist/jqvmap.min.css') }}" rel="stylesheet"/>

        <!-- Custom Theme Style -->
        <link href="{{ asset('build/css/custom.min.css') }}" rel="stylesheet">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{ asset('plugins/bootstrap-slider/css/bootstrap-slider.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/select2/dist/css/select2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('css/candidates.css') }}">
        <link href="{{ asset('css/admin-style.css') }}" rel="stylesheet">
        <!-- <link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet"> -->

        @yield('styles')
    </head>
    <body class="nav-md">
        <div class="container body">
          <div class="main_container">
            
            @include('backend.layouts.sidebar')

            @include('backend.layouts.top-nav')

            <!-- page content -->
            <div class="right_col" role="main">
              @yield('content')
            </div>
            <!-- /page content -->
             @include('backend.layouts.footer')
            
          </div>
        </div>

        @include('layouts.modals.master', ['size' => 'large'])
        @include('layouts.modals.master', ['size' => 'small'])
        @include('layouts.modals.master', ['size' => 'medium'])

        <!-- jQuery -->

		<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        <script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <!-- FastClick -->
        <script src="{{ asset('vendors/fastclick/lib/fastclick.js') }}"></script>
        <!-- NProgress -->
        {{--<script src="{{ asset('vendors/nprogress/nprogress.js') }}"></script>--}}

        <script src="{{ asset('js/main.js') }}" integrity="" crossorigin="anonymous"></script>
        <script src="{{ asset('plugins/bootstrap-slider/js/bootstrap-slider.min.js') }}"></script>
        <script src="{{ asset('vendors/select2/dist/js/select2.full.min.js') }}"></script>
        <script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ asset('js/jobs.js') }}"></script>
        <script src="{{ asset('js/messenger.js') }}"></script>
        <!-- <script src="{{ asset('js/bootstrap.min.js') }}"></script> -->
        
        @yield('scripts')

        <!-- Custom Theme Scripts -->
        <script src="{{ asset('build/js/custom.min.js') }}"></script>

        
    </body>
</html>
