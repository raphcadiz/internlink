<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <div class="clearfix"></div>
    <ul class="nav side-menu">
        <li>
            <a href="{{ url('student/matches') }}"><i class="fa fa-fw fa-bar-chart"></i> Matches</a>
        </li>
        <li>
            <a href="/messages"><i class="fa fa-fw fa-weixin"></i> Messages</a>
        </li>
        <li>
            <a href="{{ url('student/results') }}"><i class="fa fa-fw fa-pie-chart"></i> Results</a>
        </li>
        <li>
            <a><i class="fa fa-reorder fa-fw"></i> More <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li>
                    <a href="{{ url('student/profile') }}"><i class="fa fa-fw fa-user"></i> My Profile</a>
                </li>
                <li>
                    <a href="{{ url('student/email-settings') }}"><i class="fa fa-fw fa-gears"></i> Email Settings</a>
                </li>
                <li>
                    <a href="{{ url('student/subscription-pricing') }}"><i class="fa fa-fw fa-star"></i> Premium Features</a>
                </li>
                <li>
                    <a href="{{ url('student/update-password') }}"><i class="fa fa-fw fa-lock"></i> Password</a>
                </li>
                <li>
                    <a href="{{ url('student/update-email') }}"><i class="fa fa-fw fa-envelope"></i> Email</a>
                </li>
                <li>
                    <a href="{{ url('student/payment-history') }}"><i class="fa fa-fw fa-dollar"></i> Payment History</a>
                </li>
                <li>
                    <a href="{{ url('student/deactivate') }}"><i class="fa fa-fw fa-close"></i> Deactivate</a>
                </li>
                <li>
                    <a href="{{ url('/logout') }}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
  </div>
</div>
<!-- /sidebar menu -->