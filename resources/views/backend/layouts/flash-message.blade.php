@if(Session::has('success'))
    <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                {{ Session::get('success') }}
    </div>
@endif

@if(Session::has('message'))
    <div class="alert alert-info fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                {{ Session::get('message') }}
    </div>
@endif

@if(Session::has('error'))
    <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                {{ Session::get('error') }}
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif