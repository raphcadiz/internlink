<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                @if(!Auth::user()->hasRole('Administrator'))
                    <li class="">
                        @if(Auth::user()->hasRole('Employer'))
                            <a href="{{ url('/employer/profile') }}" class="user-profile">
                                @if(!empty(Auth::user()->basic_information->company_logo))
                                    <img class="img-round" src="{{ asset('uploads/profile-pic/'.Auth::user()->basic_information->company_logo) }}" width="30" height="30">
                                @endif
                                {{ (!empty(Auth::user()->basic_information->company_name)) ? Auth::user()->basic_information->company_name : '&nbsp;' }}
                            </a>
                        @else
                            <a href="{{ url('/student/profile') }}" class="user-profile">
                                @if(!empty(Auth::user()->basic_information->prof_pic))
                                    <img class="img-round" src="{{ asset('uploads/profile-pic/'.Auth::user()->basic_information->prof_pic) }}" width="30" height="30">
                                @endif
                                {{ Auth::user()->getFullNameAttribute() }}
                            </a>
                        @endif
                    </li>
                @endif
                @if(Auth::user()->hasRole('Employer') && !empty(Auth::user()->basic_information->company_name))
                    <li>
                        <a class="" href="{{ url('/employer/postings/add-new') }}">New Posting <i class="fa fa-plus"></i></a>
                    </li>
                @endif
            </ul>
        </nav>
    </div>
</div>