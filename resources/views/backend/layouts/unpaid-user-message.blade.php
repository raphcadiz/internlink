<div class="modal fade" tabindex="-1" role="dialog" id="unpaid-user-warning">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Premium Feature</h4>
                </div>
                <div class="modal-body text-center">
                    <p style="font-size:18px" class="center-block"> This feature is for premium users only. </p>
                    <a href="{{ url( strtolower(Auth::user()->role->role).'/subscription-pricing') }}" class="btn btn-primary medium-btn" id="download-trigger">Upgrade Now!</a>
                </div>
            </div>
        </div>
    </div>
</div>