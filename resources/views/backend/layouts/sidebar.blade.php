<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="" class="site_title">
        @if( Auth::user()->hasRole('Employer') )
            <img class="center-block lg-logo login-logo" src="{{ asset('images/login-logo.png') }}" width="200px" style="margin-top: 5px;">
        @elseif( Auth::user()->hasRole('Student') )
           <img class="center-block lg-logo login-logo" src="{{ asset('images/login-logo-1.png') }}" width="220px" style="margin-top: 5px;">
        @elseif( Auth::user()->hasRole('Administrator') )
             <img src="{{ asset('images/icon.png') }}" id="head-logo" /> <span>InternLogic</span>
        @endif

        <img src="{{ asset('images/icon.png') }}" id="mobile-logo" style="width: 34px;margin: 0 0 0 9px;" />
      </a>
    </div>

    <div class="clearfix"></div>
    @if( Auth::user()->hasRole('Employer') )
        @include('backend.layouts.employer-sidebar')
    @elseif( Auth::user()->hasRole('Student') )
        @include('backend.layouts.student-sidebar')
    @elseif( Auth::user()->hasRole('Administrator') )
        @include('backend.layouts.admin-sidebar')
    @endif

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Logout">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>