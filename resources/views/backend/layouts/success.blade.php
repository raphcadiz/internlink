@extends('backend.layouts.master')

@section('title')
    InternLogic - Success
@stop

@section('styles')

@stop

@section('content')
<div class="container-fluid">
    <br />
    <br />
    <div class="row">
        <div class="col-md-6 col-xs-12 col-md-offset-3">
            @if(isset($data['title']))
                <h3>{{ $data['title'] }}</h3>
            @endif
            <div class="x_panel">
                <div class="x_content text-center">
                    @if(isset($data['message']))
                        <h3 class="">{{ $data['message'] }}</h3>
                    @endif

                    @if(isset($data['sub_message']))
                        <p style="font-size:18px">{{ $data['sub_message'] }}</p>
                    @endif

                    <span class="center-block"><i class="success-check fa fa-check-circle"></i></span>

                    @if(isset($data['redirect_url']))
                        <a href="{{ $data['redirect_url'] }}">Done</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@stop

@section('scripts')

@stop