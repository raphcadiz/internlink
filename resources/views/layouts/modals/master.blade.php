@if(isset($size))
    @if($size == 'large')
        {{--*/ $size_id = 'large' /*--}}
        {{--*/ $size_class = 'lg' /*--}}
    @elseif($size == 'small')
        {{--*/ $size_id = 'small' /*--}}
        {{--*/ $size_class = 'sm' /*--}}
    @else
        {{--*/ $size_id = 'medium' /*--}}
        {{--*/ $size_class = 'md' /*--}}
    @endif
@else
    {{--*/ $size_id = 'medium' /*--}}
    {{--*/ $size_class = 'md' /*--}}
@endif


<div class="modal fade" id="bs-modal-{{ $size_id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-{{ $size_class }}" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>
<script type="text/javascript">
    var defer_until_jquery_loaded_{{ $size_id }} = setInterval(function() {
        if (window.jQuery !== undefined) {
            (function($) {
                $(function() {
                    $('#bs-modal-{{ $size_id }}').on('hidden.bs.modal', function () {
                        $(this).removeData('bs.modal');
                    });
                });
            })(jQuery);
            clearInterval(defer_until_jquery_loaded_{{ $size_id }});
        }
    }, 100);
</script>