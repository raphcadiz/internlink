<div class="modal-header">
    @if(!isset($btn_close_show) ||  $btn_close_show !== false)
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    @endif
    <h4 class="modal-title" id="myModalLabel">@yield('title', trans('modal.no-title'))</h4>
</div>
<div class="modal-body">
    @yield('content')
</div>