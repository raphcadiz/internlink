<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Job Application</title>
</head>
<body>
    <h1>New Job Application!</h1>

    <p>{{ $user->getFullNameAttribute() }} applied to you job posting {{ $job->title }}. View <a href="{{ url('employer/applicants') }}">here</a></p>

</body>
</html>