<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up Confirmation</title>
</head>
<body>
    <h1>You are added as "{{ $user->role->role }}" by the Administrator!</h1>
    <p>
        We just need you to confirm it real quick! Click <a href='{{ url("register/confirm/{$user->token}") }}'>Here</a>
    </p>
    <p>
    	Here is your login information:<br />
    	email: {{ $user->email }}
    	password: {{ $password }}
    </p>
</body>
</html>