<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Job Invitation</title>
</head>
<body>
    <p>Hi {{ $user->first_name }},</p>
    <p>{{ $job->user->first_name.' '.$job->user->last_name }} has invited you to apply on his {{ $job->title }} position.</p>
</body>
</html>