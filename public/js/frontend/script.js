jQuery(document).ready(function(){
	//Floating Menu
	jQuery(document).scroll(function() {
	  var y = $(this).scrollTop();
	  if (y > 90) {
		jQuery('.floating-header').fadeIn();
	  } else {
		jQuery('.floating-header').fadeOut();
	  }
	});
		
	
	// MOBILE MENU
	jQuery('.mobile-menu-container').mmenu({
		classes: 'mm-light',
		counters: true,
		offCanvas: {
			position  : 'left',
			zposition :	'front',
		}
	});

	jQuery('a.mobile-menu-trigger').click(function() {
		jQuery('.mobile-menu-container').trigger('open.mm');
	});
	
	jQuery(window).resize(function() {
		jQuery('.mobile-menu-container').trigger("close.mm");
	});


	  jQuery('#play-video').on('click', function(ev) {
	   if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
	        || location.hostname == this.hostname) {

	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	           if (target.length) {
	             $('html,body').animate({
	                 scrollTop: target.offset().top
	            }, 1000);
	             $src = jQuery(".laptop-img>iframe")[0].src;
	             if($src.indexOf("autoplay") >= 0){
	             	jQuery(".laptop-img>iframe").attr("src",$src);
	             }else{
	             	jQuery(".laptop-img>iframe")[0].src += "?rel=0&autoplay=1";
	             }     
	            return false;
	        }
	    }
	    ev.preventDefault();
	  });

});