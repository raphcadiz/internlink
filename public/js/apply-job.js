(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

(function () {
    window.ApplyJob = {
        init: function init() {
            this.bindEvents();
        },

        bindEvents: function bindEvents() {
            $('.save-job').on('click', $.proxy(this.saveJob, this));
            $('.apply-job').on('click', $.proxy(this.applyJob, this));
            $('.apply-with-confirm').on('click', $.proxy(this.applyJobConfirm, this));
            $('#dont-confirm-again').on('click', $.proxy(this.dontConfirmAgain, this));
            $('#view-compatibility-free-user').on('click', $.proxy(this.getFreeUserWarning, this));
            $('#view-compatibility').on('click', $.proxy(this.showCompatibilityData, this));
        },

        saveJob: function saveJob(e) {
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type: 'POST',
                url: base_url + '/student/save-job/' + id,
                data: '_token=' + _token
            }).done(function (data) {
                if (data == 'max') {
                    $('#maxxed-saved-jobs').modal('show');
                } else if (data == 'saved') {
                    self.children('i').addClass('saved-job');
                } else if (data == 'deleted') {
                    self.children('i').removeClass('saved-job');
                } else {
                    console.log('Something went wrong. Please try again later.');
                }
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        applyJob: function applyJob(e) {
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            self.prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: base_url + '/student/apply-job/' + id,
                data: '_token=' + _token
            }).done(function (data) {
                if (data == 'success') {
                    $('#apply-with-confirm').modal('hide');
                    $('.apply-job').addClass('saved-job');
                    $('.apply-with-confirm').addClass('saved-job');
                    $('#application-submitted').modal('show');
                } else {
                    console.log('Something went wrong. Please try again later.');
                }
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        applyJobConfirm: function applyJobConfirm(e) {
            $('#apply-with-confirm').modal('show');
        },

        dontConfirmAgain: function dontConfirmAgain(e) {
            $.ajax({
                type: 'POST',
                url: base_url + '/student/dont-confirm-application',
                data: '_token=' + _token
            }).done(function (data) {}).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        getFreeUserWarning: function getFreeUserWarning(e) {
            $.ajax({
                type: 'GET',
                url: base_url + '/student/get-free-user-warning'
            }).done(function (data) {
                if (!$('#unpaid-user-warning').length) {
                    $("body").append(data);
                }
                $('#unpaid-user-warning').modal('show');
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        showCompatibilityData: function showCompatibilityData(e) {
            $('#compatibility-data-container').slideToggle();
        }

        //end
    };

    ApplyJob.init();
})();

},{}]},{},[1]);

//# sourceMappingURL=apply-job.js.map
