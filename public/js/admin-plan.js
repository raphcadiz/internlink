(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

(function () {
    window.PlanManagement = {
        init: function init() {
            this.bindEvents();
        },

        bindEvents: function bindEvents() {
            $('#add-new-plan').on('click', $.proxy(this.addPlanModal, this));
            $('.delete-plan').on('click', $.proxy(this.deletePlanModal, this));
            $('.update-plan').on('click', $.proxy(this.updatePlanModal, this));
        },

        addPlanModal: function addPlanModal(e) {
            $.ajax({
                type: 'GET',
                url: base_url + '/administrator/plans/add-new'
            }).done(function (data) {
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        deletePlanModal: function deletePlanModal(e) {
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type: 'GET',
                url: base_url + '/administrator/plans/delete-plan/' + id
            }).done(function (data) {
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        updatePlanModal: function updatePlanModal(e) {
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type: 'GET',
                url: base_url + '/administrator/plans/update-plan/' + id
            }).done(function (data) {
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        }
    };

    PlanManagement.init();
})();

},{}]},{},[1]);

//# sourceMappingURL=admin-plan.js.map
