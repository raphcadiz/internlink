(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

(function () {
    window.CollegeManagement = {
        init: function init() {
            this.bindEvents();
        },

        bindEvents: function bindEvents() {
            $('#add-new-college').on('click', $.proxy(this.addCollegeModal, this));
            $('.delete-college').on('click', $.proxy(this.deleteCollegeModal, this));
            $('.update-college').on('click', $.proxy(this.updateCollegeModal, this));
        },

        addCollegeModal: function addCollegeModal(e) {
            $.ajax({
                type: 'GET',
                url: base_url + '/administrator/edit/college/add-new'
            }).done(function (data) {
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        deleteCollegeModal: function deleteCollegeModal(e) {
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type: 'GET',
                url: base_url + '/administrator/edit/college/delete-college/' + id
            }).done(function (data) {
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        updateCollegeModal: function updateCollegeModal(e) {
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type: 'GET',
                url: base_url + '/administrator/edit/college/update-college/' + id
            }).done(function (data) {
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        }
    };

    CollegeManagement.init();
})();

},{}]},{},[1]);

//# sourceMappingURL=admin-college.js.map
