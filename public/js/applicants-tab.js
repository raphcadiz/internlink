(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

(function () {
    window.Applicant = {
        init: function init() {
            this.bindEvents();
        },

        bindEvents: function bindEvents() {
            $('#view-compatibility-free-user').on('click', $.proxy(this.getFreeUserWarning, this));
            $('#view-compatibility').on('click', $.proxy(this.showCompatibilityData, this));
        },

        getFreeUserWarning: function getFreeUserWarning(e) {
            $.ajax({
                type: 'GET',
                url: base_url + '/employer/get-free-user-warning'
            }).done(function (data) {
                if (!$('#unpaid-user-warning').length) {
                    $("body").append(data);
                }
                $('#unpaid-user-warning').modal('show');
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        showCompatibilityData: function showCompatibilityData(e) {
            $('#compatibility-data-container').slideToggle();
        }
    };

    Applicant.init();
})();

},{}]},{},[1]);

//# sourceMappingURL=applicants-tab.js.map
