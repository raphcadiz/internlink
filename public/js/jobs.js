(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

(function ($) {
    $(function () {
        $('select[name=status]').select2({
            containerCssClass: 'select2-form-control',
            width: 100
        }).on('change', function (e) {
            var job_id = $(this).attr('data-job_id');
            var status_id = $(this).val();
            var current_status = $('input[name=current_status]').val();
            var url = $('.changePostingStatus').attr('href');

            if (status_id != current_status) {
                $('.changePostingStatus').trigger('click');
            }
        });

        $('#bs-modal-medium').on('shown.bs.modal', function () {
            $('#changeJobStatusForm').on('submit', function (e) {
                alert();
                e.preventDefault();
                var $this = $(this),
                    $token = $this.find('input[name="_token"]').val(),
                    $url = $this.attr('action');

                var $def = $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $token
                    },
                    url: $url,
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    beforeSend: function beforeSend() {
                        $this.find(".btn").prop("disabled", true);
                    }
                });

                $this.find("[data-dismiss='modal']").one("click", function () {
                    $def.abort();
                });

                $def.done(function (res) {
                    $('.form-content').hide();
                    $('.form-success').find('strong.message').html(res.message);
                    $('.form-success').removeClass('hide');

                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                });

                return false;
            });
        });
    });
})(jQuery);

},{}]},{},[1]);

//# sourceMappingURL=jobs.js.map
