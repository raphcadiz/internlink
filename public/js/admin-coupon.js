(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

(function () {
    window.Coupon = {
        init: function init() {
            this.bindEvents();
        },

        bindEvents: function bindEvents() {
            $('#add-new-coupon').on('click', $.proxy(this.createCoupon, this));
        },

        createCoupon: function createCoupon(e) {
            $.ajax({
                type: 'GET',
                url: base_url + '/administrator/coupons/add-new'
            }).done(function (data) {
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        }
    };

    Coupon.init();
})();

},{}]},{},[1]);

//# sourceMappingURL=admin-coupon.js.map
