(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

(function ($) {
    $(function () {
        $(document).on('shown.bs.modal', '#bs-modal-medium', function () {
            $(document).on('submit', '#inviteCandidateForm', function (e) {
                e.preventDefault();
                var $this = $(this),
                    $token = $this.find('input[name="_token"]').val(),
                    $url = $this.attr('action'),
                    $form_content = $('.form-content'),
                    $form_success = $('.form-success'),
                    $modal = $('#bs-modal-medium');

                var $def = $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $token
                    },
                    url: $url,
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    beforeSend: function beforeSend() {
                        $this.find(".btn").prop("disabled", true);
                    }
                });

                $this.find("[data-dismiss='modal']").one("click", function () {
                    $def.abort();
                });

                $def.done(function (res) {
                    $form_content.hide();
                    $form_success.find('strong.message').html(res.message);
                    $form_success.removeClass('hide');

                    $form_success.delay(500).fadeOut('slow', function () {
                        $modal.modal('hide');
                        // $('.modal-backdrop').removeClass('in').addClass('out');
                        $('.modal-backdrop').remove();
                        load_candidates();
                    });
                });

                return false;
            });
        });

        $(document).on('click', '.go-to-applicant', function (e) {
            var self = $(e.currentTarget);
            var job_id = self.attr('data-job-id');
            var user_id = self.attr('data-user-id');

            window.location.href = base_url + '/employer/show-applicant/' + job_id + '/' + user_id;
        });

        $(document).on('click', '.go-to-candidate', function (e) {
            var self = $(e.currentTarget);
            var job_id = self.attr('data-job-id');
            var user_id = self.attr('data-user-id');

            window.location.href = base_url + '/employer/show-candidate/' + job_id + '/' + user_id;
        });

        $('select').select2();

        $('.filter-slider').slider({
            reversed: true
        });

        load_applicants();

        $(document).on('click', '.nav li', function () {
            var $active_tab = $(this).find('a').attr('href');
            if ($active_tab == '#applicants') {
                load_applicants();
            }

            if ($active_tab == '#candidates') {
                load_candidates();
            }
        });

        $('form#filter-applicants').on('submit', function (e) {
            e.preventDefault();

            var $active_tab = $('.nav-tabs').find('li.active').find('a');

            if ($active_tab.attr('href') == '#applicants') {
                load_applicants();
            }

            if ($active_tab.attr('href') == '#candidates') {
                load_candidates();
            }
        });

        function load_applicants() {
            var $this = $('form#filter-applicants'),
                $loader = $('#applicants').find('.loading-applicants'),
                $result_table = $('#applicants').find('.applicants-result-table');

            var $def = $.ajax({
                url: base_url + '/employer/filter-applicants',
                type: 'POST',
                data: $this.serialize(),
                beforeSend: function beforeSend() {
                    $loader.show();
                    $result_table.html('');
                }

            });

            $def.done(function (view) {
                $loader.delay(500).fadeOut('slow', function () {
                    $result_table.html(view);
                });
            });
        }

        function load_candidates() {
            var $this = $('form#filter-applicants'),
                $url = base_url + '/employer/filter-candidates',
                $loader = $('#candidates').find('.loading-candidates'),
                $result_table = $('#candidates').find('.candidates-result-table');

            var $def = $.ajax({
                url: $url,
                type: 'POST',
                data: $this.serialize(),
                beforeSend: function beforeSend() {
                    $loader.show();
                    $result_table.html('');
                }

            });

            $def.done(function (view) {
                $loader.delay(500).fadeOut('slow', function () {
                    $result_table.html(view);
                });
            });
        }
    });
})(jQuery);

},{}]},{},[1]);

//# sourceMappingURL=applicants.js.map
