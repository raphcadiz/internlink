$(document).ready(function(){

    checkNewMessages();

    function checkNewMessages()
    {
        var $base_url = window.location.origin;
        var $current_url = window.location.href;

        if ($current_url.indexOf($base_url+"/messages") > -1) {

            var $segments = $current_url.split('/');

            var $thread_id = ($segments.length == 5) ? $segments[4] : null;
            var $keyword = $('input[name=keyword]').val();
            var $current_thread_messages_count = $('input[name=current_thread_messages_count]').val();

            $.ajax({
                url: '/check-messages',
                type: 'GET',
                data: {
                    thread_id : $thread_id,
                    keyword : $keyword,
                    current_thread_messages_count : $current_thread_messages_count,
                    current_url : $current_url
                },
                dataType: 'json',
                success: function (res) {
                    $('div.conversation-list').html(res.conversation_list);
                    if(res.refresh_view) {
                        $('div.conversation-content').html(res.conversation_content);
                    }

                    triggerMessagesJs();
                    setTimeout(function() {
                        checkNewMessages();
                    }, 5000);
                }
            });
        }
    }

    function triggerMessagesJs()
    {
        $(document).on('click', '.conversation-item', function () {
            var $this = $(this),
                $thread_id = $this.attr('data-thread-id');

            window.location.href = '/messages/'+$thread_id
        });

        $(document).on('keyup', 'textarea[name=message]', function() {
            var $this = $(this),
                $submit = $this.closest('form').find('button');

            if ($this.val().trim() != '') {
                $submit.removeClass('disabled').prop('disabled', false);
            } else {
                $submit.addClass('disabled').prop('disabled', true);
            }
        });

        var elem = document.getElementById('conversation_messages');
        if( elem !== null) {
            elem.scrollTop = elem.scrollHeight;
        }
    }
});