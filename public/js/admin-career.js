(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

(function () {
    window.CareerModal = {
        init: function init() {
            this.bindEvents();
        },

        bindEvents: function bindEvents() {
            $('#add-new-career').on('click', $.proxy(this.addCareerModal, this));
            $('.delete-career').on('click', $.proxy(this.deleteCareerModal, this));
            $('.update-career').on('click', $.proxy(this.updateCareerModal, this));
        },

        addCareerModal: function addCareerModal(e) {
            $.ajax({
                type: 'GET',
                url: base_url + '/administrator/edit/career/add-new'
            }).done(function (data) {
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        deleteCareerModal: function deleteCareerModal(e) {
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type: 'GET',
                url: base_url + '/administrator/edit/career/delete-career/' + id
            }).done(function (data) {
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        updateCareerModal: function updateCareerModal(e) {
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            $.ajax({
                type: 'GET',
                url: base_url + '/administrator/edit/career/update-career/' + id
            }).done(function (data) {
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function (xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        }
    };

    CareerModal.init();
})();

},{}]},{},[1]);

//# sourceMappingURL=admin-career.js.map
