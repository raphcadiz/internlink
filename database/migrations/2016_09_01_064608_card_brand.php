<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CardBrand extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_payments', function (Blueprint $table) {
            $table->string('card_brand')->after('stripe_charge_token');
            $table->integer('card_last_four')->after('card_brand');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_payments', function (Blueprint $table) {
            $table->dropColumn('card_brand');
            $table->dropColumn('card_last_four');
        });
    }
}
