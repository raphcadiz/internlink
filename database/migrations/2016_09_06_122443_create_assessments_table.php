<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('statement_1');
            $table->text('statement_2');
            $table->string('attribute_l');
            $table->string('attribute_r');
            $table->float('mean', 8 ,5);
            $table->float('stdev', 8 ,5);
            $table->string('myersbriggs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assessments');
    }
}
