<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateTraitSelectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_trait_selections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id')->unsigned()->index;
            $table->integer('trait_id')->unsigned()->index;
            $table->integer('value')->nullable();
            $table->foreign('job_id')->references('id')->on('jobs');
            $table->foreign('trait_id')->references('id')->on('candidate_traits');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidate_trait_selections');
    }
}
