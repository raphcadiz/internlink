<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateJobPositionEnumValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function(Blueprint $table) {
            $table->dropColumn('queue_position');
        });

        Schema::table('jobs', function (Blueprint $table) {
            $table->enum('queue_position', ['new posting', 'candidate selection', 'pricing', 'payment', 'payment confirmation', 'completed']);
        });

        \Illuminate\Support\Facades\DB::table('jobs')->update(['queue_position' => 'completed']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->enum('queue_position', ['post description', 'trait selection', 'pricing', 'payment', 'completed']);
        });
    }
}
