<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeIndustryTypeToIdOnEmployerInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employer_informations', function (Blueprint $table) {
            $table->dropColumn('industry_type');
            $table->integer('industry_id')->after('company_name')->unsigned()->nullable();

            $table->foreign('industry_id')->references('id')->on('industries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_informations', function (Blueprint $table) {
            $table->string('industry_type')->after('company_name');
            $table->dropColumn('industry_id');
        });
    }
}
