<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status_name');
            $table->timestamps();
        });

        Schema::table('job_applicants', function(Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('job_applicants', function (Blueprint $table) {
            $table->integer('status_id')->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('applicant_statuses');

        Schema::table('job_applicants', function(Blueprint $table) {
            $table->dropColumn('status_id');
        });

        Schema::table('job_applicants', function (Blueprint $table) {
            $table->enum('status', ['phone screened', 'interviewed']);
        });

        \Illuminate\Support\Facades\DB::table('job_applicants')->update(['status' => 'phone screened']);
    }
}
