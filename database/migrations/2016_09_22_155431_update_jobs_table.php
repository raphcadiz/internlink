<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function(Blueprint $table) {
            $table->decimal('gpa', 5,2);
            $table->enum('year', ['graduated', 'senior', 'junior', 'sophomore', 'freshman']);
            $table->boolean('virtual');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function(Blueprint $table) {
            $table->dropColumn('gpa');
            $table->dropColumn('year');
            $table->dropColumn('virtual');
        });
    }
}
