<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_candidates', function (Blueprint $table) {
            $table->dropColumn('invited');
        });

        Schema::table('candidate_trait_selections', function (Blueprint $table) {
            $table->renameColumn('value', 'employer_standard_score');
        });

        Schema::table('candidate_trait_selections', function (Blueprint $table) {
            $table->decimal('employer_standard_score', 8, 5)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
