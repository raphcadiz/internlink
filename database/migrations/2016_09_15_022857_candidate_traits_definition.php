<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CandidateTraitsDefinition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessments', function (Blueprint $table) {
            $table->text('attribute_l_description')->after('attribute_r')->nullable();
            $table->text('attribute_r_description')->after('attribute_l_description')->nullable();
            $table->string('attribute_l_icon')->after('attribute_r_description')->nullable();
            $table->string('attribute_r_icon')->after('attribute_l_icon')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessments', function (Blueprint $table) {
            $table->dropColumn('attribute_l_description');
            $table->dropColumn('attribute_r_description');
            $table->dropColumn('attribute_l_icon');
            $table->dropColumn('attribute_r_icon');
        });
    }
}
