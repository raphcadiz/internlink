<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentCoupons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coupon_code');
            $table->double('amount_off', 5, 2);
            $table->double('percent_off', 5, 2);
            $table->string('currency');
            $table->enum('duration', ['once', 'multi-month', 'forever']);
            $table->integer('duration_in_months');
            $table->integer('max_redemptions');
            $table->dateTime('redeem_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_coupons');
    }
}
