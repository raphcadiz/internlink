<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentStandardScore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_assessments', function (Blueprint $table) {
            $table->float('student_standard_score', 8 ,5);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_assessments', function (Blueprint $table) {
            $table->dropColumn('student_standard_score');
        });
    }
}
