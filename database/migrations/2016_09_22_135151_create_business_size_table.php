<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('size_name');
            $table->timestamps();
        });

        Schema::table('employer_informations', function (Blueprint $table) {
            $table->integer('business_size_id')->after('industry_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_informations', function (Blueprint $table) {
           $table->dropColumn('business_size_id');
        });

        Schema::drop('business_sizes');
    }
}
