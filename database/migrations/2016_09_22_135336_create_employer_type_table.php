<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type_name');
            $table->timestamps();
        });

        Schema::table('employer_informations', function (Blueprint $table) {
            $table->integer('employer_type_id')->after('industry_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employer_informations', function (Blueprint $table) {
            $table->dropColumn('employer_type_id');
        });

        Schema::drop('employer_types');
    }
}
