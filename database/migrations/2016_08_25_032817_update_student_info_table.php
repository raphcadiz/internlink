<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStudentInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_informations', function (Blueprint $table) {
            $table->string('address')->nullable();
            $table->string('city');
            $table->string('state');
            $table->string('zip_code');
            $table->integer('phone_number');
            $table->integer('college_id')->nullable()->unsigned();
            $table->integer('year')->nullable();
            $table->string('gpa')->nullable();
            $table->text('professional_info')->nullable();
            $table->text('about')->nullable();
            $table->text('experience')->nullable();
            $table->text('education')->nullable();
            $table->text('skills')->nullable();
            $table->string('resume')->nullable();

            $table->foreign('college_id')->references('id')->on('colleges');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_informations', function (Blueprint $table) {
            //
        });
    }
}
