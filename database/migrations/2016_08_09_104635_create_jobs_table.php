<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->text('company_description')->nullable();
            $table->text('responsibilities')->nullable();
            $table->text('requirements')->nullable();
            $table->text('skills')->nullable();
            $table->enum('employment_type', ['Full-Time', 'Part-Time']);
            $table->date('job_start')->nullable();
            $table->date('job_end')->nullable();
            $table->enum('job_type', ['Internship', 'Job']);
            $table->enum('paid', ['Paid', 'Unpaid']);
            $table->string('salary');
            $table->enum('rate', ['Per Hour', 'Per Month', 'Per Year']);
            $table->string('job_url')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobs');
    }
}
