<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PersonalityTypeAvatar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personality_types', function (Blueprint $table) {
            $table->string('avatar')->after('careers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personality_types', function (Blueprint $table) {
            $table->dropColumn('avatar');
        });
    }
}
