<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateYearColumnOnStudentInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_informations', function (Blueprint $table) {
            $table->dropColumn('year');
        });

        Schema::table('student_informations', function (Blueprint $table) {
            $table->enum('year', ['graduated', 'senior', 'junior', 'sophomore', 'freshman', 'other'])->after('major_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('student_informations', function (Blueprint $table) {
            $table->dropColumn('year');
        });

        Schema::table('student_informations', function (Blueprint $table) {
            $table->integer('year')->nullable()->after('major_id');
        });
    }
}
