<?php

use Illuminate\Database\Seeder;

class EmployerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employer_types')->insert(array(
                array(
                    'type_name'          => 'Profit',
                ),
                array(
                    'type_name'          => 'Non-Profit',
                ),
                array(
                    'type_name'          => 'Government',
                )
            )
        );
    }
}
