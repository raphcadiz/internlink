<?php

use Illuminate\Database\Seeder;

class IndustriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('industries')->insert(array(
                array(
                    'industry'          => 'Arts and Humanities',
                ),
        		array(
                    'industry'          => 'Business and Management',
                ),
        		array(
                    'industry'          => 'Communications',
                ),
                array(
                    'industry'          => 'Education and Training',
                ),
                array(
                    'industry'          => 'Engineering and Technology',
                ),
                array(
                    'industry'          => 'Health Services',
                ),
                array(
                    'industry'          => 'Law and Public Safety',
                ),
                array(
                    'industry'          => 'Manufacturing and Production',
                ),
                array(
                    'industry'          => 'Mathematics and Science',
                ),
                array(
                    'industry'          => 'Transportation',
                ),
                array(
                    'industry'          => 'Miscellaneous',
                ),
        	)
        );
    }
}
