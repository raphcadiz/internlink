<?php

use Illuminate\Database\Seeder;
use App\Models\Industry;

class CareersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$arts_humanities 		= Industry::where('industry', 'Arts and Humanities')->first();
    	$business_management 	= Industry::where('industry', 'Business and Management')->first();
    	$communications 		= Industry::where('industry', 'Communications')->first();
    	$idu_training	 		= Industry::where('industry', 'Education and Training')->first();
    	$engineering_tec	 	= Industry::where('industry', 'Engineering and Technology')->first();
    	$health_services	 	= Industry::where('industry', 'Health Services')->first();
    	$law_pub_safety		 	= Industry::where('industry', 'Law and Public Safety')->first();
    	$manufuc_prod		 	= Industry::where('industry', 'Manufacturing and Production')->first();
    	$math_science		 	= Industry::where('industry', 'Mathematics and Science')->first();
    	$transportation		 	= Industry::where('industry', 'Transportation')->first();
    	$miscellaneous		 	= Industry::where('industry', 'Miscellaneous')->first();

        DB::table('careers')->insert(array(
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Interior Decorator',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Designer',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Photographer',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Actor',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Writer',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Technical Writer',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Musicians and Artists',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Photographers',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Artist',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Performer or Actor',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Social Work',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Fashion Designer',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Writer / Journalist',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Musician',
                ),
                array(
                	'industry_id'	=> $arts_humanities->id,
                    'title'         => 'Musicians',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Business Executive',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Administrator',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Manager',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Accountant',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Financial Officer',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Organization Builder',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Entrepreneur',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Business Administrator',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Business Manager',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Sales Representative',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Office Manager',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Administrative Assistant',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Shopkeeper',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Bookkeeper',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Consultant',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Sales Rep',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Marketing Rep',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Corporate Strategist',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Strategic Planner',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Human Resources',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Sales Representatives',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Marketing Personnel',
                ),
                array(
                	'industry_id'	=> $business_management->id,
                    'title'         => 'Entrepreneurs',
                ),
                array(
                	'industry_id'	=> $communications->id,
                    'title'         => 'Facilitator',
                ),
                array(
                	'industry_id'	=> $communications->id,
                    'title'         => 'Events Coordinator',
                ),
                array(
                	'industry_id'	=> $communications->id,
                    'title'         => 'Television Reporter',
                ),
                array(
                	'industry_id'	=> $idu_training->id,
                    'title'         => 'University Professor',
                ),
          		array(
                	'industry_id'	=> $idu_training->id,
                    'title'         => 'Teacher',
                ),
          		array(
                	'industry_id'	=> $idu_training->id,
                    'title'         => 'Coach',
                ),
          		array(
                	'industry_id'	=> $idu_training->id,
                    'title'         => 'Counselor',
                ),
          		array(
                	'industry_id'	=> $idu_training->id,
                    'title'         => 'Professor',
                ),
          		array(
                	'industry_id'	=> $idu_training->id,
                    'title'         => 'Teachers',
                ),
          		array(
                	'industry_id'	=> $idu_training->id,
                    'title'         => 'Teachers / Professors',
                ),
                array(
                	'industry_id'	=> $engineering_tec->id,
                    'title'         => '',
                ),
                array(
                	'industry_id'	=> $engineering_tec->id,
                    'title'         => 'Computer Programmer',
                ),
    			array(
                	'industry_id'	=> $engineering_tec->id,
                    'title'         => 'Systems Analyst',
                ),
    			array(
                	'industry_id'	=> $engineering_tec->id,
                    'title'         => 'Computer Consultant',
                ),
    			array(
                	'industry_id'	=> $engineering_tec->id,
                    'title'         => 'Engineer',
                ),
    			array(
                	'industry_id'	=> $engineering_tec->id,
                    'title'         => 'Mechanic',
                ),
    			array(
                	'industry_id'	=> $engineering_tec->id,
                    'title'         => 'PC Technicians or Network Cablers',
                ),
    			array(
                	'industry_id'	=> $engineering_tec->id,
                    'title'         => 'Computer Technical Support',
                ),
    			array(
                	'industry_id'	=> $engineering_tec->id,
                    'title'         => 'Computer Programmer / Systems Analyst',
                ),
                array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Medical Doctor',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Dentist',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Nurse',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Child Care Provider',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Social Worker',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Psychologist',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Child Care',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Physician',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Paramedic / Emergency Medical Technician',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Medical Doctors / Dentists',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Alternative Health Care Practitioners, i.e. Chiropractor, Reflexologist',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Psychologists',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Psychiatrists',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Counselors / Social Workers',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Child Care / Early Childhood Development',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Veterinarian',
                ),
    			array(
                	'industry_id'	=> $health_services->id,
                    'title'         => 'Pediatrician',
                ),
                array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Police Officer',
                ),
    			array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Detective',
                ),
    			array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Judge',
                ),
    			array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Attorney',
                ),
    			array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Military Leader',
                ),
    			array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Lawyer',
                ),
    			array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Firefighter',
                ),
    			array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Paralegal',
                ),
    			array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Forest Ranger',
                ),
    			array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Forensic Research',
                ),
    			array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Park Ranger',
                ),
    			array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Forensic Pathologist',
                ),
    			array(
                	'industry_id'	=> $law_pub_safety->id,
                    'title'         => 'Police / Detective Work',
                ),
                array(
                	'industry_id'	=> $manufuc_prod->id,
                    'title'         => 'Construction',
                ),
    			array(
                	'industry_id'	=> $manufuc_prod->id,
                    'title'         => 'Farmer',
                ),
                array(
                	'industry_id'	=> $math_science->id,
                    'title'         => 'Scientist',
                ),
    			array(
                	'industry_id'	=> $math_science->id,
                    'title'         => 'Physicist',
                ),
    			array(
                	'industry_id'	=> $math_science->id,
                    'title'         => 'Chemist',
                ),
    			array(
                	'industry_id'	=> $math_science->id,
                    'title'         => 'Mathematician',
                ),
                array(
                	'industry_id'	=> $transportation->id,
                    'title'         => 'Pilot',
                ),
                array(
                	'industry_id'	=> $miscellaneous->id,
                    'title'         => 'Athlete',
                ),
    			array(
                	'industry_id'	=> $miscellaneous->id,
                    'title'         => 'Pastor or Priest',
                ),
    			array(
                	'industry_id'	=> $miscellaneous->id,
                    'title'         => 'Church Employee',
                ),
    			array(
                	'industry_id'	=> $miscellaneous->id,
                    'title'         => 'Clergy',
                ),
    			array(
                	'industry_id'	=> $miscellaneous->id,
                    'title'         => 'Politician',
                ),
    			array(
                	'industry_id'	=> $miscellaneous->id,
                    'title'         => 'Diplomat',
                ),
    			array(
                	'industry_id'	=> $miscellaneous->id,
                    'title'         => 'Clergy / Religious Work',
                ),
    			array(
                	'industry_id'	=> $miscellaneous->id,
                    'title'         => 'Politician / Diplomat',
                ),
    			array(
                	'industry_id'	=> $miscellaneous->id,
                    'title'         => 'Clergy / Religious Workers',
                ),
        	)
        );
    }
}
