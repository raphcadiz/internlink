<?php

use Illuminate\Database\Seeder;

class PlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert(array(
                array(
                    'name'	=> 'premium',
                    'alias'	=> 'Premium Internlogic Subscription',
                    'price'	=> '4.99'
                ),
                array(
                    'name'  => 'starter',
                    'alias' => 'Starter Internlogic Subscription',
                    'price' => '49.99'
                ),
                array(
                    'name'  => 'plus',
                    'alias' => 'Plus Internlogic Subscription',
                    'price' => '99.99'
                ),
                array(
                    'name'  => 'pro',
                    'alias' => 'Pro Internlogic Subscription',
                    'price' => '149.99'
                ),
        	)
        );
    }
}
