<?php

use Illuminate\Database\Seeder;
use App\Models\Assessment;

class AssessmentsDescriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $assessments = Assessment::all();

        foreach($assessments as $assessment) {
            if( $assessment->attribute_l == 'Detail-minded' ){
            	$assessment->attribute_l_description = 'Once committed to complete a task or project by a specific date, does whatever it takes to meet that obligation.  Has an eye for detail and takes pleasure in checking work to ensure no mistakes have been made. Most comfortable and effective working within the rules or guidelines to get things done.  Has a knack for looking at a project plan, a set of procedures, or a process and finding inconsistencies or conflicts that must be corrected.';
            	$assessment->attribute_r_description = 'Ensures that quality work is achieved even if it means deadlines are not met. Quickly tires when forced to attend to each detail; uses more energy checking than creating.  Best working with someone who can carefully scrutinize the details of the more conceptual work.  Acts as if rules are made to be broken; believes it is more important to get meaningful work done than to follow the rules.';
            	$assessment->attribute_l_icon = 'trait-detail-minded.png';
            	$assessment->attribute_r_icon = 'trait-big-picture.png';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Practical-minded' ){
            	$assessment->attribute_l_description = 'To him/her, fixing problems quickly is more important than finding the most elegant solution. Impatient with talking about a fix - jumps in and starts doing something. Believes best ideas are simple and easy to implement.  More concerned with getting results than with methods used.';
            	$assessment->attribute_r_description = 'Comfortable with intangible processes. Enjoys wrestling with abstract notions. Tends to read between the lines and extrapolate. Prone to interpret and move beyond data quickly.';
            	$assessment->attribute_l_icon = 'trait-practical.svg';
            	$assessment->attribute_r_icon = 'trait-conceptual.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Efficiency' ){
            	$assessment->attribute_l_description = 'Unwilling to exert effort on what is perceived to be a lost cause; values working smart instead of hard; pushes to find the easiest way to achieve goals; prefers working at a sustainable pace and avoids becoming overextended.';
            	$assessment->attribute_r_description = 'Driven to test his/her ability to persist when others are giving up; enjoys demonstrating his/her skill in overcoming significant obstacles; enjoys working long hours to get something important done ahead of others expectations.';
            	$assessment->attribute_l_icon = 'trait-efficiency.svg';
            	$assessment->attribute_r_icon = 'trait-endurance.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Ambition' ){
            	$assessment->attribute_l_description = 'Needs to steadily move up the ladder of success.  Compelled to do those things that most enhance chances of achieving success in life. Stays busy by achieving things of importance-i.e.,.. career and sense of success.  Finds it difficult to maintain an adequate balance between the demands of career and private life.';
            	$assessment->attribute_r_description = 'As long as thoroughly enjoying work, not important whether or not successes are rewarded with steady progress in profession.  Needs to have some time to sit back, relax and consider where all this effort is leading.  Finds it important to sense of well-being to have balance across many aspects of life.  Believes there are many things in life more important than achieving financial or professional success.  ';
            	$assessment->attribute_l_icon = 'trait-ambition.svg';
            	$assessment->attribute_r_icon = 'trait-balance.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Realistic' ){
            	$assessment->attribute_l_description = 'Understands concepts through experience. Has no use for ideas that don\'t have practical applications. Takes the shortest path to the goal. Prefers straightforward answers in plain language.';
            	$assessment->attribute_r_description = 'Impatient with people who take too long to get to the point. Finishes people\'s sentences. Invents new approaches in order to cope with situations. Inventive or resourceful. Takes different routes to experience variety or novelty.';
            	$assessment->attribute_l_icon = 'trait-realistic.svg';
            	$assessment->attribute_r_icon = 'trait-imaginative.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Competition' ){
            	$assessment->attribute_l_description = 'Looks for opportunities to test his/her skills and abilities against those of others; enjoys participating in and winning arguments or debates; willing to use any means to win at whatever he/she does.';
            	$assessment->attribute_r_description = 'Avoids individual competition; dislikes arguing or debating; avoids even the perception of having taken advantage of someone; seeks work environments characterized by collaboration instead of competition.';
            	$assessment->attribute_l_icon = 'trait-competition.svg';
            	$assessment->attribute_r_icon = 'trait-collaboration.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Persuasion' ){
            	$assessment->attribute_l_description = 'Seeks out chances to express opinions in ways that others end up seeing his/her way.  Once an opinion is formulated on an issue, struggles to give in to an opposing view.  Selling a new idea, concept, or product provides personal satisfaction.  Engaging in negotiations is highly stimulating and personally rewarding.';
            	$assessment->attribute_r_description = 'Offended by those who exert a lot of influence over others. Believes people should come to their own conclusions without undue influence from others. Uncomfortable convincing others that his/her point of view is superior to theirs. Prefers accepting the offering price of something to haggling for a better deal.';
            	$assessment->attribute_l_icon = 'trait-persuasion.svg';
            	$assessment->attribute_r_icon = 'trait-compliance.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'All Work' ){
            	$assessment->attribute_l_description = 'Avoids activities that simply pass time; postpones play until after all work is completed; prefers work to most other activities.';
            	$assessment->attribute_r_description = 'Leaves work behind and engages in recreational or family activities without second thoughts or discomfort; blends humor with serious work; works to afford favorite pastimes.';
            	$assessment->attribute_l_icon = 'trait-allwork.svg';
            	$assessment->attribute_r_icon = 'trait-allplay.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Tough-minded' ){
            	$assessment->attribute_l_description = 'Holds people accountable. Takes swift corrective action to get others back on track. Keeps the desired outcome clearly in mind at all times. Seeks the most direct path to goals. Intolerant of fooling around - takes things seriously.  ';
            	$assessment->attribute_r_description = 'Avoids hurting others\' feelings. Treats people with kindness. Does things for the right reasons vs. only for personal gain. Builds and maintains strong relationships over long periods of time. Avoids even the appearance of taking advantage of others. Takes a gentle approach to dealing with others.';
            	$assessment->attribute_l_icon = 'trait-toughminded.svg';
            	$assessment->attribute_r_icon = 'trait-tenderhearted.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Systematic' ){
            	$assessment->attribute_l_description = 'Takes a set of objectives and develops systems for achieving them. Works best within well-defined boundaries. Systematically approaches tasks regardless of the level of apparent chaos in the immediate environment; rarely pushed out of his/her routine no matter what else may be occurring at the time.  ';
            	$assessment->attribute_r_description = 'Unruffled by things not going as planned. Excited by a variety of demands coming at him/her at the same time. Looks forward with eager anticipation to being interrupted in the course of the day. ';
            	$assessment->attribute_l_icon = 'trait-systematic.svg';
            	$assessment->attribute_r_icon = 'trait-casual.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Reflective' ){
            	$assessment->attribute_l_description = 'Puts others views into proper context. Sits on the sidelines, watches, and thinks about what it means. Communicates with people in writing. Arrives at best insights after taking the time to carefully think about all the contributing factors.  ';
            	$assessment->attribute_r_description = 'Solicits the inputs of a variety of people when engaged in problem solving; allows ideas to take shape during discussions with everyone contributing; thinks out loud -- letting the thoughts flow without censorship.  Interacts with the things directly involved in the work -- manipulating, moving and experimenting with them; produces best ideas during active interactions with others; expresses even only crudely developed ideas for others to improve.';
            	$assessment->attribute_l_icon = 'trait-reflective.svg';
            	$assessment->attribute_r_icon = 'trait-interactive.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Critical Minded' ){
            	$assessment->attribute_l_description = 'Insists on seeing the evidence. Has a real talent for seeing the errors or oversights in a planned course of action. Doesn\'t take people\'s word for things - needs proof. Challenges assumptions of proposed action until assured things have been well thought through.  ';
            	$assessment->attribute_r_description = 'Unhesitatingly trusts people. Generous with praise and stingy with criticism. Gives people the benefit of the doubt. Emphasizes people\'s strengths rather than shortcomings. Very accepting of others\' faults.';
            	$assessment->attribute_l_icon = 'trait-criticalminded.svg';
            	$assessment->attribute_r_icon = 'trait-accepting.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Questioning' ){
            	$assessment->attribute_l_description = 'Seeks the role of devil\'s advocate. Energized by debate.  Asks lots of questions and seriously challenges sales pitches. Employs probing questions to reveal the truth.  ';
            	$assessment->attribute_r_description = 'Very accommodating to the needs of others. Seeks a way to compromise and avoid getting into conflicts. Devoted to reconciling differences over rendering judgment.';
            	$assessment->attribute_l_icon = 'trait-questioning.svg';
            	$assessment->attribute_r_icon = 'trait-accomodating.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Reasonable' ){
            	$assessment->attribute_l_description = 'Thinks things through and arrives at most reasonable course of action. Seeks underlying causes before proposing solutions. Finds most reasonable or logical course of action.  Doesn\'t let emotion influence decisions.';
            	$assessment->attribute_r_description = 'Expresses sympathy for the plight of others. Seeks underlying causes of conflict to bring resolution. Strongly values loyalty. Sees issues from everyone\'s perspective. Personal feelings influence decisions. Heart rules over head. Possesses great concern for the welfare of others.';
            	$assessment->attribute_l_icon = 'trait-reasonable.svg';
            	$assessment->attribute_r_icon = 'trait-compassionate.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Analytical' ){
            	$assessment->attribute_l_description = 'Takes pride in finding meaning in complex arrays of numbers. Believes it is important work to delve beyond the opinions of people, get to the facts, and formulate empirically based views on the issues at hand. Takes great delight in discovering patterns and trends in numerical data that other people have over-looked.';
            	$assessment->attribute_r_description = 'Derives insight through either listening to what people have to say or observing patterns of behavior - believes reviewing numbers doesn\'t afford the same level of understanding. Quickly bored with either presentations of financial or other numeric-based analyses; works best with high-level summaries of this kind of information.  ';
            	$assessment->attribute_l_icon = 'trait-analytical.svg';
            	$assessment->attribute_r_icon = 'trait-qualitative.svg';
            	$assessment->save();
            }
            elseif( $assessment->attribute_l == 'Methodical' ){
                  $assessment->attribute_l_description = 'Requires clearly delineated processes to follow. Makes lists of tasks to complete. Takes organized approaches to most tasks; once proven, employs the same methodology in similar circumstances.';
                  $assessment->attribute_r_description = 'Always open to new possibilities. Sees opportunities to every turn of events. Seeks new adventures.  Takes previously unexplored paths with enthusiasm and optimism.';
                  $assessment->attribute_l_icon = '';
                  $assessment->attribute_r_icon = '';
                  $assessment->save();
            }
            elseif( $assessment->attribute_l == 'Thick-skinned' ){
                  $assessment->attribute_l_description = 'Avoids taking the blame for mistakes; is relatively indifferent to criticism; believes that the mistakes of others are the result of not following his/her directions explicitly; is not particularly interested in listening to the complaints or concerns of others.   ';
                  $assessment->attribute_r_description = 'Among the most accessible of people; encourages people\'s complaints or criticism; does to great lengths to objectively evaluate criticism from almost any source; often allowing others to take the lead, feels little need to assert his/her authority; willing to take the blame for the mistakes of others. ';
                  $assessment->attribute_l_icon = '';
                  $assessment->attribute_r_icon = '';
                  $assessment->save();
            }
            elseif( $assessment->attribute_l == 'Early Starting' ){
                  $assessment->attribute_l_description = 'Gives self a lot of time to prepare. Relentlessly sticks to a plan. Frustrated when steady progress is not being made. Always takes time to properly prepare.';
                  $assessment->attribute_r_description = 'Requires strict deadlines to make significant progress. Productivity varies with the demands. Rarely gets an early start on a project.  Waits to the last moment to get started.';
                  $assessment->attribute_l_icon = '';
                  $assessment->attribute_r_icon = '';
                  $assessment->save();
            }
            elseif( $assessment->attribute_l == 'Stability' ){
                  $assessment->attribute_l_description = 'Cautious and skeptical. Will hesitate or be slow to accept a new course of action. Focused on tangible events and returns. Wary of long-term forecasts and uncharted territory.';
                  $assessment->attribute_r_description = 'Finds change stimulating and refreshing; will make changes simply for the thrill he/she finds in novelty; stagnation and failure to grow make him/her uncomfortable; is quickly bored by repetitive tasks or conditions.  ';
                  $assessment->attribute_l_icon = '';
                  $assessment->attribute_r_icon = '';
                  $assessment->save();
            }
            elseif( $assessment->attribute_l == 'Planful' ){
                  $assessment->attribute_l_description = 'Develops plans that attempt to account for all contingencies. Strongly values developing a plan before taking action. Seeks to minimize surprises. Pays close attention to details in developing action plans.  ';
                  $assessment->attribute_r_description = 'Takes pleasure in every moment. Usually puts pleasure ahead of duty.  Enthusiastically adapts to changing circumstances. Employs only broad plans that are readily open to modification. Never locked in to the specifics of a plan.  Places little value on attempts to anticipate all that could happen.';
                  $assessment->attribute_l_icon = '';
                  $assessment->attribute_r_icon = '';
                  $assessment->save();
            }
            elseif( $assessment->attribute_l == 'Intimate' ){
                  $assessment->attribute_l_description = 'Finds spending time with one or two close friends most stimulating. Prefers being close to a few people. Expresses self best in one-on-one conversations. Struggles for words when in front of groups. One-on-one dialogue with a colleague represents quality time.  ';
                  $assessment->attribute_r_description = 'Seeks opportunities to be a popular person. Needs to be a part of a wide variety of business and social groups. Usually finds his/her way to the center of discussions or activities. Insists on working closely with other people rather than alone. Energized by social gatherings.';
                  $assessment->attribute_l_icon = '';
                  $assessment->attribute_r_icon = '';
                  $assessment->save();
            }
            elseif( $assessment->attribute_l == 'Pessimism' ){
                  $assessment->attribute_l_description = 'Exercises caution and restraint when facing new sets of circumstances; resists the tide of enthusiasm for new things until ramifications can be evaluated; maintains healthy skepticism about how things will be better in the future.  Sees the various ways things might veer off course; anticipates potential trouble even in the best of circumstances; keeps a vigilant eye out for the negative signs and downplay the positive ones.';
                  $assessment->attribute_r_description = 'Takes the optimistic view of most sets of circumstances; becomes energized by possibility of doing something new; shows upbeat, positive attitudes about the future.  Envisions all the good things that might result from a course of action; sees the upside potential of even the worse set of circumstances; emphasizes the positive signs and downplay the negative ones.';
                  $assessment->attribute_l_icon = '';
                  $assessment->attribute_r_icon = '';
                  $assessment->save();
            }
            elseif( $assessment->attribute_l == 'Influence' ){
                  $assessment->attribute_l_description = 'Loves the challenge of convincing people to shift to his/her position; most satisfied when he/she can clearly see his/her impact on course of events; driven to make a difference in some meaningful way even if no one else sees it; finds it very difficult to carry out plans of others without putting his/her mark on them.  ';
                  $assessment->attribute_r_description = 'Resists pushing his/her ideas onto others; better in responding to questions; much more comfortable supporting the efforts of others than leading the charge; makes it very easy for others to influence his/her thinking; has no trouble picking up and carrying on a project initiated by others without having to make substantial changes. ';
                  $assessment->attribute_l_icon = '';
                  $assessment->attribute_r_icon = '';
                  $assessment->save();
            }
            elseif( $assessment->attribute_l == 'Achievement' ){
                  $assessment->attribute_l_description = 'Dissatisfied with current accomplishments; driven to raise the standards or to be the first or best; takes pride in symbols of achievement; constantly looks for more and greater challenges.  ';
                  $assessment->attribute_r_description = 'Symbols of achievement hold little value; finds it easier to go along with things than to exert control; prefers exploring issues over driving toward specific objectives; places little value on striving for goal attainment.  ';
                  $assessment->attribute_l_icon = '';
                  $assessment->attribute_r_icon = '';
                  $assessment->save();
            }
            elseif( $assessment->attribute_l == 'Order' ){
                  $assessment->attribute_l_description = 'Uncomfortable with disorganized environments; seeks to keep things in order and well organized; devotes time to carefully planning the details of projects; prepares for presentations or events well in advance; enjoys the comfort of well established methods or processes.  ';
                  $assessment->attribute_r_description = 'Takes action with only the general topic of what is to be discussed; enjoys organizing thoughts on the fly; resists pressure to follow established methods; values having the freedom to adapt to situations as they occur.  ';
                  $assessment->attribute_l_icon = '';
                  $assessment->attribute_r_icon = '';
                  $assessment->save();
            }
        }
    }
}
