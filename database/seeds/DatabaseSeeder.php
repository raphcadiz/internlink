<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(IndustriesSeeder::class);
        $this->call(CareersSeeder::class);
        $this->call(CandidateTraitsSeeder::class);
        $this->call(colleges_db_seeder::class);
        $this->call(CollegeMajorSeeder::class);
        $this->call(PlansSeeder::class);
        $this->call(AssessmentSeeder::class);
        $this->call(AssessmentIdOnCandidatesTraits::class);
        $this->call(PersonalityTypeSeeder::class);
        $this->call(AssessmentsDescriptionsSeeder::class);
        $this->call(BusinessSizeSeeder::class);
        $this->call(EmployerTypeSeeder::class);
        $this->call(NotificationSeeder::class);
        $this->call(ApplicantStatusSeeder::class);
    }
}
