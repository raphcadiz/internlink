<?php

use Illuminate\Database\Seeder;

class CandidateTraitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('candidate_traits')->insert(array(
                array(
                    'trait_1'          => 'Detail Minded',
                    'trait_2'          => 'Big Picture'
                ),
                array(
                    'trait_1'          => 'Practical-minded',
                    'trait_2'          => 'Conceptual'
                ),
                array(
                    'trait_1'          => 'Efficiency',
                    'trait_2'          => 'Endurance'
                ),
                array(
                    'trait_1'          => 'Ambition',
                    'trait_2'          => 'Balance'
                ),
                array(
                    'trait_1'          => 'Realistic',
                    'trait_2'          => 'Imaginative'
                ),
                array(
                    'trait_1'          => 'Competition',
                    'trait_2'          => 'Collaboration'
                ),
                array(
                    'trait_1'          => 'Persuasion',
                    'trait_2'          => 'Compliance'
                ),
                array(
                    'trait_1'          => 'All Work',
                    'trait_2'          => 'Play'
                ),
                array(
                    'trait_1'          => 'Tough-minded',
                    'trait_2'          => 'Tender-hearted'
                ),
                array(
                    'trait_1'          => 'Systematic',
                    'trait_2'          => 'Casual'
                ),
                array(
                    'trait_1'          => 'Reflective',
                    'trait_2'          => 'Interactive'
                ),
                array(
                    'trait_1'          => 'Critical-minded',
                    'trait_2'          => 'Accepting'
                ),
                array(
                    'trait_1'          => 'Questioning',
                    'trait_2'          => 'Accommodating'
                ),
                array(
                    'trait_1'          => 'Reasonable',
                    'trait_2'          => 'Compassionate'
                ),
                array(
                    'trait_1'          => 'Analytical',
                    'trait_2'          => 'Qualitative'
                ),
            )
        );
    }
}
