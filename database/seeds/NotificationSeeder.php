<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class NotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$employer_id = Role::whereRole('Employer')->first()->id;
    	$student_id = Role::whereRole('Student')->first()->id;

        DB::table('notifications')->insert(array(
                array(
                    'notification'	=> 'New Applicants',
                    'description'	=> 'Receive a message when you have candidates applied to your jobs.',
                    'role_id' 		=> $employer_id
                ),
                array(
                    'notification'	=> 'Messages',
                    'description'	=> 'Receive a notification when an applicant message you.',
                    'role_id' 		=> $employer_id
                ),
                array(
                    'notification'	=> 'Applicant Invitations',
                    'description'	=> 'Receive a message with how many candidates have applied to your jobs from your invitations.',
                    'role_id' 		=> $employer_id
                ),
                array(
                    'notification'	=> 'Posting Views',
                    'description'	=> 'Receive a daily message with how many candidates viewed your postings.',
                    'role_id' 		=> $employer_id
                ),
                array(
                    'notification'	=> 'Messages from Internlogic',
                    'description'	=> 'Occastional emails from InternLogic with tips and offers on how to get the most out of InternLogic.',
                    'role_id' 		=> $employer_id
                ),
                array(
                    'notification'	=> 'Invitations',
                    'description'	=> 'Receive a message when you have been invited to apply to a job.',
                    'role_id' 		=> $student_id
                ),
                array(
                    'notification'	=> 'Company Viewed your Profile',
                    'description'	=> 'Be notified when a company looks at your profile.',
                    'role_id' 		=> $student_id
                ),
                array(
                    'notification'	=> 'Messages',
                    'description'	=> 'Receive a notification when a company messages you.',
                    'role_id' 		=> $student_id
                ),
                array(
                    'notification'	=> 'New Jobs Added',
                    'description'	=> 'Receive a weekly updates when jobs are added.',
                    'role_id' 		=> $student_id
                ),
                array(
                    'notification'	=> 'Application Confirmation',
                    'description'	=> 'Get notified that your application was submitted successfully.',
                    'role_id' 		=> $student_id
                ),
                array(
                    'notification'	=> 'Messages from Internlogic',
                    'description'	=> 'Occastional emails from InternLogic with tips and offers on how to get the most out of InternLogic.',
                    'role_id' 		=> $student_id
                ),
        	)
        );
    }
}
