<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class AssessmentSeeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function __construct()
    {
        $this->table = 'assessments';
        $this->filename = base_path().'/database/seeds/csvs/Assessment-Normsv1.csv';
        $this->offset_rows = 1;
        $this->mapping = [
	        1 => 'statement_1',
	        2 => 'statement_2',
	        3 => 'attribute_l',
	        4 => 'attribute_r',
	        5 => 'mean',
	        6 => 'stdev',
	        7 => 'myersbriggs'
	    ];
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        parent::run();
    }
}
