<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(array(
                array(
                    'role'          => 'Administrator',
                    'description'   => 'Owner of the site'
                ),
        		array(
        			'role'			=> 'Employer',
        			'description'	=> 'User who can employ'
        		),
        		array(
        			'role'			=> 'Student',
        			'description'	=> 'Applicants'
        		),
        	)
        );
    }
}
