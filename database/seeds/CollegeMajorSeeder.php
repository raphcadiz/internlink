<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class CollegeMajorSeeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = 'college_majors';
        $this->filename = base_path().'/database/seeds/csvs/College-Majors-Sheet.csv';
        $this->offset_rows = 1;
        $this->mapping = [
	        0 => 'fod1p',
	        1 => 'major',
	        2 => 'major_category'
	    ];
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        parent::run();
    }
}
