<?php

use Illuminate\Database\Seeder;
use App\Models\Assessment;
use App\Models\CandidateTraits;

class AssessmentIdOnCandidatesTraits extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $candidateTraits = CandidateTraits::all();

        foreach($candidateTraits as $candidateTrait) {
            $assessment = Assessment::where('attribute_l', $candidateTrait->trait_1)
                                    ->orWhere('attribute_r', $candidateTrait->trait_2)
                                    ->first();

            if($assessment) {
                $candidateTrait->assessment_id = $assessment->id;
                $candidateTrait->save();
            }
        }
    }
}
