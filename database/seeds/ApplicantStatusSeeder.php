<?php

use Illuminate\Database\Seeder;

class ApplicantStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('applicant_statuses')->insert(array(
                ['status_name' => 'New'],
                ['status_name' => 'Reviewed'],
                ['status_name' => 'Phone Screened'],
                ['status_name' => 'Interviewed'],
                ['status_name' => 'Offer Made'],
                ['status_name' => 'Hired'],
                ['status_name' => 'Rejected']
            )
        );

        \Illuminate\Support\Facades\DB::table('job_applicants')->update(['status_id' => '1']);
    }
}
