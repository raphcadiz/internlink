<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;

class colleges_db_seeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = 'colleges';
        $this->filename = base_path().'/database/seeds/csvs/College-Database-Sheet1.csv';
        $this->mapping = [
	        0 => 'college',
	    ];
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        parent::run();
    }
}
