<?php

use Illuminate\Database\Seeder;

class BusinessSizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('business_sizes')->insert(array(
                array(
                    'size_name'          => 'Small (00-99)',
                ),
                array(
                    'size_name'          => 'Medium (100-499)',
                ),
                array(
                    'size_name'          => 'Large (500+)',
                )
            )
        );
    }
}
