var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass(['app.scss'], 'public/assets/css/bootsrap.css');
    mix.sass(['styles.scss'], 'public/assets/css/styles.css');
    mix.sass('admin-style.scss');

    mix.browserify('main.js');
    mix.browserify('admin-user-management.js');
    mix.browserify('applicants-tab.js');
    mix.browserify('applicants.js');
    mix.browserify('apply-job.js');
    mix.browserify('jobs.js');
    mix.browserify('matches.js');
    mix.browserify('admin-coupon.js');
    mix.browserify('admin-plan.js');
    mix.browserify('admin-college.js');
    mix.browserify('admin-career.js');
});
