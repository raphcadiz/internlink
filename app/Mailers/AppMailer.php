<?php

namespace App\Mailers;

use App\Models\User;
use Illuminate\Contracts\Mail\Mailer;

class AppMailer
{
    /**
     * The Laravel Mailer instance.
     *
     * @var Mailer
     */
    protected $mailer;

    /**
     * The sender of the email.
     *
     * @var string
     */
    protected $from = 'support@internlogic.com';

    /**
     * The recipient of the email.
     *
     * @var string
     */
    protected $to;

    /**
     * The view for the email.
     *
     * @var string
     */
    protected $view;

    protected $subject;
    /**
     * The data associated with the view for the email.
     *
     * @var array
     */
    protected $data = [];

    /**
     * Create a new app mailer instance.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Deliver the email confirmation.
     *
     * @param  User $user
     * @return void
     */
    public function sendEmailConfirmationTo(User $user)
    {
        $this->to = $user->email;
        $this->subject = "Sign up";
        $this->view = 'emails.confirm';
        $this->data = compact('user');

        $this->deliver();
    }

    public function sendJobApplicantionNotification(User $user, $job)
    {
        $this->to =  $job->user->email;
        $this->subject = "Job Application";
        $this->view = 'emails.job-application';
        $this->data = compact('user','job');

        $this->deliver();
    }

    public function newUserByAdminConfirmation(User $user, $password)
    {
        $this->to = $user->email;
        $this->subject = "Sign up";
        $this->view = 'emails.confirm-admin-user-creation';
        $this->data = compact('user', 'password');

        $this->deliver();
    }

    public function sendCandidateInviteNotification($user, $job)
    {
        $this->to = $user->email;
        $this->subject = "Job Invitation";
        $this->view = 'emails.job-invitation';
        $this->data = compact('user', 'job');
        
        $this->deliver();
    }

    /**
     * Deliver the email.
     *
     * @return void
     */
    public function deliver()
    {
        $this->mailer->send($this->view, $this->data, function ($message) {
            $message->from($this->from, 'InternLogic')
                    ->to($this->to)
                    ->subject($this->subject);
        });
    }
}
