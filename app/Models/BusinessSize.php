<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessSize extends Model
{
    protected $table = 'business_sizes';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'size_name'
    ];

    public function employers()
    {
        return $this->hasMany('App\Models\EmployerInformation');
    }
}
