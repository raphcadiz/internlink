<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CandidateTraitSelection extends Model
{
    protected $fillable = [
        'job_id',
        'trait_id',
        'employer_standard_score'
    ];

    public function candidateTrait()
    {
        return $this->belongsTo('App\Models\CandidateTraits', 'trait_id');
    }

    public function job()
    {
        return $this->belongsTo('App\Models\Job', 'job_id');
    }

}
