<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewedStudent extends Model
{
    protected $table = 'viewed_students';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'student_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function student()
    {
        return $this->belongsTo('App\Models\User', 'student_id');
    }
}
