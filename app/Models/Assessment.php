<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assessment extends Model
{
    public function candidateTrait()
    {
        return $this->hasOne('App\Models\CandidateTrait');
    }
}
