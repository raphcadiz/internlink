<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobCandidate extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job_candidates';

    /**
     * The table's primary key if other than 'id'
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id', 'user_id'
    ];

    public function job()
    {
        return $this->belongsTo('App\Models\Job');
    }

    public function user()
    {
        return $this->hasMany('App\Models\User', 'id', 'user_id');
    }
}