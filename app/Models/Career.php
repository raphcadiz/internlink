<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Career extends Model
{
	use SoftDeletes;

	protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function industry()
    {
    	return $this->belongsTo('App\Models\Industry', 'industry_id')->withTrashed();
    }

    public function jobs()
    {
    	return $this->hasMany('App\Models\Job');
    }
}
