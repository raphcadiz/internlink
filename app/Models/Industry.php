<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Industry extends Model
{
	use SoftDeletes;

	protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function careers()
    {
    	return $this->hasMany('App\Models\Career');
    }

    public function companies()
    {
    	return $this->hasMany('App\Models\EmployerInformation');
    }
}
