<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployerType extends Model
{
    protected $table = 'employer_types';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'type_name'
    ];

    public function employers()
    {
        return $this->hasMany('App\Models\EmployerInformation');
    }
}
