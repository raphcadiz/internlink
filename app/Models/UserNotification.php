<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
	protected $fillable = [
        'notification_id'
    ];

    public function user()
    {
    	return $this->belongsTo('App\Models\User', 'user_id');
    }
}
