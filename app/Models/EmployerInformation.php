<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployerInformation extends Model
{
	protected $dates = ['created_at', 'updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function industry()
    {
    	return $this->belongsTo('App\Models\Industry', 'industry_id');
    }

    public function businessSize()
    {
        return $this->belongsTo('App\Models\BusinessSize', 'business_size_id');
    }

    public function employerType()
    {
        return $this->belongsTo('App\Models\EmployerType', 'employer_type_id');
    }
}
