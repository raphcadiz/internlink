<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Cmgmyr\Messenger\Traits\Messagable;

class User extends Authenticatable
{
    use Billable;
    use SoftDeletes;
    use Messagable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'middle_name', 'last_name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->activation_token = str_random(30);
        });
    }

    public function owns($related)
    {
        return $this->id == $related->user_id;
    }

    public function hasRole($role)
    {
        if( $this->role->role == $role )
            return true;

        return false;
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id');
    }

    public function isSuperAdmin()
    {
        if( $this->role->role == 'Administrator' )
            return true;

        return false;
    }


    public function basic_information()
    {
        if( $this->hasRole('Employer') )
            return $this->hasOne('App\Models\EmployerInformation');

        if( $this->hasRole('Student') )
            return $this->hasOne('App\Models\StudentInformation');

        return false;
    }

    public function isPremiumStudent()
    {
        return $this->subscribed('premium');
    }

    public function isPremiumEmployer()
    {
        if($this->subscribed('starter'))
            return true;

        if($this->subscribed('plus'))
            return true;

        if($this->subscribed('pro'))
            return true;

        return false;
    }

    public function jobs()
    {
        if( $this->hasRole('Employer') )
            return $this->hasMany('App\Models\Job');

        return false;
    }

    public function total_active_jobs()
    {
        return $this->jobs()->whereStatus(1)->count();
    }

    public function plan_payments()
    {
        return $this->hasMany('App\Models\PlanPayment');
    }

    public function plan_subscriptions()
    {
        return PlanPayment::join('subscriptions', 'plan_payments.stripe_id', '=', 'subscriptions.stripe_id')
                    ->select('plan_payments.id as plan_payment_id', 'plan_payments.*', 'subscriptions.*')
                    ->where('plan_payments.user_id', '=', $this->id)
                    ->orderBy('plan_payments.created_at')
                    ->get();
    }

    public function trial_consumed()
    {
        return $this->trial;
    }

    public function saved_jobs()
    {
        return $this->hasMany('App\Models\SavedJob');
    }

    public function isSavedJob($job_id)
    {
        $job = SavedJob::whereJobId($job_id)->whereUserId($this->id)->first();
        if( empty($job) )
            return false;

        return true;
    }

    public function isInvitedJob($job_id)
    {
        $job_applied = JobCandidate::whereJobId($job_id)->whereUserId($this->id)->first();
        if( empty($job_applied) )
            return false;

        return true;
    }

    public function isViewedBy($user_id)
    {
        $viewed_student = ViewedStudent::whereUserId($user_id)->whereStudentId($this->id)->first();
        if( empty($viewed_student) )
            return false;

        return true;
    }

    public function assessments()
    {
        return $this->hasMany('App\Models\StudentAssessment');
    }

    public function assessement_raw_score($assessment_id)
    {
        $student_assessment = StudentAssessment::whereAssessmentId($assessment_id)->whereUserId($this->id)->first();
        if($student_assessment) {
            return $student_assessment->student_raw_score;
        } else {
            return 0;
        }
    }

    public function assessment_results()
    {
        $assessment_results  = $this->assessments()
                            ->join('assessments', 'assessments.id', '=', 'student_assessments.assessment_id')
                            ->join('candidate_traits', 'candidate_traits.assessment_id', '=', 'assessments.id')
                            ->select(DB::raw('assessments.*, student_assessments.*'))
                            ->where('student_assessments.user_id', $this->id)
                            ->get();

        return $assessment_results;
    }

    public function confirmEmail()
    {
        $this->activation_token = null;
        $this->verified = true;
        $this->save();

        $notifications = Notification::whereRoleId($this->role->id)->get(); 
        foreach ($notifications as $notification) {
            $email_notif = new UserNotification(['notification_id' => $notification->id]);
            $this->notifications()->save($email_notif);
        }
    }

    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function appliedJobs()
    {
        return $this->hasMany('Models\JobApplicant', 'user_id');
    }

    public function isAppliedJob($job_id)
    {
        $job_applied = JobApplicant::whereJobId($job_id)->whereUserId($this->id)->first();
        if( empty($job_applied) )
            return false;

        return true;
    }

    public function qualifiedJobs()
    {
        return $this->hasMany('App\Models\JobCandidate', 'user_id');
    }

    public function getJobCompatibility($job_id)
    {
        $assessments = $this->assessments()
                            ->join('assessments', 'assessments.id', '=', 'student_assessments.assessment_id')
                            ->join('candidate_traits', 'candidate_traits.assessment_id', '=', 'assessments.id')
                            ->join('candidate_trait_selections', 'candidate_trait_selections.trait_id', '=', 'candidate_traits.id')
                            ->select(DB::raw("assessments.id AS assessment_id, student_assessments.student_final_score as sss, candidate_trait_selections.employer_standard_score as ess"))
                            ->where('candidate_trait_selections.job_id', $job_id)
                            ->where('student_assessments.user_id', $this->id)
                            ->get();

        return $assessments;
    }

    public function getJobCompatibilityPercentage($job_id)
    {
        $assessments = $this->getJobCompatibility($job_id);

        $matchScores = [];
        foreach($assessments as $assessment) {
            $ssd =  abs($assessment->sss - $assessment->ess);
            $matchScores[] = round(100 - $ssd, 2);
        }

        $count = count($matchScores);
        if($count == 0) {
            return 0;
        }

        $sum = array_sum($matchScores);
        $finalMatchScore = round($sum / $count, 2);
        // $finalMatchScore = ( ($finalMatchScore - 48) / (84 - 48) ) * 100 ;

        return $finalMatchScore;
    }

    private function calculatePersonalityTrait($traits){

        if( $traits == 'introvert_extrovert' ){
            $traits = ['Optimism', 'Gregorious', 'Interactive'];
            $pMean   = 58.87;
            $pstdDev = 13.54;
        }
        elseif( $traits == 'sensing_intuiting' ){
            $traits = ['Imaginative', 'Conceptual'];
            $pMean   = 51.43;
            $pstdDev = 13.70;
        }
        elseif( $traits == 'thinking_feeling' ){
            $traits = ['Tender-hearted', 'Compassionate', 'Accommodating', 'Accepting'];
            $pMean   = 54.13;
            $pstdDev = 12.64;
        }
        elseif( $traits == 'judging_perceiving' ){
            $traits = ['Emergent', 'Pressure-prompted', 'Casual', 'Open-minded'];
            $pMean   = 56.47;
            $pstdDev = 13.88;
        }

        $traits = Assessment::whereIn('attribute_r', $traits)->get();
        $traits_ids = array();
        foreach ($traits as $key => $value) {
            $traits_ids[] = $value->id;
        }
        $student_assessments = StudentAssessment::whereIn('assessment_id', $traits_ids)->whereUserId($this->id)->get();
        $srs_group = array();
        foreach ($student_assessments as $student_assessment) {
            $srs_group[] = $student_assessment->student_raw_score;
        }

        $student_personality_mean = array_sum($srs_group) / count($srs_group);
        $pDiff = $student_personality_mean - $pMean;
        $ptValue = $pDiff / $pstdDev;
        $standard_ptValue = $ptValue * 10;
        $css = $standard_ptValue + 50;

        return round($css, 2);
    }

    public function personalityType(){

        if(!$this->hasRole('Student'))
            return array();

        $introvert_extrovert = $this->calculatePersonalityTrait('introvert_extrovert');
        // I for Introvert, E for Extrovert
        $ie = $introvert_extrovert < 50 ? 'I' : 'E';

        $sensing_intuiting = $this->calculatePersonalityTrait('sensing_intuiting');
        // S for Sensing, N for Intuiting
        $se = $sensing_intuiting < 50 ? 'S' : 'N';

        $thinking_feeling = $this->calculatePersonalityTrait('thinking_feeling');
        // T for Thinking, F for feeling
        $tf = $thinking_feeling < 50 ? 'T' : 'F';

        $judging_perceiving = $this->calculatePersonalityTrait('judging_perceiving');
        // J for Judging, P for perceiving
        $jp = $judging_perceiving < 50 ? 'J' : 'P';


        $personality_type = PersonalityType::whereType($ie.$se.$tf.$jp)->first();

        return $personality_type;
    }

    public function jobViews()
    {
        return $this->hasMany('App\Models\JobView');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\UserNotification');
    }

    public function userAllowNotification($notification_id)
    {
        return $this->notifications()->where('notification_id', '=', $notification_id)->get()->count();
    }

    public function messageThreads()
    {
        return User::join('participants', 'participants.user_id', '=', 'users.id')
                ->join('threads', 'threads.id', '=', 'participants.thread_id')
                ->where('users.id', $this->id)
                ->get();
    }
}
