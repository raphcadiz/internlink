<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Job extends Model
{
    //

    protected $fillable = [
        'user_id',
        'title',
        'company_description',
        'responsibilities',
        'requirements',
        'skills',
        'employment_type',
        'job_start',
        'job_end',
        'job_type',
        'paid',
        'salary',
        'rate',
        'job_url',
        'status',
        'created_at',
        'updated_at',
        'career_id',
        'gpa',
        'year',
        'virtual'
    ];

    public function career()
    {
    	return $this->belongsTo('App\Models\Career', 'career_id');
    }

    public function traitsSelection()
    {
        return $this->hasMany('App\Models\CandidateTraitSelection');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function applicants()
    {
        return $this->hasMany('App\Models\JobApplicant');
    }

    public function candidates()
    {
        return $this->hasMany('App\Models\JobCandidate');
    }

    public function getStudentCompatibility($user)
    {
        $assessments = $this->traitsSelection()
                            ->join('candidate_traits', 'candidate_traits.id', '=', 'candidate_trait_selections.trait_id')
                            ->join('assessments', 'assessments.id', '=', 'candidate_traits.assessment_id')
                            ->join('student_assessments', 'student_assessments.assessment_id', '=', 'assessments.id')
                            ->select(DB::raw("assessments.id AS assessment_id, student_assessments.student_final_score as sss, candidate_trait_selections.employer_standard_score as ess"))
                            ->where('student_assessments.user_id', $user->id)
                            ->get();
        return $assessments;
    }

    public function getStudentCompatibilityPercentage($user)
    {
        $assessments = $this->getStudentCompatibility($user);

        $matchScores = [];
        foreach($assessments as $assessment) {
            $ssd =  abs($assessment->sss - $assessment->ess);
            $matchScores[] = round(100 - $ssd, 2);
        }

        $count = count($matchScores);
        if($count == 0) {
            return 0;
        }

        $sum = array_sum($matchScores);
        $finalMatchScore = round($sum / $count, 2);
        // $finalMatchScore = ( ($finalMatchScore - 48) / (84 - 48) ) * 100 ;

        return $finalMatchScore;
    }

    public function studentViews()
    {
        return $this->hasMany('App\Models\JobView');
    }

    public function getPaidApplicantsCount()
    {
        $applicants_count = $this->applicants()->count();
    }
}
