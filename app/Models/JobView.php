<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobView extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job_views';

    /**
     * The table's primary key if other than 'id'
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id', 'student_id'
    ];

    public function job()
    {
        return $this->belongsTo('App\Models\Job');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'student_id');
    }
}