<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CandidateTraits extends Model
{
    public function traitSelection()
    {
        return $this->hasMany('App\Models\CandidateTraitSelection');
    }

    public function assessment()
    {
        return $this->belongsTo('App\Models\Assessment', 'assessment_id');
    }
}
