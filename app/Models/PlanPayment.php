<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanPayment extends Model
{
	public $table = 'plan_payments';
	
	public function user()
	{
		return $this->belongsTo('App\Models\User', 'user_id');
	}
}
