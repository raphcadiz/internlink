<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class College extends Model
{
    use SoftDeletes;

    protected $fillable = ['college'];

    protected $guarded = ['id'];

}
