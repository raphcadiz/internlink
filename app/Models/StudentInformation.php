<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentInformation extends Model
{
    protected $dates = ['created_at', 'updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function college()
    {
        return $this->belongsTo('App\Models\College', 'college_id');
    }

    public function major()
    {
    	return $this->belongsTo('App\Models\CollegeMajor', 'major_id');
    }

}
