<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    //
	use SoftDeletes;
	
    protected $fillable = ['name', 'alias', 'price'];

    protected $guarded = ['id'];

}
