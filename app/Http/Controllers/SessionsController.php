<?php 
namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SessionsController extends Controller
{
    /**
     * Create a new sessions controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Show the login page.
     *
     * @return \Response
     */
    public function login()
    {
        return view('auth.login');
    }

    /**
     * Perform the login.
     *
     * @param  Request  $request
     * @return \Redirect
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, ['email' => 'required|email', 'password' => 'required']);

        if ($this->signIn($request)) {
            $user = Auth::user();

            if($user->hasRole('Employer')){
                if($user->profile_progress == 0)
                    return redirect('employer/edit-basic-information/'.$user->id);

                return redirect('employer/postings');
            }

            elseif($user->hasRole('Student')){
                if($user->profile_progress == 0)
                    return redirect('student/edit-basic-information/'.$user->id);
                
                return redirect('/student/matches');
            }

            elseif($user->hasRole('Administrator'))
                return redirect('administrator/dashboard');

            else
                return redirect('/');

        }


        return redirect()->back()->with('error', 'Could not sign you in.');
    }

    /**
     * Destroy the user's current session.
     *
     * @return \Redirect
     */
    public function logout()
    {
        $user = Auth::user();
        Auth::logout();

        if($user->role->role == 'Employer'){
            return redirect('/');
        }
        else if($user->role->role == 'Student'){
            return redirect('http://www.internmagic.com/');
        }
        else {
            return redirect('/');
        }
    }

    /**
     * Attempt to sign in the user.
     *
     * @param  Request $request
     * @return boolean
     */
    protected function signIn(Request $request)
    {
        return Auth::attempt($this->getCredentials($request), $request->has('remember'));
    }

    /**
     * Get the login credentials and requirements.
     *
     * @param  Request $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return [
            'email'    => $request->input('email'),
            'password' => $request->input('password'),
            'verified' => true
        ];
    }
}
