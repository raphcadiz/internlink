<?php

namespace App\Http\Controllers;

use App\Models\Career;
use App\Models\StudentAssessment;
use App\Models\User;
use App\Repositories\JobRepository;
use App\Services\JobService;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Models\Industry;
use App\Models\Job;
use App\Models\CandidateTraits;

class JobController extends Controller
{

    protected $jobRepository;
    protected $jobService;

    public function __construct()
    {
        $this->jobRepository = new JobRepository();
        $this->jobService = new JobService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('backend.employer.jobs.postings', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $industries = Industry::orderBy('industry')->get();
        $careers = Auth::user()->basic_information->industry->careers()->orderBy('title')->get();

        return view('backend.employer.jobs.create', compact('industries', 'careers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'salary' => 'required',
        ]);

        $dates = (!empty($request->input('job_dates'))) ? explode('-', $request->input('job_dates')) : null;
        $start = (!is_null($dates)) ? date('Y-m-d', strtotime(trim($dates[0]))) : null;
        $end = (!is_null($dates)) ? date('Y-m-d', strtotime(trim($dates[1]))) : null;

        $job = new Job;
        $job->title                 = $request->input('title');
        $job->user_id               = $request->user()->id;
        $job->career_id             = $request->input('career_id');
        $job->company_description   = $request->input('company_description');
        $job->responsibilities      = $request->input('responsibilities');
        $job->requirements          = $request->input('requirements');
        $job->skills                = $request->input('skills');
        $job->employment_type       = $request->input('employment_type');
        $job->job_start             = $start;
        $job->job_end               = $end;
        $job->job_type              = $request->input('job_type');
        $job->paid                  = $request->input('paid');
        $job->salary                = $request->input('salary');
        $job->rate                  = $request->input('rate');
        $job->gpa                   = $request->input('gpa');
        $job->year                  = $request->input('year');
        $job->virtual               = $request->input('virtual_internship');

        $user = Auth::user();
        if ($user->cannot('activate_posting', $user))
            $job->status = 0;
        else
            $job->status = 1;

        $job->save();

        return redirect("employer/candidate/trait-selection/$job->id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getJobDetails($id)
    {
        $job = Job::findOrFail($id);

        $skills = (!empty($job->skills) && !is_null($job->skills)) ? explode(',', $job->skills) : [];

        return view('backend.employer.jobs.details', compact('job', 'skills'));
    }

    public function getChangeStatus($id, $status_id)
    {
        $job = Job::findOrFail($id);

        return view('backend.employer.jobs.modals.change-status', compact('job', 'status_id'));
    }

    public function postChangeStatus(Request $request)
    {
        $data = $request->all();

        try {

            $job = Job::findOrFail($data['job_id']);

            $job->status = $data['status'];
            $job->save();

            // TODO: apply conditions on note

        } catch(Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json(['message' => 'Status successfully changed.'], 200);
    }

    public function getFreeUserWarning()
    {
        return view('backend.layouts.unpaid-user-message');
    }

    public function getUpdateJob($job_id)
    {
        $job = Job::findOrFail($job_id);
        $careers = Auth::user()->basic_information->industry->careers()->orderBy('title')->get();

        return view('backend.employer.jobs.update', compact('job', 'careers'));
    }

    public function postUpdateJob(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'salary' => 'required',
        ]);

        $dates = (!empty($request->input('job_dates'))) ? explode('-', $request->input('job_dates')) : null;
        $start = (!is_null($dates)) ? date('Y-m-d', strtotime(trim($dates[0]))) : null;
        $end = (!is_null($dates)) ? date('Y-m-d', strtotime(trim($dates[1]))) : null;

        $data = $request->all();

        $data['job_start'] = $start;
        $data['job_end'] = $end;
        $data['virtual'] = $data['virtual_internship'];

        $job = $this->jobRepository->find($data['job_id']);

        unset($data['_token']);
        unset($data['job_dates']);
        unset($data['job_id']);
        unset($data['show_salary']);
        unset($data['virtual_internship']);

        $job = $this->jobRepository->update($job->id, $data);

        $skills = (!empty($job->skills) && !is_null($job->skills)) ? explode(',', $job->skills) : [];

        return view('backend.employer.jobs.details', compact('job', 'skills'));
    }

    public function getUpdateTraitSelection($job_id)
    {
        $job = Job::findOrFail($job_id);
        $traits = CandidateTraits::all();

        return view('backend.employer.jobs.update-trait-selection', compact('traits', 'job'));
    }

    public function postUpdateTraitSelection(Request $request)
    {
        $post = $request->all();

        $job = $this->jobRepository->find($post['job_id']);

        foreach($job->traitsSelection as $selection) {
            $selection->delete();
        }

        foreach($post['traits'] as $trait_id => $value) {

            $data = [
                'job_id' => $job->id,
                'trait_id' => $trait_id,
                'employer_standard_score' => $value
            ];

            $this->jobService->createJobCandidateTraitSelection($data);
        }

        $skills = (!empty($job->skills) && !is_null($job->skills)) ? explode(',', $job->skills) : [];

        return view('backend.employer.jobs.details', compact('job', 'skills'));
    }
}
