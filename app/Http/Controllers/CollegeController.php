<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\College;

class CollegeController extends Controller
{

    public function index()
    {
    	$colleges = College::withTrashed()->get();
    	return view('backend.admin.colleges.index', compact('colleges'));
    }

    public function create()
    {
    	return view('backend.admin.colleges.create');
    }

    public function store(Request $request)
    {
       $colleges = College::create([
            'college' => $request->input('college')
        ]);
       return redirect()->back()->with('success', 'College created!');
    }

    public function edit($id)
    {
        $colleges = College::findOrFail($id);
        return view('backend.admin.colleges.update', compact('colleges'));
    }  

    public function update(Request $request, $id)
    {
        $colleges = College::findOrFail($id);
        $colleges->college = $request->input('college');
        $colleges->save();

        return redirect()->back()->with('success', 'College Successfully Updated!');
    }

    public function delete($id)
    {
        $colleges = College::findOrFail($id);
        return view('backend.admin.colleges.destroy', compact('colleges'));
    }

    public function destroy($id)
    {
        $colleges = College::findOrFail($id);
        $colleges->delete();

        return redirect()->back()->with('success', 'College Successfully Deleted and moved to trash!');
    }

    public function forceDestroy($id)
    {
        $colleges = College::findOrFail($id);
        $colleges->forceDelete();

        return redirect()->back()->with('success', 'College Successfully Deleted!');
    }

    public function restore($id)
    {
        $colleges = College::withTrashed()
            ->where('id', $id)
            ->restore();

        return redirect()->back()->with('success', 'College Successfully Restored!');
    }


}