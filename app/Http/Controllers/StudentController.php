<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\StudentInformation;
use App\Models\Industry;
use App\Models\College;
use App\Models\CollegeMajor;
use App\Models\User;
use App\Models\Assessment;
use App\Models\StudentAssessment;
use App\Models\SavedJob;
use App\Models\PersonalityType;
use Auth;
use Storage;

class StudentController extends Controller
{
    //
    public function editBasicInformation($id)
    {
        $user = User::findOrFail($id);
    	$student_information   = StudentInformation::findOrFail($user->basic_information->id);
        $colleges              = College::all();
        $majors                = CollegeMajor::all();

    	return view('backend.student.basic-information', compact('student_information','colleges', 'majors'));
    }

    public function updateBasicInformation(Request $request, $id)
    {
    	$this->validate($request, [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'college_id'    => 'required',
            'major_id'      => 'required'
        ]);

    	$user = User::findOrFail($id);
        $user->first_name      = $request->input('first_name');
        $user->last_name       = $request->input('last_name');
        $user->middle_name     = $request->input('middle_name');

        if($user->profile_progress == 0)
            $user->profile_progress = 1;

        $user->save();

        $student_information   = StudentInformation::findOrFail($user->basic_information->id);
        $student_information->address       = $request->input('address');
        $student_information->city          = $request->input('city');
        $student_information->state         = $request->input('state');
        $student_information->zip_code      = $request->input('zip_code');
        $student_information->phone_number  = $request->input('phone_number');
        $student_information->dob           = $request->input('dob');
        
        if($request->hasFile('prof_pic')):
            $file = $request->file('prof_pic');
            $file_name = time().'-'.$file->getClientOriginalName();
            Storage::disk('public')->put(
                'profile-pic/'.$file_name, 
                file_get_contents($file->getRealPath())
            );
            $student_information->prof_pic = $file_name;
        endif;

        $student_information->college_id    = $request->input('college_id');
        $student_information->major_id      = $request->input('major_id');
        $student_information->year          = $request->input('year');
        $student_information->gpa           = $request->input('gpa');
        $student_information->about         = $request->input('about');
        $student_information->experience    = $request->input('experience');
        $student_information->education     = $request->input('education');
        $student_information->skills        = $request->input('skills');
        
        if($request->hasFile('resume')):
            $file = $request->file('resume');
            $file_name = time().'-'.$file->getClientOriginalName();
            Storage::disk('public')->put(
                'docs/'.$file_name, 
                file_get_contents($file->getRealPath())
            );
            $student_information->resume = $file_name;
        endif;

        $student_information->save();

        if($user->profile_progress == 1)
            return redirect('student/assessment');

    	return redirect()->back()->with('success', 'Student Basic Information Updated!');
    }

    public function getAssessment()
    {
        $assessments = Assessment::all();

        return view('backend.student.assessment.take-assessment', compact('assessments'));
    }

    public function postAssessment(Request $request)
    {
        $user = Auth::user();
        $assessments = $request->input('assessment');

        foreach ($assessments as $key => $srs_value) {

            $assessment = Assessment::find($key);
            $attribute_diff         = $srs_value - $assessment->mean;
            $attribute_t_value      = $attribute_diff / $assessment->stdev;
            $standard_t_value       = $attribute_t_value * 10;
            $student_standard_score = $standard_t_value + 50;
            $FinalSSS = (($student_standard_score - 20) / (80-20) ) * 100;

            $student_assessment = StudentAssessment::whereUserId($user->id)->whereAssessmentId($key)->first();
            if( empty($student_assessment) )
                $student_assessment = new StudentAssessment;
            
            $student_assessment->user_id = $user->id;
            $student_assessment->assessment_id = $key;
            $student_assessment->student_raw_score = $srs_value;
            $student_assessment->student_standard_score = $student_standard_score;
            $student_assessment->student_final_score = $FinalSSS;
            $student_assessment->save();
        }

        if($user->profile_progress == 1){
            $user->profile_progress = 2;
            $user->save();
        }

        return view('backend.student.assessment.finish');
    }

    public function saveJob(Request $request, $id)
    {
        $user = Auth::user();

        $job = SavedJob::whereJobId($id)->whereUserId($user->id)->first();
        if( empty($job) ){
            
            if ($user->cannot('savejob', $user))
                return 'max';

            $job = new SavedJob;
            $save_job = new SavedJob;
            $save_job->user_id = $user->id;
            $save_job->job_id = $id;
            $save_job->save();

            return 'saved';
        }
        else {
            $job->delete();
            return 'deleted';
        }

        
    }

    public function dontConfirmJobApplication(Request $request)
    {
        $user = Auth::user();
        $student_information   = StudentInformation::findOrFail($user->basic_information->id);
        $student_information->confirm_on_apply = 0;
        $student_information->save();
    }

    public function results()
    {
        $user = Auth::user();

        if( $user->assessments()->count() <1 )
            return redirect('student/assessment')->with('message', 'You haven\'t took the assessment yet! Start taking the assessment now.');

        $personality_type = $user->personalityType();
        return view('backend.student.results.results', compact('personality_type','user'));
    }
}
