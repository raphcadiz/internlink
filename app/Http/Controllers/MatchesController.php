<?php

namespace App\Http\Controllers;

use App\Models\Career;
use App\Models\Industry;
use App\Services\MatchesService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Job;
use App\Models\JobApplicant;
use App\Mailers\AppMailer;
use Auth;

class MatchesController extends Controller
{

	protected $matchesService;
	public function __construct()
	{
		$this->matchesService = new MatchesService();
	}

	public function matches()
    {
        if( Auth::user()->assessments()->count() <1 ) {
			return redirect('student/assessment')->with('message', 'You haven\'t took the assessment yet! Start taking the assessment now.');
		}

    	$jobs = Job::whereStatus(1)->orderBy('created_at', 'DESC')->paginate(16);

		$jobsArr =[];

		foreach($jobs as $job) {
			$jobsArr[] = [
				'job_id' => $job->id,
				'title' => $job->title,
				'company' => $job->user->basic_information->company_name,
				'city' => !empty($job->user->basic_information->city) ? '- '.$job->user->basic_information->city : '',
				'state' => !empty($job->user->basic_information->state) ? ', '.$job->user->basic_information->state : '',
				'career' => $job->career->title,
				'compatibility' => $job->getStudentCompatibilityPercentage(Auth::user())
			];
		}

		foreach ($jobsArr as $key => $row) {
			$compatibility[$key]  = $row['compatibility'];
		}

		$careers = Career::orderBy('title')->get();
		$industries = Industry::orderBy('industry')->get();

    	return view('backend.student.matches.index', compact('jobsArr', 'careers', 'industries'));
    }

    public function getApplyJob($id)
    {
    	$job = Job::findOrFail($id);

		if(!$job->studentViews()->where('student_id', Auth::user()->id)->first()) {
			$this->matchesService->addJobView($job->id, Auth::user()->id);
		}

    	return view('backend.student.matches.apply-job', compact('job'));
    }

    public function postApplyJob(Request $request, AppMailer $mailer, $id)
    {
    	$user = Auth::user();
    	$job_applicant = new JobApplicant;
    	$job_applicant->job_id = $id;
    	$job_applicant->user_id = $user->id;
    	$job_applicant->save();

        $job = Job::find($id);
        $mailer->sendJobApplicantionNotification($user, $job);

    	return 'success';
    }

	public function getCareersByIndustry($id)
	{
		$industry = Industry::findOrFail($id);

		$careers = $industry->careers()->orderBy('title')->get();

		return view('backend.student.matches.partials.careers-dropdown', compact('careers'));
	}

	public function postFilterMatches(Request $request)
	{
		$data = $request->all();

		$matches = $this->matchesService->getMatches($data);
		$user = Auth::user();

		return view('backend.student.matches.partials.matches-list', compact('matches', 'user'));
	}
}
