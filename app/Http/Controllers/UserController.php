<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\EmployerInformation;
use App\Models\StudentInformation;
use App\Mailers\AppMailer;
use App\Models\User;
use App\Models\Role;
use App\Models\Notification;
use App\Models\UserNotification;
use Auth;
use Gate;
use App\Models\Job;
use App\Models\PlanPayment;
use DB;

class UserController extends Controller
{
    public function index()
    {
    	$users = User::withTrashed()->get();

    	return view('backend.admin.users.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('backend.admin.users.create', compact('roles'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        return view('backend.admin.users.update', compact('user','roles'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required|confirmed'
        ]);

        $user = User::findOrFail($id);
        $user->name = $request->input('name');
        $user->password = bcrypt($request->input('password'));
        $user->save();

        return redirect()->back()->with('success', 'User Successfully Updated!');
    }

    public function store(Request $request, AppMailer $mailer)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        $password = $request->input('password');
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'role_id' => $request->input('role_id'),
        ]);

        $mailer->newUserByAdminConfirmation($user, $password);

        return redirect()->back()->with('success', 'User created! User need to confirm this to his/her email');
    }

    public function deactivate($id = null)
    {
        if( empty($id) )
            $user = Auth::user();
        else
            $user = User::findOrFail($id);

        return view('backend.profile.deactivate', compact('user'));
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        return view('backend.admin.users.destroy', compact('user'));
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
    	$user->delete();

        if( Auth::user()->id == $user->id ){
            Auth::logout();
            return redirect('login')->with('success', 'Your account is now deacitivated!');
        }
        else
            return redirect()->back()->with('success', 'User Successfully Deleted and moved to trash!');
    }

    public function forceDestroy($id)
    {
        $user = User::findOrFail($id);

        if($user->hasRole('Employer')) {
            EmployerInformation::where('user_id',$id)->delete();
        }
        if($user->hasRole('Student')) {
            StudentInformation::where('user_id',$id)->delete();
        }

        $user->forceDelete();

        return redirect()->back()->with('success', 'User Successfully Deleted!');
    }

    public function restore($id)
    {
        $user = User::withTrashed()
            ->where('id', $id)
            ->restore();

        return redirect()->back()->with('success', 'User Successfully Restored!');
    }

    public function profile()
    {
        $user = Auth::user();

        if( $user->hasRole('Student') ){
            if( empty($user->basic_information->college_id) || empty($user->basic_information->major_id) )
                return redirect('student/edit-basic-information/'.$user->id);
            
            return view('backend.profile.student', compact('user'));
        }
        else if( $user->hasRole('Employer') )
            return view('backend.profile.employer', compact('user'));
        else
            abort(404);
    }

    public function pricing()
    {
        $user = Auth::user();

        if( $user->hasRole('Student') )
            return view('backend.pricing.index');
        else if( $user->hasRole('Employer') )
            return view('backend.pricing.employer-plans');
        else
            abort(404);
    }

    public function updatePassword( $id = null )
    {
        if( empty($id) )
            $user = Auth::user();
        else
            $user = User::findOrFail($id);

        if ($user->cannot('update', $user))
            abort(403, 'Unauthorized');

        return view('backend.profile.password-update', compact('user'));
    }

    public function savePassword(Request $request, $id = null )
    {
        $this->validate($request, [
            'current_password' => 'required',
            'password' => 'required|confirmed'
        ]);

        $credentials = ['password' => $request->input('current_password')];

        if (!Auth::validate($credentials)) {
            return redirect()->back()->withError('Invalid Old Password');
        }

        if( empty($id) )
            $user = Auth::user();
        else
            $user = User::findOrFail($id);

        $user->password = bcrypt($request->input('password'));
        $user->save();

        return view('backend.layouts.success', 
                    [ 'data' => array(
                            'title' => 'Change Password',
                            'message' => 'Password Successfully Changed!',
                            'redirect_url' => url('student/more')
                        )]
                );
    }

    public function updateEmail( $id = null )
    {
        if( empty($id) )
            $user = Auth::user();
        else
            $user = User::findOrFail($id);

        if ($user->cannot('update', $user))
            abort(403, 'Unauthorized');


        return view('backend.profile.email-update', compact('user'));

    }

    public function saveEmail(Request $request, $id = null )
    {
        $this->validate($request, [
            'current_email' => 'required|email',
            'password'      => 'required',
            'email'     => 'required|email|unique:users',
        ]);

        $credentials = [
            'email' =>  $request->input('current_email'), 
            'password' => $request->input('password')
        ];

        if (!Auth::validate($credentials)) {
            return redirect()->back()->withError('Invalid Credentials');
        }

        if( empty($id) )
            $user = Auth::user();
        else
            $user = User::findOrFail($id);

        $user->email = $request->input('email');
        $user->save();

        return view('backend.layouts.success', 
                    [ 'data' => array(
                            'title' => 'Change Email',
                            'message' => 'Email Successfully Changed!',
                            'redirect_url' => url('student/more')
                        )]
                );
    }

    public function paymentHistory( $id = null )
    {
        if( empty($id) )
            $user = Auth::user();
        else
            $user = User::findOrFail($id);

        $payments = array();
        if( !empty($user->stripe_id) ):
            $invoices = $user->invoices();
            foreach ($invoices as $invoice) {
                $plan_subscription = PlanPayment::whereStripeId($invoice->subscription)->first();
                if($plan_subscription){
                    array_push($payments, array(
                        'type'          => 'subscription_payment',
                        'payment_id'    => $plan_subscription->id,
                        'payment_n'     => $plan_subscription->user->plan,
                        'created_at'    => $plan_subscription->created_at,
                        'amount'        => $invoice->total()
                    ));
                } else {
                    array_push($payments, array(
                        'type'          => 'subscription_auto_payment',
                        'payment_id'    => $invoice->id,
                        'payment_n'     => 'Auto Charge',
                        'created_at'    => $invoice->date(),
                        'amount'        => $invoice->total()
                    ));
                }
            }
        endif;

        usort($payments, function ($a, $b) { return strnatcmp($b['created_at'], $a['created_at']); });

        return view('backend.profile.payment-history', compact('payments'));
    }

    public function stripeInvoice($invoice)
    {
        return Auth::user()->downloadInvoice($invoice, [
            'vendor'  => 'Internlogic',
            'product' => 'Premium Plan',
        ]);
    }

    public function printSubscriptionPayment($id)
    {
        $plan_payment = PlanPayment::findOrFail($id);
        return view('backend.pricing.payment-print', compact('plan_payment'));
    }

    public function getEmailSettings()
    {
        $user = Auth::user();
        $notifications = Notification::whereRoleId($user->role->id)->get();  
        
        return view('backend.profile.email-settings', compact('notifications', 'user'));
    }

    public function postEmailSettings(Request $request)
    {
        $user = Auth::user();
        DB::table('user_notifications')->where('user_id', '=', $user->id)->delete();
        $notifications = $request->input('notifications');
        foreach ($notifications as $notification) {
            $email_notif = new UserNotification(['notification_id' => $notification]);
            $user->notifications()->save($email_notif);
        }
       
        return redirect()->back()->with('success', 'Email Settings Updated!');
    }
}
