<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Plan;
class PlansController extends Controller
{
    public function index()
    {
    	$plans = Plan::withTrashed()->get();
    	return view('backend.admin.plans.index', compact('plans'));
    }

    public function create()
    {
    	return view('backend.admin.plans.create');
    }

    public function store(Request $request)
    {
       $plans = Plan::create([
            'name' => $request->input('name'),
            'alias' => $request->input('description'),
            'price' => $request->input('price'),
        ]);
       return redirect()->back()->with('success', 'Plan created!');
    }

    public function edit($id)
    {
        $plans = Plan::findOrFail($id);
        return view('backend.admin.plans.update', compact('plans'));
    }  

    public function update(Request $request, $id)
    {
        $plan = Plan::findOrFail($id);
        $plan->name = $request->input('name');
        $plan->alias = $request->input('description');
        $plan->price = $request->input('price');
        $plan->save();

        return redirect()->back()->with('success', 'Plan Successfully Updated!');
    }

    public function delete($id)
    {
        $plans = Plan::findOrFail($id);
        return view('backend.admin.plans.destroy', compact('plans'));
    }

    public function destroy($id)
    {
        $plans = Plan::findOrFail($id);
        $plans->delete();

        return redirect()->back()->with('success', 'Plan Successfully Deleted and moved to trash!');
    }

    public function forceDestroy($id)
    {
        $plans = Plan::findOrFail($id);
        $plans->forceDelete();

        return redirect()->back()->with('success', 'Plan Successfully Deleted!');
    }

    public function restore($id)
    {
        $plans = Plan::withTrashed()
            ->where('id', $id)
            ->restore();

        return redirect()->back()->with('success', 'Plan Successfully Restored!');
    }
}