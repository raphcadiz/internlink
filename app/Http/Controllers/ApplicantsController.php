<?php

namespace App\Http\Controllers;

use App\Mailers\AppMailer;
use App\Models\College;
use App\Models\CollegeMajor;
use App\Models\JobCandidate;
use App\Repositories\CollegeRepository;
use App\Repositories\JobApplicantRepository;
use App\Repositories\JobCandidateRepository;
use App\Services\ApplicantsService;
use App\Services\JobApplicantService;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Models\Job;
use Mockery\CountValidator\Exception;

class ApplicantsController extends Controller
{
    protected $jobApplicantRepository;
    protected $jobCandidateRepository;
    protected $jobApplicantService;
    protected $collegeRepository;
    protected $applicantsService;

    public function __construct()
    {
        $this->jobApplicantRepository = new JobApplicantRepository();
        $this->jobApplicantService = new JobApplicantService();

        $this->jobCandidateRepository = new JobCandidateRepository();
        $this->collegeRepository = new CollegeRepository();

        $this->applicantsService = new ApplicantsService();
    }

    public function getView()
    {
        $applicants = $this->jobApplicantRepository->getAllApplicantsByUser(Auth::user()->id);
        $candidates = $this->jobCandidateRepository->getAllCandidatesByUser(Auth::user()->id);

        $jobs = Auth::user()->jobs()->orderBy('title')->get();
        $colleges = College::orderBy('college')->lists('college', 'id');
        $majors = CollegeMajor::orderBy('major')->lists('major', 'id');

        return view('backend.employer.applicants.list', compact('applicants','candidates', 'jobs', 'colleges', 'majors'));
    }

    public function getInviteCandidate($job_id, $user_id)
    {
        $job = Job::findOrFail($job_id);

        $user = User::findOrFail($user_id);

        return view('backend.employer.applicants.modals.invite-candidate', compact('job', 'user'));
    }

    public function postInviteCandidate(Request $request, AppMailer $mailer)
    {
        try {

            $posts = $request->all();

            $candidate = new JobCandidate();
            $candidate->job_id = $posts['job_id'];
            $candidate->user_id = $posts['user_id'];
            $candidate->save();

            $job = $candidate->job()->first();
            $user = $candidate->user()->first();

            $mailer->sendCandidateInviteNotification($user, $job);

            return response()->json(['message' => 'Candidate successfully invited.'], 200);
        } catch(Exception $e) {

            return response()->json(['message' => 'Failed to invite candidate.'], 500);
        }


    }

    public function showApplicant($job_id, $student_id)
    {
        $job = Job::find($job_id);
        $user = User::find($student_id);

        $invited = $this->applicantsService->checkIfStudentIsInvited($job_id, $student_id);
        $status = $this->applicantsService->getApplicantStatus($job_id, $student_id);

        $this->applicantsService->createViewedStudentRecord($job->user_id, $student_id);

        return view('backend.employer.applicants.show-applicant', compact('job','user', 'invited', 'status'));
    }

    public function showCandidate($job_id, $student_id)
    {
        $job = Job::find($job_id);
        $user = User::find($student_id);
        return view('backend.employer.applicants.show-candidate', compact('job','user'));
    }

    public function postFilterApplicants(Request $request)
    {
        $data = $request->all();

        $applicants = $this->applicantsService->getApplicantsBaseOnFilter($data);

        return view('backend.employer.applicants.partials.applicants-list', compact('applicants'));
    }

    public function postFilterCandidates(Request $request)
    {
        $data = $request->all();

        $candidates = $this->jobApplicantService->getCandidatesBaseOnFilter($data);

        return view('backend.employer.applicants.partials.candidates-list', compact('candidates'));
    }
}
