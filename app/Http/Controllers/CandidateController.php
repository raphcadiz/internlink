<?php

namespace App\Http\Controllers;

use App\Models\CandidateTraitSelection;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Models\Job;
use App\Models\CandidateTraits;
use App\Models\Industry;

class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $job = Job::findOrFail($id);
        $traits = CandidateTraits::all();
        return view('backend.employer.candidates.trait-selection', compact('traits', 'job'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $job = Job::findOrFail($request->input('job_id'));

        foreach($request->input('traits') as $trait_id => $value) {
            $traitSelection = new CandidateTraitSelection();
            $traitSelection->job_id = $job->id;
            $traitSelection->trait_id = $trait_id;
            $traitSelection->employer_standard_score = $value;
            $traitSelection->save();
        }

        if(!Auth::user()->isPremiumEmployer()) {
            return redirect("employer/subscription-pricing");
        }

        return view('backend.layouts.success',
            [ 'data' => array(
                'title' => 'Job Post Creation',
                'message' => 'Job post successfully created!',
                'sub_message' => '',
                'redirect_url' => url( 'employer/applicants')
            )]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function postForwardToHiringManager(Request $request)
    {
        // TODO: forward to hiring manager process
        $message = 'You will be notified when the Candidate Trait Selection has been filled out. You can also check the status on your POSTINGS page as well.';
        return response()->json(['message' => $message]);
    }
}
