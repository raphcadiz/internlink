<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Job;
use CountryState;
use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Customer;
use App\Models\Plan;
use App\Models\PlanPayment;
use Stripe\Coupon;
use Barryvdh\DomPDF\Facade as PDF;
use Auth;

class SubscriptionController extends Controller
{
    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    public function getPaymentInformation(Request $request)
    {
        // $job = Job::findOrFail($id);
        $plan = Plan::whereName($request->get('plan_name'))->firstOrFail();
        $user = Auth::user();

        $countries = CountryState::getCountries();
        $states = CountryState::getStates('US');

        return view('backend.pricing.payment-information', compact('plan', 'countries', 'states', 'user'));
    }

    public function postPaymentInformation(Request $request)
    {
        $plan = Plan::findOrFail($request->input('plan_id'));

        $user = Auth::user();
        $plan_payment = new PlanPayment;
        if( $request->input('stripeToken') ){

            try {
                if( $user->trial_consumed() ){
                    if( $request->input('use_promo_code') == 1 ){
                        $coupon = $this->retrieveCoupon(trim($request->input('promo_code')));
                        if($coupon->valid){
                            $response = $user->newSubscription( $plan->name,  $plan->name )
                                ->withCoupon(trim($request->input('promo_code')))
                                ->create($request->input('stripeToken'));
                            $plan_payment->amount = $this->calculatePaymentWithCoupon($coupon, $plan->price);
                        } else {
                            $response = $user->newSubscription( $plan->name,  $plan->name )
                                ->create($request->input('stripeToken'));
                            $plan_payment->amount = $plan->price;
                        }                    
                    } else {
                        $response = $user->newSubscription( $plan->name,  $plan->name )
                            ->create($request->input('stripeToken'));
                        $plan_payment->amount = $plan->price;
                    }
                } else {
                     $response = $user->newSubscription( $plan->name,  $plan->name )
                        ->trialDays(30)
                        ->create($request->input('stripeToken'));
                    $plan_payment->amount = 0;
                    $user->trial = 1;
                }

                $plan_payment->user_id   = $user->id;
                $plan_payment->stripe_id = $response->stripe_id;
                $plan_payment->address_1 = $request->input('address_line1');
                $plan_payment->address_2 = $request->input('address_line2');
                $plan_payment->city      = $request->input('address_city');
                $plan_payment->state     = $request->input('address_state');
                $plan_payment->zip_code  = $request->input('address_zip');
                $plan_payment->country   = $request->input('address_country');
                $plan_payment->save();

                $user->plan = $plan->name;
                $user->save();
                return view('backend.pricing.payment-success', compact('plan_payment'));
            } catch (Exception $e) {
                return redirect()->back()->with('message', 'Something went wrong. Try again later.');
            }
            
        }
        else {
            return redirect()->back()->withError('Stripe Token Missing');
        }
        
    }

    public function cancelSubscription( $user = null )
    {
        if( empty($id) )
            $user = Auth::user();
        else
            $user = User::findOrFail($id);


        $user->subscription($user->plan)->cancel();

        return view('backend.layouts.success', 
                    [ 'data' => array(
                            'title' => 'Cancel Subscription',
                            'message' => 'Subscription Successfully Cancelled!',
                            'sub_message' => 'You can use premium features until grace period ends',
                            'redirect_url' => url( strtolower($user->role->role).'/subscription-pricing')
                        )]
                );
    }

    public function resumeSubscription( $user = null )
    {
        if( empty($id) )
            $user = Auth::user();
        else
            $user = User::findOrFail($id);


        $user->subscription($user->plan)->resume();

        return view('backend.layouts.success', 
                    [ 'data' => array(
                            'title' => 'Resume Subscription',
                            'message' => 'Subscription Successfully Resumed!',
                            'redirect_url' => url( strtolower($user->role->role).'/subscription-pricing')
                        )]
                );
    }

    public function showSubscriptionReceipt($id)
    {
        $plan_payment = PlanPayment::findOrFail($id);
        return view('backend.pricing.payment-success', compact('plan_payment'));
    }

    private function calculatePaymentWithCoupon($coupon, $payment){
        $finalpayment = $payment;

        if( $coupon->percent_off ){
            $finalpayment = $payment - ($coupon->percent_off / 100);
        }

        if( $coupon->amount_off ){
            $finalpayment = $payment - ($coupon->amount_off / 100);
        }

        return $finalpayment;
    }

    public function retrieveCoupon($code)
    {
        try {
            $coupon = Coupon::retrieve( trim($code) );
            return $coupon;
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }
    }

    public function postValidatePromoCode(Request $request)
    {
        if (strlen(trim($request->input('promo_code'))) > 0) {
            $coupon = $this->retrieveCoupon($request->input('promo_code'));

            if($coupon->valid)
                return response()->json(['valid_promo' => true]);
            else
                return response()->json(['valid_promo' => false]);
        }
    }
}
