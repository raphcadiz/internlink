<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\MessengerService;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Models\Job;

class MessagesController extends Controller
{
    protected $messengerService;

    public function __construct()
    {
        $this->messengerService = new MessengerService();
    }

    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index()
    {
        $currentUserId = Auth::user()->id;
        $latestThread = Thread::forUser($currentUserId)->latest('updated_at')->first();

        if($latestThread) {
            return redirect('messages/'.$latestThread->id);
        }

        $threads = [];
        $currentUser = Auth::user();
        $currentThread = [];
        $type = 'view-conversation';
        return view('backend.messenger.index', compact('threads', 'currentUser', 'currentThread', 'type'));


    }

    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $currentThread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            return redirect('messages');
        }
        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();
        // don't show the current user in list
        $userId = Auth::user()->id;
        $currentThread->markAsRead($userId);

        $threads = Thread::forUser($userId)->latest('updated_at')->get();

        $currentUser = Auth::user();
        $type = 'open-conversation';

        $job = Job::findOrFail($currentThread->job_id);
        $applicant = User::findOrFail($currentThread->participants()->where('user_id', '!=', Auth::user()->id)->first()->user_id);

        return view('backend.messenger.index', compact('threads', 'currentUser', 'currentThread', 'type', 'applicant', 'job'));
    }
    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create($job_id, $user_id)
    {
        $job = Job::findOrFail($job_id);
        $applicant = User::findOrFail($user_id);

        $thread = $this->messengerService->getThreadByJobAndApplicant($job_id, $user_id);

        if($thread) {
            return redirect('messages/'.$thread->thread_id);
        }

        $currentUser = Auth::user();
        $threads = Thread::forUser($currentUser->id)->latest('updated_at')->get();
        $currentThread = [];
        $type = 'new-message';
        return view('backend.messenger.index', compact('threads', 'currentUser', 'currentThread', 'type', 'applicant', 'job'));
    }
    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store()
    {
        $input = Input::all();

        $thread = new Thread;
        $thread->job_id = $input['job_id'];
        $thread->subject = $input['subject'];
        $thread->save();

        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'body'      => $input['message'],
            ]
        );

        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'last_read' => new Carbon,
            ]
        );

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant($input['recipients']);
        }

        return redirect('messages');
    }
    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            return redirect('messages');
        }
        $thread->activateAllParticipants();
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::id(),
                'body'      => Input::get('message'),
            ]
        );
        // Add replier as a participant
        $participant = Participant::firstOrCreate(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant(Input::get('recipients'));
        }
        return redirect('messages/' . $id);
    }

    public function search()
    {
        $keyword = Input::get('keyword');

        $threads = $this->messengerService->searchConversation($keyword, Auth::user()->id);

        try {
            $currentThread = $threads->first();
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread was not found.');
            return redirect('messages');
        }
        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();
        // don't show the current user in list
        $userId = Auth::user()->id;
        $currentThread->markAsRead($userId);

        $threads = $this->messengerService->searchConversation($keyword, Auth::user()->id)->get();

        $currentUser = Auth::user();
        $type = 'open-conversation';

        $job = Job::findOrFail($currentThread->job_id);
        $applicant = User::findOrFail($currentThread->participants()->where('user_id', '!=', Auth::user()->id)->first()->user_id);

        return view('backend.messenger.index', compact('threads', 'currentUser', 'currentThread', 'type', 'applicant', 'job', 'keyword'));

    }

    public function checkNewMessages()
    {
        $thread_id = Input::get('thread_id');
        $keyword = Input::get('keyword');
        $current_thread_messages_count = Input::get('current_thread_messages_count');
        $current_url = Input::get('current_url');
        $current_user_id = Auth::user()->id;

        if(empty($thread_id)) {
            $currentThread = Thread::forUser($current_user_id)->latest('updated_at')->first();
        } else {
            $currentThread = Thread::findOrFail($thread_id);
        }

        $conversation_content = '';
        $refresh_view = false;

        $threads = Thread::forUser($current_user_id)->latest('updated_at')->get();

        $currentUser = Auth::user();
        $type = 'open-conversation';

        if(strpos($current_url, 'messages/new') !== false) {
            $currentThread = [];
            $url_segments = explode('/', $current_url);
            $job = Job::findOrFail($url_segments[5]);
            $applicant = User::findOrFail($url_segments[6]);

            $type = 'new-message';
            $conversation_content = view('backend.messenger.partials.conversation-content', compact('threads', 'currentUser', 'currentThread', 'type', 'applicant', 'job'))->render();

        } else if($currentThread->messages()->count() > $current_thread_messages_count) {
            $job = Job::findOrFail($currentThread->job_id);
            $applicant = User::findOrFail($currentThread->participants()->where('user_id', '!=', Auth::user()->id)->first()->user_id);
            $currentThread->markAsRead($current_user_id);
            $conversation_content = view('backend.messenger.partials.conversation-content', compact('threads', 'currentUser', 'currentThread', 'type', 'applicant', 'job'))->render();
            $refresh_view = true;
        }


        $conversation_list = view('backend.messenger.partials.conversation-list', compact('threads', 'currentUser', 'currentThread', 'type', 'applicant', 'job'))->render();

        return response()->json([
            'conversation_list' => $conversation_list,
            'conversation_content' => $conversation_content,
            'refresh_view' => $refresh_view
        ]);

    }

}