<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Mailers\AppMailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\EmployerInformation;
use App\Models\StudentInformation;

class RegistrationController extends Controller
{
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */

    public function getRegister()
    {
        if (property_exists($this, 'registerView')) {
            return view($this->registerView);
        }

        if(\Request::is('register/student')){
            $role = Role::where('role', '=', 'Student')->first();
            return view('auth.register-student', compact('role'));
        }

        if(\Request::is('register/employer')){
            $role = Role::where('role', '=', 'Employer')->first();
            return view('auth.register-employer', compact('role'));
        }

        $roles = Role::where('role', '!=', 'Administrator')->get();

        return view('auth.register', compact('roles'));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request, AppMailer $mailer)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ]);

        $user = User::create([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'role_id' => $request->input('role_id'),
        ]);

        $mailer->sendEmailConfirmationTo($user);

        return redirect()->back()->with('message', 'Please confirm your registration through your email');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return string|null
     */
    protected function getGuard()
    {
        return property_exists($this, 'guard') ? $this->guard : null;
    }

    public function confirmRegistration($token)
    {
        $user = User::whereActivationToken($token)->firstOrFail();

        if($user->hasRole('Employer')){
            $employer_info = new EmployerInformation;
            $employer_info->user_id = $user->id;
            $employer_info->save();
        }

        if($user->hasRole('Student')){
            $student_info = new StudentInformation;
            $student_info->user_id = $user->id;
            $student_info->save();
        }

        $user->confirmEmail();

        return redirect('login')->with('message', 'Verified! Try to login Again.');
    }
}
