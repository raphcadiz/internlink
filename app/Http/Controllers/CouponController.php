<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class CouponController extends Controller
{
    public function index()
    {
    	$coupons = array();
    	return view('backend.admin.coupons.index', compact('coupons'));
    }

    public function create()
    {
    	return view('backend.admin.coupons.create');
    }
}
