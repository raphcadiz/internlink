<?php

namespace App\Http\Controllers;

use App\Models\BusinessSize;
use App\Models\EmployerType;
use Illuminate\Http\Request;
use CountryState;
use App\Http\Requests;
use App\Models\EmployerInformation;
use App\Models\Industry;
use App\Models\User;
use Storage;

class EmployerController extends Controller
{
    //
    public function editBasicInformation($id)
    {
        $user = User::findOrFail($id);
    	$employer_information  = EmployerInformation::findOrFail($user->basic_information->id);
        $industries = Industry::all();
		$businessSizes = BusinessSize::all();
		$employerTypes = EmployerType::all();
		$states = CountryState::getStates('US');

    	return view('backend.employer.basic-information', compact('employer_information','industries', 'businessSizes', 'employerTypes', 'states'));
    }

    public function updateBasicInformation(Request $request, $id)
    {
    	$this->validate($request, [
            'company_name' => 'required',
        ]);

        $user = User::findOrFail($id);
        $user->profile_progress = 1;
        $user->save();
        
    	$employer_information = EmployerInformation::findOrFail($user->basic_information->id);
    	$employer_information->company_name		=	$request->input('company_name');
		$employer_information->industry_id	    =	$request->input('industry_id');
		$employer_information->business_size_id	    =	$request->input('business_size_id');
		$employer_information->employer_type_id	    =	$request->input('employer_type_id');
		$employer_information->phone_number		=	$request->input('phone_number');
		$employer_information->address 			=	$request->input('address');
		$employer_information->city 			=	$request->input('city');
		$employer_information->state 			=	$request->input('states');
		$employer_information->zip_code 		=	$request->input('zip_code');

        if($request->hasFile('company_logo')):
            $file = $request->file('company_logo');
            $file_name = time().'-'.$file->getClientOriginalName();
            Storage::disk('public')->put(
                'profile-pic/'.$file_name, 
                file_get_contents($file->getRealPath())
            );
            $employer_information->company_logo = $file_name;
        endif;

		$employer_information->save();

    	return redirect()->back()->with('success', 'Employer Basic Information Updated!');
    }
}
