<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',  function () {
	return view('frontend.employer');
});

// Student FrontEnd
Route::get('/student',  function () {
	return view('frontend.student');
});

Route::get('/student',  function () {
	return view('frontend.student');
});

Route::get('/student/faq',  function () {
	return view('frontend.student-faq');
});

Route::get('/faq',  function () {
	return view('frontend.faq');
});

Route::get('/privacy',  function () {
	return view('frontend.privacy');
});

Route::get('/terms-of-service',  function () {
	return view('frontend.terms');
});

Route::get('/contact',  function () {
	return view('frontend.contact');
});

Route::get('/team',  function () {
	return view('frontend.team');
});

Route::get('/brands',  function () {
	return view('frontend.brands');
});

// Employer FrontEnd
Route::get('/employer',  function () {
	return view('frontend.employer');
});



Route::get('/dashboard', function(){
	if(Auth::user()->hasRole('Student')){
        return redirect('student/matches');
    }
    elseif(Auth::user()->hasRole('Employer')){
        return redirect('employer/postings');
    }
    elseif(Auth::user()->hasRole('Administrator')){
        return redirect('administrator/dashboard');
    }
    else {
        $this->redirectTo = '/';
    }
});

Route::get('register/employer', 'RegistrationController@getRegister');
Route::get('register/student', 'RegistrationController@getRegister');
Route::get('register', 'RegistrationController@getRegister');
Route::post('register', 'RegistrationController@postRegister');
Route::get('register/confirm/{token}', 'RegistrationController@confirmRegistration');

Route::get('login/employer', 'SessionsController@login');
Route::get('login/student', 'SessionsController@login');
Route::get('login', 'SessionsController@login');
Route::post('login', 'SessionsController@postLogin');
Route::get('logout', 'SessionsController@logout');

Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/reset', 'Auth\PasswordController@reset');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');

/*
 * Employer routes protected by auth
 * and employer middleware
*/
Route::group(['middleware' => ['auth', 'employer'], ['prefix' => 'employer']],function(){
	Route::group(['prefix' => 'employer'], function () {
		Route::get('/dashboard', function () {
		    return redirect('employer/postings');
		});

		Route::get('profile', 'UserController@profile');

		Route::get('edit-basic-information/{id}', 'EmployerController@editBasicInformation');
		Route::post('edit-basic-information/{id}', 'EmployerController@updateBasicInformation');

		Route::get('postings', 'JobController@index');
		Route::get('postings/add-new', 'JobController@create');
		Route::post('postings/add-new', 'JobController@store');

		Route::get('candidate/trait-selection/{id}', 'CandidateController@create');
		Route::post('candidate/save-trait-selection', 'CandidateController@store');
		Route::post('candidate/forward-to-hiring-manager', 'CandidateController@postForwardToHiringManager');

		Route::get('subscription-pricing', 'UserController@pricing');
		Route::get('update-password/{id?}', 'UserController@updatePassword');
		Route::get('update-email/{id?}', 'UserController@updateEmail');
		Route::get('deactivate/{id?}', 'UserController@deactivate');
		Route::get('payment-history/{id?}', 'UserController@paymentHistory');
		Route::get('email-settings', 'UserController@getEmailSettings');
		Route::post('email-settings', 'UserController@postEmailSettings');

		Route::get('pricing/{id}', 'PricingController@create');
		Route::get('pricing-more-info', 'PricingController@getMoreInfo');
		Route::get('payment-info/{id}/{type}', 'PricingController@getPaymentInfo');
		Route::post('payment-info/{id}', 'PricingController@postPayment');
		Route::get('payment-confirmation/{id}', 'PricingController@getPaymentConfirmation');

		Route::get('print-payment-confirmation/{id}', 'PricingController@getPrintOrderSummary');

		Route::get('posting-info/{id}', 'JobController@getJobDetails');
		Route::get('posting/change-status/{id}/{status_id}', 'JobController@getChangeStatus');
		Route::post('posting/change-status', 'JobController@postChangeStatus');

		Route::get('new-cc-form', 'PricingController@getNewCreditCardForm');
		Route::post('payment/validate-promo-code', 'PricingController@postValidatePromoCode');

		Route::get('applicants', 'ApplicantsController@getView');
		Route::get('show-applicant/{job_id}/{user_id}', 'ApplicantsController@showApplicant');
		Route::get('show-candidate/{job_id}/{user_id}', 'ApplicantsController@showCandidate');
		Route::get('invite-candidate/{job_id}/{user_id}', 'ApplicantsController@getInviteCandidate');

		Route::post('invite-candidate', 'ApplicantsController@postInviteCandidate');
		Route::get('get-free-user-warning', 'JobController@getFreeUserWarning');

		Route::post('filter-applicants', 'ApplicantsController@postFilterApplicants');
		Route::post('filter-candidates', 'ApplicantsController@postFilterCandidates');

		Route::get('posting/update/{job_id}', 'JobController@getUpdateJob');
		Route::post('posting/update', 'JobController@postUpdateJob');

		Route::get('posting/update-trait-selection/{job_id}', 'JobController@getUpdateTraitSelection');
		Route::post('posting/update-trait-selection', 'JobController@postUpdateTraitSelection');

		Route::get('get-majors-by-college/{college_id}', 'ApplicantsController@getMajorsByCollege');
	});

});

/*
 * Student routes protected by auth
 * and student middleware
*/
Route::group(['middleware' => ['auth', 'student']],function(){
	Route::group(['prefix' => 'student'], function () {
		Route::get('/dashboard', function () {
		    return redirect('student/matches');
		});

		Route::get('profile', 'UserController@profile');

		Route::get('edit-basic-information/{id}', 'StudentController@editBasicInformation');
		Route::post('edit-basic-information/{id}', 'StudentController@updateBasicInformation');

		Route::get('subscription-pricing', 'UserController@pricing');
		Route::get('update-password/{id?}', 'UserController@updatePassword');
		Route::get('update-email/{id?}', 'UserController@updateEmail');
		Route::get('deactivate/{id?}', 'UserController@deactivate');
		Route::get('payment-history/{id?}', 'UserController@paymentHistory');
		Route::get('email-settings', 'UserController@getEmailSettings');
		Route::post('email-settings', 'UserController@postEmailSettings');

		Route::get('assessment', 'StudentController@getAssessment');
		Route::post('assessment', 'StudentController@postAssessment');

		Route::get('matches', 'MatchesController@matches');
		Route::get('apply-job/{id}', 'MatchesController@getApplyJob');
		Route::post('apply-job/{id}', 'MatchesController@postApplyJob');
		Route::post('dont-confirm-application', 'StudentController@dontConfirmJobApplication');
		Route::post('save-job/{id}', 'StudentController@saveJob');

		Route::get('results', 'StudentController@results');

		Route::get('get-free-user-warning', 'JobController@getFreeUserWarning');

		Route::post('filter-matches', 'MatchesController@postFilterMatches');
		Route::get('matches/get-careers-by-industry/{id}', 'MatchesController@getCareersByIndustry');
	});
});

/*
 * Administrator routes protected by auth
 * and admin middleware
*/
Route::group(['middleware' => ['auth', 'admin']],function(){
	Route::group(['prefix' => 'administrator'], function () {
		Route::get('/dashboard', function () {
		    return view('backend.dashboard');
		});

		Route::get('users', 'UserController@index');
		Route::get('users/add-new', 'UserController@create');
		Route::post('users/add-new', 'UserController@store');
		Route::get('users/update-user/{id}', 'UserController@edit');
		Route::post('users/update-user/{id}', 'UserController@update');
		Route::get('users/delete-user/{id}', 'UserController@delete');
		Route::get('users/delete-trash/{id}', 'UserController@destroy');
		Route::get('users/delete-permanently/{id}', 'UserController@forceDestroy');
		Route::get('user/restore/{id}', 'UserController@restore');


		// Business Routes
		Route::get('business', 'BusinessController@index');

		Route::get('industries', 'IndustryController@index');
		Route::get('delete-industry/{id}', 'IndustryController@destroy');

		// Route::get('careers', 'CareerController@index');
		// Route::get('delete-career/{id}', 'CareerController@destroy');

		Route::get('coupons', 'CouponController@index');
		Route::get('coupons/add-new', 'CouponController@create');



		Route::get('careers', 'CareerController@index');
		Route::get('delete-career/{id}', 'CareerController@destroy');


		// Plans route
		Route::get('plans', 'PlansController@index');
		Route::get('plans/add-new', 'PlansController@create');
		Route::post('plans/add-new', 'PlansController@store');
		Route::get('plans/update-plan/{id}', 'PlansController@edit');
		Route::post('plans/update-plan/{id}', 'PlansController@update');
		Route::get('plans/delete-plan/{id}', 'PlansController@delete');
		Route::get('plans/delete-trash/{id}', 'PlansController@destroy');
		Route::get('plans/delete-permanently/{id}', 'PlansController@forceDestroy');
		Route::get('plans/restore/{id}', 'PlansController@restore');

		// Student data route 

		//college
		Route::get('edit/college', 'CollegeController@index');
		Route::get('edit/college/add-new', 'CollegeController@create');
		Route::post('edit/college/add-new', 'CollegeController@store');
		Route::get('edit/college/update-college/{id}', 'CollegeController@edit');
		Route::post('edit/college/update-college/{id}', 'CollegeController@update');
		Route::get('edit/college/delete-college/{id}', 'CollegeController@delete');
		Route::get('edit/college/delete-trash/{id}', 'CollegeController@destroy');
		Route::get('edit/college/delete-permanently/{id}', 'CollegeController@forceDestroy');
		Route::get('edit/college/restore/{id}', 'CollegeController@restore');
		//career
		Route::get('edit/career', 'CareerController@index');
		Route::get('edit/career/add-new', 'CareerController@create');
		Route::post('edit/career/add-new', 'CareerController@store');
		Route::get('edit/career/update-career/{id}', 'CareerController@edit');
		Route::post('edit/career/update-career/{id}', 'CareerController@update');
		Route::get('edit/career/delete-career/{id}', 'CareerController@delete');
		Route::get('edit/career/delete-trash/{id}', 'CareerController@destroy');
		Route::get('edit/career/delete-permanently/{id}', 'CareerController@forceDestroy');
		Route::get('edit/career/restore/{id}', 'CareerController@restore');


	});
});


/*
 * Routes protected by auth
 * and admin middleware
*/
Route::group(['middleware' => 'auth'],function(){
	Route::post('update-password/{id?}', 'UserController@savePassword');
	Route::post('update-email/{id?}', 'UserController@saveEmail');

	Route::get('plan-subscription', 'SubscriptionController@getPaymentInformation');
	Route::post('plan-subscription', 'SubscriptionController@postPaymentInformation');

	Route::get('cancel-subscription/{id?}', 'SubscriptionController@cancelSubscription');
	Route::get('resume-subscription/{id?}', 'SubscriptionController@resumeSubscription');
	Route::get('show-receipt/{id}', 'SubscriptionController@showSubscriptionReceipt');

	Route::get('deactivate-account/{id?}', 'UserController@destroy');
	Route::get('invoice-download/{invoice}', 'UserController@stripeInvoice');
	Route::get('print-subscription-payment/{id}', 'UserController@printSubscriptionPayment');

	Route::post('validate-promo-code', 'SubscriptionController@postValidatePromoCode');

});

Route::group(['prefix' => 'messages', 'middleware' => 'auth'], function () {
	Route::get('/', 'MessagesController@index');
	Route::get('new/{job_id}/{user_id}', 'MessagesController@create');
	Route::post('store', 'MessagesController@store');
	Route::get('{id}', 'MessagesController@show');
	Route::post('update/{id}', 'MessagesController@update');
	Route::post('search', 'MessagesController@search');
});

Route::get('check-messages', 'MessagesController@checkNewMessages');

Route::get('test', function(){
	$user = Auth::user();
	$personality_type = $user->personalityType();
	echo '<pre>';
	print_r($personality_type);
});