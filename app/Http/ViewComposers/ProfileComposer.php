<?php

namespace App\Http\ViewComposers;

use JavaScript;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class ProfileComposer
{

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        JavaScript::put([
            'base_url'  => url('/'),
            '_token'    => csrf_token()
        ]);

        $test_vairable = 'test';
        $view->with(compact('test_vairable'));
    }
}