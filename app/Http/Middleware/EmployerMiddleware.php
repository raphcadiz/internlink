<?php

namespace App\Http\Middleware;

use Closure;

class EmployerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()->hasRole('Employer')) {
            return response()->view('errors.403');
        }

        return $next($request);
    }
}
