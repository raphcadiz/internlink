<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if(Auth::user()->hasRole('Student')){
                return redirect('student/dashboard');
            }
            elseif(Auth::user()->hasRole('Employer')){
                return redirect('employer/dashboard');
            }
            elseif(Auth::user()->hasRole('Administrator')){
                return redirect('administrator/dashboard');
            }
            else {
                return redirect('/');
            }
        }

        return $next($request);
    }
}
