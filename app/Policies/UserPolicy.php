<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\User;
use App\Models\Job;
use Auth;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function update(User $user)
    {
        if(Auth::user()->id == $user->id)
            return true;

        return false;
    }

    public function savejob(User $user)
    {
        if( !$user->isPremiumStudent() ) {
            if( $user->saved_jobs->count() >= 10 )
                return false;
            else
                return true;
        }

        return true;
    }

    public function view_compatibility_data(User $user)
    {
        if( !$user->isPremiumStudent() || !$user->isPremiumEmployer() )
            return false;

        return true;
    }

    public function activate_posting(User $user)
    {
        $active_jobs = $user->total_active_jobs();
        if($user->subscribed('starter')){
            if($active_jobs >= 1)
                return false;
        }
        elseif($user->subscribed('plus')){
            if($active_jobs >= 3)
                return false;
        }
        elseif($user->subscribed('pro')){
            if($active_jobs >= 5)
                return false;
        }

        return true;
    }
}
