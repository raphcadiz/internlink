<?php

namespace App\Repositories;

use App\Models\SavedJob;

class SavedJobRepository extends AbstractRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new SavedJob();
    }
}