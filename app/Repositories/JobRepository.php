<?php

namespace App\Repositories;

use App\Models\Job;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class JobRepository extends AbstractRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new Job;
    }

    public function getJobMatches($filters = [], $type = null)
    {
        $keyword = $filters['keyword'];
        $location = $filters['location'];
        $company = $filters['company'];

        $matches = $this->model
                    ->addSelect(DB::raw("jobs.id AS job_id, jobs.title, jobs.user_id AS employer_id, employer_informations.company_name, employer_informations.city, employer_informations.state, careers.title AS career"))
                    ->join('users', 'users.id', '=', 'jobs.user_id')
                    ->join('employer_informations', 'employer_informations.user_id', '=', 'users.id')
                    ->join('careers', 'careers.id', '=', 'jobs.career_id')
                    ->join('industries', 'industries.id', '=', 'careers.industry_id');

        if ($type === 'applied-jobs') {
            $matches->join('job_applicants', function($join) {
                $join->on('job_applicants.job_id', '=', 'jobs.id')
                    ->where('job_applicants.user_id', '=', Auth::user()->id);
            });
        }

        if ($type === 'invited-jobs') {
            $matches->join('job_candidates', function($join) {
                $join->on('job_candidates.job_id', '=', 'jobs.id')
                    ->where('job_candidates.user_id', '=', Auth::user()->id);
            });
        }

        if ($type === 'saved-jobs') {
            $matches->join('saved_jobs', function($join) {
                $join->on('saved_jobs.job_id', '=', 'jobs.id')
                    ->where('saved_jobs.user_id', '=', Auth::user()->id);
            });
        }

        if ($type === 'viewed-me') {
            $matches->join('viewed_students', function($join) {
                $join->on('viewed_students.user_id', '=', 'jobs.user_id')
                    ->where('viewed_students.student_id', '=', Auth::user()->id);
            });
        }

        if(isset($filters['industry_id']) && $filters['industry_id'] != 0) {
            $matches->where('careers.industry_id', $filters['industry_id']);
        }

        if(isset($filters['career_id']) && $filters['career_id'] != 0) {
            $matches->where('careers.id', $filters['career_id']);
        }

        if(isset($filters['time_commitment'])) {
            $matches->whereIn('jobs.employment_type', $filters['time_commitment']);
        }

        if(isset($filters['compensation'])) {
            $matches->whereIn('jobs.paid', $filters['compensation']);
        }

        if(isset($filters['job_type'])) {
            $matches->whereIn('jobs.job_type', $filters['job_type']);
        }

        if(isset($keyword) && !empty($keyword)) {
            $matches->where(function($query) use ($keyword) {
                $query->where("jobs.title", "LIKE", "%{$keyword}%")
                    ->orWhere("jobs.company_description", "LIKE", "%{$keyword}%");
            });
        }

        if(isset($location) && !empty($location)) {
            $matches->where(function($query) use ($location) {
                $query->where("employer_informations.city", "LIKE", "%{$location}%")
                    ->orWhere("employer_informations.state", "LIKE", "%{$location}%")
                    ->orWhere("employer_informations.zip_code", "LIKE", "%{$location}%");
            });
        }

        if(isset($company) && !empty($company)) {
           $matches->where("employers_informations.company_name", "LIKE", "%{$company}%");
        }
        
        $matches->where("jobs.status", "=", "1");

        return $matches->get();
    }
}