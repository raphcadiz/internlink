<?php

namespace App\Repositories;


use App\Models\College;

class CollegeRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new College();
    }

    public function getCollegeList()
    {
        return $this->model
                    ->select('id', 'college')
                    ->orderBy('college')
                    ->lists('college', 'id');
    }
}