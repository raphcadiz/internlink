<?php

namespace App\Repositories;

use App\Models\CandidateTraitSelection;

class CandidateTraitSelectionRepository extends AbstractRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new CandidateTraitSelection();
    }
}