<?php

namespace App\Repositories;

use App\Models\ViewedStudent;

class ViewedStudentRepository extends AbstractRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new ViewedStudent();
    }
}