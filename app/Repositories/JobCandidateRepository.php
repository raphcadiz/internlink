<?php

namespace App\Repositories;

use App\Models\JobCandidate;

class JobCandidateRepository extends AbstractRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new JobCandidate;
    }

    public function getAllCandidatesByUser($user_id)
    {
        return $this->model
            ->select('job_candidates.*')
            ->join('jobs', 'job_candidates.job_id', '=', 'jobs.id')
            ->where('jobs.user_id', $user_id)
            ->get();
    }
}