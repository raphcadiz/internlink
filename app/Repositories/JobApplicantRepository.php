<?php

namespace App\Repositories;

use App\Models\JobApplicant;
use Illuminate\Support\Facades\DB;

class JobApplicantRepository extends AbstractRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new JobApplicant;
    }

    public function getAllApplicantsByUser($user_id)
    {
        return $this->model
            ->select('job_applicants.*')
            ->join('jobs', 'job_applicants.job_id', '=', 'jobs.id')
            ->where('jobs.user_id', $user_id)
            ->get();
    }

    public function getApplicantsBaseOnFilter($filter = [])
    {
        $query = $this->model
                ->addSelect(DB::raw("users.id AS user_id, CONCAT(users.first_name,' ',users.last_name) AS name, colleges.college, student_informations.gpa, college_majors.major"))
                ->addSelect(DB::raw("jobs.id AS job_id, jobs.title AS job_title, job_applicants.status_id"))
                ->join('jobs', 'job_applicants.job_id', '=', 'jobs.id')
                ->join('users', 'job_applicants.user_id', '=', 'users.id')
                ->join('student_informations', 'student_informations.user_id', '=', 'users.id')
                ->join('colleges', 'student_informations.college_id', '=', 'colleges.id')
                ->join('college_majors', 'student_informations.major_id', '=', 'college_majors.id');

        /*if(isset($filter['invited'])) {
            $query->join('job_candidates', function($join   ) {
                 $join->on('job_candidates.job_id', '=', 'jobs.id')
                     ->where('job_candidates.user_id', '=', 'users.id');
            });
        }*/

        if (isset($filter['job_id']) && $filter['job_id'] != '') {
            $query->where('jobs.id', $filter['job_id']);
        }

        if (isset($filter['college_id']) && $filter['college_id'] != 0) {
           $query->where('colleges.id', $filter['college_id']);
        }

        if (isset($filter['major_id']) && $filter['major_id'] != 0) {
            $query->where('college_majors.id', $filter['major_id']);
        }

        if (isset($filter['gpa']) && $filter['gpa'] != '') {
            $query->where('student_informations.gpa', '>', $filter['gpa']);
        }

        if (isset($filter['status']) && $filter['status'] != 'all') {
            $query->where('job_applicants.status_id', $filter['status']);
        }

        if (isset($filter['year']) && !empty($filter['year'])) {
            $query->whereIn('student_informations.year', $filter['year']);
        }

        return $query->groupBy('user_id')->get();
    }
}