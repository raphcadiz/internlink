<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new User;
    }

    public function getCandidatesBaseOnFilter($filter)
    {
        $query = $this->model
                    ->select('users.*')
                    //->addSelect(DB::raw("users.id AS user_id, CONCAT(users.first_name,' ',users.last_name) AS name, colleges.college, student_informations.gpa"))
                    ->join('student_informations', 'student_informations.user_id', '=', 'users.id')
                    ->join('colleges', 'student_informations.college_id', '=', 'colleges.id');

        if (isset($filter['college_id']) && $filter['college_id'] != 0) {
            $query->where('colleges.id', $filter['college_id']);
        }

        if (isset($filter['gpa']) && $filter['gpa'] != '') {
            $query->where('student_informations.gpa', '>', $filter['gpa']);
        }

        if (isset($filter['year']) && !empty($filter['year'])) {
            $query->whereIn('student_informations.year', $filter['year']);
        }

        return $query->get();
    }
}