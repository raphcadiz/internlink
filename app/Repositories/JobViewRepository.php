<?php

namespace App\Repositories;

use App\Models\JobView;

class JobViewRepository extends AbstractRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new JobView();
    }
}