<?php

namespace App\Services;

use App\Repositories\JobApplicantRepository;
use App\Repositories\JobCandidateRepository;
use App\Repositories\ViewedStudentRepository;
use App\Models\Job;
use App\Models\User;
use App\Models\JobCandidate;

class ApplicantsService
{
    protected $viewedStudentRepository;
    protected $jobApplicantRepository;
    protected $jobCandidateRepository;

    public function __construct()
    {
        $this->viewedStudentRepository = new ViewedStudentRepository();
        $this->jobApplicantRepository = new JobApplicantRepository();
        $this->jobCandidateRepository = new JobCandidateRepository();
    }

    public function createViewedStudentRecord($employer_id, $student_id)
    {
        $isAdded = $this->viewedStudentRepository->findBy(['user_id' => $employer_id, 'student_id' => $student_id])->first();

        if(!$isAdded) {
            $this->viewedStudentRepository->create(['user_id' => $employer_id, 'student_id' => $student_id]);
        }
    }

    public function getApplicantsBaseOnFilter($data)
    {
        $result = $this->jobApplicantRepository->getApplicantsBaseOnFilter($data);

        $applicants = [];

        foreach($result as $row) {
            $job = Job::findOrFail($row->job_id);
            $user = User::findOrFail($row->user_id);
            $invited = JobCandidate::where('user_id', $row->user_id)->where('job_id', $row->job_id)->first();

            if((isset($data['invited']) && $invited) || (!isset($data['invited']))) {
                if(round($job->getStudentCompatibilityPercentage($user)) >= $data['compatibility']) {
                    $applicants[] = [
                        'job_id' => $row->job_id,
                        'user_id' => $row->user_id,
                        'name' => $row->name,
                        'college' => $row->college,
                        'major' => $row->major,
                        'gpa' => $row->gpa,
                        'job_title' => $row->job_title,
                        'status' => $row->status,
                        'invited' => ($invited) ? true : false,
                        'compatibility' => round($job->getStudentCompatibilityPercentage($user))
                    ];
                }
            }
        }

        if (!empty($applicants)) {
            foreach ($applicants as $key => $row) {
                $compatibility[$key]  = $row['compatibility'];
            }

            array_multisort($compatibility, SORT_DESC, $applicants);
        }

        return $applicants;
    }

    public function checkIfStudentIsInvited($job_id, $student_id)
    {
        return ($this->jobCandidateRepository->findBy(['job_id' => $job_id, 'user_id' => $student_id])->first()) ? true : false;
    }

    public function getApplicantStatus($job_id, $student_id)
    {
        return $this->jobApplicantRepository->findBy(['job_id' => $job_id, 'user_id' => $student_id])->first()->status;
    }
}