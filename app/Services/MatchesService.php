<?php

namespace App\Services;

use App\Repositories\JobApplicantRepository;
use App\Repositories\JobCandidateRepository;
use App\Repositories\JobRepository;
use App\Repositories\JobViewRepository;
use App\Repositories\SavedJobRepository;
use App\Repositories\UserRepository;
use App\Repositories\ViewedStudentRepository;
use Illuminate\Support\Facades\Auth;

class MatchesService
{
    protected $jobRepository;
    protected $userRepository;
    protected $jobApplicantRepository;
    protected $jobCandidatesRepository;
    protected $savedJobRepository;
    protected $viewedStudentRepository;
    protected $jobViewRepository;

    public function __construct()
    {
        $this->jobRepository = new JobRepository();
        $this->userRepository = new UserRepository();
        $this->jobApplicantRepository = new JobApplicantRepository();
        $this->jobCandidatesRepository = new JobCandidateRepository();
        $this->savedJobRepository = new SavedJobRepository();
        $this->viewedStudentRepository = new ViewedStudentRepository();
        $this->jobViewRepository = new JobViewRepository();
    }

    public function getMatches($filters)
    {
        $jobsIdHolder = [];
        $matches = [];
        $filter_main = (isset($filters['main'])) ? $filters['main'] : [];

        foreach($filter_main as $value) {
            $result = $this->jobRepository->getJobMatches($filters, $value);

            foreach($result as $row)
            {
                if (!in_array($row->job_id, $jobsIdHolder)) {
                    $job = $this->jobRepository->find($row->job_id);
                    $jobCompatibility = round($job->getStudentCompatibilityPercentage(Auth::user()));

                    if($jobCompatibility >= $filters['compatibility']) {
                        $matches[] = [
                            'job_id' => $row->job_id,
                            'title' => $row->title,
                            'employer_id' => $row->employer_id,
                            'company' => $row->company_name,
                            'city' => $row->city,
                            'state' => $row->state,
                            'career' => $row->career,
                            'compatibility' => $jobCompatibility
                        ];
                    }
                    array_push($jobsIdHolder, $row->job_id);
                }
            }
        }

        if(count($filter_main) == 0) {
            $result = $this->jobRepository->getJobMatches($filters, '');

            foreach($result as $row)
            {
                if (!in_array($row->job_id, $jobsIdHolder)) {
                    $job = $this->jobRepository->find($row->job_id);
                    $jobCompatibility = round($job->getStudentCompatibilityPercentage(Auth::user()));

                    if($jobCompatibility >= $filters['compatibility']) {
                        $matches[] = [
                            'job_id' => $row->job_id,
                            'title' => $row->title,
                            'employer_id' => $row->employer_id,
                            'company' => $row->company_name,
                            'city' => $row->city,
                            'state' => $row->state,
                            'career' => $row->career,
                            'compatibility' => $jobCompatibility
                        ];
                    }
                    array_push($jobsIdHolder, $row->job_id);
                }
            }
        }

        if (!empty($matches)) {
            foreach ($matches as $key => $row) {
                $compatibility[$key]  = $row['compatibility'];
            }

            array_multisort($compatibility, SORT_DESC, $matches);
        }

        return $matches;
    }

    public function addJobView($job_id, $student_id)
    {
        return $this->jobViewRepository->create(['job_id' => $job_id, 'student_id' => $student_id]);
    }
}