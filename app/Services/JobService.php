<?php

namespace App\Services;

use App\Repositories\CandidateTraitSelectionRepository;
use App\Repositories\JobRepository;

class JobService
{
    protected $jobRepository;
    protected $candidateTraitRepository;

    public function __construct()
    {
        $this->jobRepository = new JobRepository();
        $this->candidateTraitRepository = new CandidateTraitSelectionRepository();
    }

    public function createJobCandidateTraitSelection($data)
    {
        return $this->candidateTraitRepository->create($data);
    }
}