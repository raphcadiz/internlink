<?php

namespace App\Services;

use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;

class MessengerService
{

    public function __construct()
    {
    }

    public function searchConversation($keyword, $user_id)
    {
        return Thread::forUser($user_id)->latest('updated_at')
            ->join('messages', 'threads.id', '=', 'messages.thread_id')
            ->where("body", "like", "%$keyword%")
            ->groupBy('threads.id');
    }

    public function getThreadByJobAndApplicant($job_id, $user_id)
    {
        return Thread::join('participants', 'participants.thread_id', '=', 'threads.id')
                ->where('participants.user_id', $user_id)
                ->where('job_id', $job_id)->first();
    }
}