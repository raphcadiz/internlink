<?php

namespace App\Services;

use App\Models\Job;
use App\Models\JobCandidate;
use App\Models\User;
use App\Repositories\JobRepository;
use App\Repositories\UserRepository;

class JobApplicantService
{
    protected $jobRepository;
    protected $userRepository;

    public function __construct()
    {
        $this->jobRepository = new JobRepository();
        $this->userRepository = new UserRepository();
    }

    public function getAllApplicants()
    {
        return $this->jobRepository->getAllApplicants();
    }

    public function getCandidatesBaseOnFilter($filter)
    {
        if(!isset($filter['job_id']) || empty($filter['job_id']))
            return [];

        $users = $this->userRepository->getCandidatesBaseOnFilter($filter);
        $job = Job::findOrFail($filter['job_id']);
        $candidates = [];
        foreach($users as $user) {
            if($user->hasRole('Student')) {
                $user_compatibility = $user->getJobCompatibilityPercentage($filter['job_id']);
                $invited = JobCandidate::where('user_id', $user->id)->where('job_id', $job->id)->first();
                if($user_compatibility >= $filter['compatibility']) {
                    $candidates[] = [
                        'job_id' => $job->id,
                        'user_id' => $user->id,
                        'name' => $user->first_name.' '.$user->last_name,
                        'college' => $user->basic_information->college->college,
                        'gpa' => $user->basic_information->gpa,
                        'job_title' => $job->title,
                        'status' => '',
                        'compatibility' => $user_compatibility,
                        'invited' => ($invited) ? true : false
                    ];
                }
            }
        }

        if (!empty($candidates)) {
            foreach ($candidates as $key => $row) {
                $compatibility[$key]  = $row['compatibility'];
            }
            array_multisort($compatibility, SORT_DESC, $candidates);
        }
        

        return $candidates;
    }
}